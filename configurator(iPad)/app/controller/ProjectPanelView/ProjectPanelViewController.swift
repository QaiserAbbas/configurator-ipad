//
//  ProjectPanelViewController.swift
//  configurator(iPad)
//
//  Created by MobileMaster on 1/13/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit
import MessageUI

class ProjectPanelViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, MFMailComposeViewControllerDelegate
{
    @IBOutlet weak var panelProject: UIView!
    @IBOutlet weak var collectionViewForClients: UICollectionView!
    
    public  var mainViewController: MainViewController!
    private var hoverIndex: Int         = -1;
    private var clients: Array<Client>  = ClientManager.getClients()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.clientRemoved(_:)), name: NSNotification.Name(rawValue: C.CLIENT_REMOVED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.clientsDownloaded(_:)), name: NSNotification.Name(rawValue: C.CLIENTS_DOWNLOADED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateClientSuccess(_:)), name: NSNotification.Name(rawValue: C.CLIENT_UPDATED), object: nil)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    func sendEmail(client: Client)
    {
        if !MFMailComposeViewController.canSendMail()
        {
            self.view.makeToast("Mail services are not available")
            return
        }
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients([client.email])

        composeVC.setSubject("")
        composeVC.setMessageBody("", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    @objc func clientRemoved(_ notification: NSNotification)
    {
        self.clients    = ClientManager.getClients()
        
        if (self.clients.count == 0)
        {
            mainViewController.setupPanelView()
        }
        else
        {
            self.collectionViewForClients.reloadData()
        }
    }
    
    func reloadClients()
    {
        ClientService.getClients(user: UserManager.getLoggedInUser()!)
    }
    
    @objc func clientsDownloaded(_ notification: NSNotification)
    {
        self.clients    = ClientManager.getClients()
        self.collectionViewForClients.reloadData()
    }
    
    @objc func updateClientSuccess(_ notification: NSNotification)
    {
        reloadClients()
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return clients.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "project_cell", for: indexPath as IndexPath)
        
        if let cell = cell as? ProjectCollectionViewCell
        {
            if indexPath.row == hoverIndex
            {
                print("indexPath.row :", indexPath.row)
                cell.viewForHoverProject.isHidden = false
            }
            else
            {
                cell.viewForHoverProject.isHidden = true
            }
            
            cell.parentViewController = self
            cell.setupCell(client: clients[indexPath.row])
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        hoverIndex = indexPath.row
        collectionView.reloadData()
    }
    
    @IBAction func addClientClicked(_ sender: Any)
    {
        let user:User   = UserManager.getLoggedInUser()!
        
        if user.accounttype == "0" && self.clients.count >= Int(user.freecount)!
        {
            mainViewController.setupAddPaymentView()
        }
        else
        {
            mainViewController.setupEditClientView(client: Client())
        }
    }
}
