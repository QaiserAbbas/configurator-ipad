//
//  ProjectCollectionViewCell.swift
//  configurator(iPad)
//
//  Created by MobileMaster on 1/13/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit

class ProjectCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imageForProject: UIImageView!

    @IBOutlet weak var labelForClientname: UILabel!
    @IBOutlet weak var labelForPointsCount: UILabel!
    
    @IBOutlet weak var viewForHoverProject: UIView!
    @IBOutlet weak var buttonForStatus: UIButton!
    
    public var parentViewController: ProjectPanelViewController!
    private var client:Client!
    
    public func setupCell(client:Client)
    {
        self.client                     = client
        client.setObjects()
        client.setHairQuestions()
        
        var clientImageUrl:URL?
        
        if (client.points_image != "")
        {
            clientImageUrl      = URL(string:C.BASE_URL + client.points_image)!
        }
        else
        {
            clientImageUrl      = URL(string:C.BASE_URL + client.image)!
        }
        
        self.imageForProject.af_setImage(withURL: clientImageUrl!)
        self.labelForClientname.text    = client.clientname
        self.labelForPointsCount.text   = client.points_count + " Konfigurationen"
        
        let formatter           = DateFormatter()
        formatter.dateFormat    = "yyyy-MM-dd HH:mm:ss"
        let dateObj             = formatter.date( from: self.client.date_last_saved)
        
        let calendar            = Calendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1               = calendar.startOfDay(for: dateObj!)
        let date2               = calendar.startOfDay(for: Date())
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        
        if (self.client.appointment_date != "")
        {
            self.buttonForStatus.setImage(#imageLiteral(resourceName: "image_client_appointment"), for: .normal)
        }
        else if (self.client.finished == "1")
        {
            self.buttonForStatus.setImage(#imageLiteral(resourceName: "image_client_finished"), for: .normal)
        }
        else if (components.day! > 2)
        {
            self.buttonForStatus.setImage(#imageLiteral(resourceName: "image_client_pending"), for: .normal)
        }
        else
        {
            self.buttonForStatus.isHidden   = true
        }
    }
    
    @IBAction func editProjectClicked(_ sender: Any)
    {
        parentViewController.mainViewController.setupEditClientView(client: self.client)
    }
    
    @IBAction func emailProjectClicked(_ sender: Any)
    {
        parentViewController.sendEmail(client: self.client)
    }
    
    @IBAction func printProjectClicked(_ sender: Any)
    {
    }

    @IBAction func goWorkspaceClicked(_ sender: Any)
    {
        parentViewController.mainViewController.setupWorkspaceView(client: self.client)
    }
    
    @IBAction func deleteProjectClicked(_ sender: Any)
    {
        ClientService.removeClient(client: self.client)
    }
}
