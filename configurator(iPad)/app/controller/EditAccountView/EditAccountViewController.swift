//
//  EditAccountViewController.swift
//  configurator(iPad)
//
//  Created by MobileMaster on 1/15/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit
import AlamofireImage

class EditAccountViewController: UIViewController
{
    public  var mainViewController: MainViewController!
    
    @IBOutlet weak var imageForUser: UIImageView!
    @IBOutlet weak var textfieldForUserName: UITextField!
    @IBOutlet weak var textfieldForFullName: UITextField!
    @IBOutlet weak var textfielForAddress: UITextField!
    @IBOutlet weak var textFieldForPostal: UITextField!
    @IBOutlet weak var textfieldForCity: UITextField!
    @IBOutlet weak var textfieldForEmail: UITextField!
    @IBOutlet weak var textfieldForPassword: UITextField!
    
    private var upload: Bool            = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let user:User                   = UserManager.getLoggedInUser()!
        
        self.textfieldForUserName.text  = user.fullname
        self.textfieldForFullName.text  = user.fullname
        self.textfielForAddress.text    = user.street
        self.textFieldForPostal.text    = user.postal
        self.textfieldForCity.text      = user.city
        self.textfieldForEmail.text     = user.username
        self.textfieldForPassword.text  = user.password
        
        changeAvatarImage(url: user.avatar)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.userImageChanged(_:)), name: NSNotification.Name(rawValue: C.NEW_PHOTO_CHANGED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userUpdated(_:)), name: NSNotification.Name(rawValue: C.USER_UPDATED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userUpdatedFailed(_:)), name: NSNotification.Name(rawValue: C.USER_FAILED_UPDATED), object: nil)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
     
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func changeAvatarImage(url: String)
    {
        let imageUrl:URL                = URL(string:C.BASE_URL + url)!
        let size                        = CGSize(width: 500.0, height: 500.0)
        let imageFilter                 = AspectScaledToFillSizeCircleFilter(size: size)
        
        self.imageForUser.af_setImage(withURL: imageUrl, placeholderImage: nil, filter: imageFilter, imageTransition: .crossDissolve(0.2))
    }
    
    @objc func userImageChanged(_ notification: NSNotification)
    {
        if let image = notification.userInfo?["image"] as? UIImage
        {
            self.imageForUser.image     = image.circleMask
            self.upload                 = true
        }
    }

    @objc func userUpdated(_ notification: NSNotification)
    {
        self.view.makeToast("Success Updating User")
    }
    
    @objc func userUpdatedFailed(_ notification: NSNotification)
    {
        self.view.makeToast("Failed Updating User")
    }
    
    @IBAction func takepictureClicked(_ sender: UIButton)
    {
        mainViewController.setupTakePictureView(fromWorkspace: false)
    }
    
    @IBAction func saveClicked(_ sender: Any)
    {
        let user:User       = UserManager.getLoggedInUser()!
        
        if (self.textfieldForFullName.text != "")
        {
            user.fullname   = self.textfieldForFullName.text!
        }
        else
        {
            user.fullname   = self.textfieldForUserName.text!
        }
        
        if (self.textfielForAddress.text != "")
        {
            user.street     = self.textfielForAddress.text!
        }
        
        if (self.textFieldForPostal.text != "")
        {
            user.postal     = self.textFieldForPostal.text!
        }
        
        if (self.textfieldForCity.text != "")
        {
            user.city       = self.textfieldForCity.text!
        }
        
        if (self.textfieldForEmail.text != "")
        {
            user.username   = self.textfieldForEmail.text!
        }
        
        if (self.textfieldForPassword.text != "")
        {
            user.password   = self.textfieldForPassword.text!
        }
        
        UserService.updateUser(user: user, image: imageForUser.image!, upload: self.upload)
    }
}
