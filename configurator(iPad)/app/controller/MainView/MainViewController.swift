//
//  MainViewController.swift
//  configurator(iPad)
//
//  Created by MobileMaster on 1/13/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit
import AlamofireImage

class MainViewController: UIViewController
{
    @IBOutlet weak var labelForUsernameInHeader: UILabel!
    @IBOutlet weak var imageForUserInHeader: UIImageView!
    @IBOutlet weak var viewForHeader: UIView!
    @IBOutlet weak var viewForWorkView: UIView!
    @IBOutlet weak var viewForProfileMenu: UIView!
    @IBOutlet weak var imageForUserInProfileMenu: UIImageView!
    @IBOutlet weak var labelForUsernameInProfileMenu: UILabel!
    @IBOutlet weak var labelForEmailInProfileMenu: UILabel!
    @IBOutlet weak var buttonForProducts: UIButton!
    
    private var overlayView: UIView!
    private var prevOverViewController: UIViewController!
    
//    private var modalView: UIView!
//    private var prevModalViewController: UIViewController!
    
    private var modalViews: Array<UIViewController> = []
    
    private var isShownProfileMenu: Bool            = false
    private var isAnimationingProfileMenu: Bool     = false
    
    var categoryList: [SubCategory]                 = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupHeader()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.userUpdated(_:)), name: NSNotification.Name(rawValue: C.USER_UPDATED), object: nil)
//        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.hidePopupMenu (_:)))
//        self.view.addGestureRecognizer(gesture)
//        setupPanelView()
//        setupEditClientView()
//        setupTakePictureView()
        createCategoryList()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
    }   
    
    deinit
    {
        if (self.prevOverViewController != nil)
        {
            removeOverlayView(viewController: self.prevOverViewController)
        }
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override var prefersStatusBarHidden: Bool
    {
        get
        {
            return true
        }
    }
    
    @objc func userUpdated(_ notification: NSNotification)
    {
        setupHeader()
    }
    
    private func setupHeader ()
    {
        let user:User                               = UserManager.getLoggedInUser()!
        
        self.labelForUsernameInHeader.text          = user.fullname
        self.labelForUsernameInProfileMenu.text     = user.fullname
        self.labelForEmailInProfileMenu.text        = user.username
        
        let userImageUrl: URL                       = URL(string:C.BASE_URL + user.avatar)!
        
        let size                                    = CGSize(width: 90.0, height: 90.0)
        let imageFilter                             = AspectScaledToFillSizeCircleFilter(size: size)
        
        self.imageForUserInHeader.af_setImage(withURL: userImageUrl, placeholderImage: nil, filter: imageFilter, imageTransition: .crossDissolve(0.2))
        self.imageForUserInProfileMenu.af_setImage(withURL: userImageUrl, placeholderImage: nil, filter: imageFilter, imageTransition: .crossDissolve(0.2))
    }
    
    private func createCategoryList()
    {
        categoryList            = []

        for categoryIndex in 0...C.CATEGORY_LIST.count - 1
        {
            let category        = SubCategory()
            
            category.name       = C.CATEGORY_LIST[categoryIndex]
            category.products   = []
            
            categoryList.append(category)
        }
    }
    
    public func setupPanelView()
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"panelViewController") as! PanelViewController
        
        viewController.mainViewController = self
        addOverlayView(viewController: viewController)
    }
    
    public func setupProjectPanelView()
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"projectPanelViewController") as! ProjectPanelViewController
        
        viewController.reloadClients()
        viewController.mainViewController = self
        
        addOverlayView(viewController: viewController)
    }
    
    public func setupWorkspaceView(client:Client)
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"workspaceViewController") as! WorkspaceViewController
        
        viewController.setClient(client: client)
        viewController.mainViewController = self
        addOverlayView(viewController: viewController)
    }
    
    public func setupProductsManageView()
    {
        let storyboard      = UIStoryboard(name: "ProductsManage", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"productsManageViewController") as! ProductsManageViewController
        
        viewController.mainViewController = self
        addOverlayView(viewController: viewController)
    }
    
    public func setupProductSelectView(productManageView: ProductsManageViewController)
    {
        let storyboard      = UIStoryboard(name: "ProductsManage", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"productSelectViewController") as! ProductSelectViewController
        
        viewController.mainViewController   = self
        viewController.delegate             = productManageView
        addModalView(viewController: viewController)
        self.view.bringSubview(toFront: self.viewForWorkView)
    }

    public func setupProductCreateView(productManageView: ProductsManageViewController)
    {
        let storyboard      = UIStoryboard(name: "ProductsManage", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"productCreatViewController") as! ProductCreatViewController
        
        viewController.mainViewController   = self
        viewController.delegate             = productManageView
        addModalView(viewController: viewController)
        self.view.bringSubview(toFront: self.viewForWorkView)
    }
    
    public func setupEditAccountView()
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"editAccountViewController") as! EditAccountViewController
        
        viewController.mainViewController = self
        addOverlayView(viewController: viewController)
    }
    
    public func setupEditClientView(client: Client)
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"editClientViewController") as! EditClientViewController

        viewController.setClient(client: client)
        viewController.mainViewController       = self
        addModalView(viewController: viewController)
        self.view.bringSubview(toFront: self.viewForWorkView)
    }
    
    public func setupDrawSignView(client: Client, isCreating:Bool)
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"signViewController") as! SignViewController
        
        viewController.setClient(client: client, isCreating: isCreating)
        viewController.mainViewController = self
        addModalView(viewController: viewController)
    }

    public func setupAddPaymentView()
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"AddPaymentViewController") as! AddPaymentViewController
        viewController.mainViewController = self
        addModalView(viewController: viewController)
        self.view.bringSubview(toFront: self.viewForWorkView)
    }
    
    public func setupPointPropertyView(object: Object, tag: Int)
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"pointPropertyViewController") as! PointPropertyViewController
        
        viewController.mainViewController   = self
        viewController.object               = object
        viewController.selectedTag          = tag
        
        addModalView(viewController: viewController)
    }
    
    public func setupPointInfoView(categoryIndex: Int, subcategoryIndex: Int)
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"pointInfoViewController") as! PointInfoViewController
        
        viewController.mainViewController   = self
        viewController.categoryIndex        = categoryIndex
        viewController.subcategoryIndex     = subcategoryIndex
        
        addModalView(viewController: viewController)
        self.view.bringSubview(toFront: self.viewForWorkView)
    }

    public func setupHairProductSelectView(subCategory: SubCategory, descriptionIndex: Int)
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"hairProductSelectViewController") as! HairProductSelectViewController
        
        viewController.mainViewController   = self
        viewController.subCategory          = subCategory
        viewController.descriptionIndex     = descriptionIndex;
        
        addModalView(viewController: viewController)

        self.view.bringSubview(toFront: self.viewForWorkView)
    }
    
    public func setupHairProductCalendarView(hairSectionView: WorkspaceHairSectionViewController)
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"hairProductCalendarViewController") as! HairProductCalendarViewController
        
        viewController.mainViewController   = self
        viewController.client               = hairSectionView.client
        viewController.delegate             = hairSectionView
        
        addModalView(viewController: viewController)
        
        self.view.bringSubview(toFront: self.viewForWorkView)
    }
    
    public func setupDrawPropertyView(object: Object)
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"drawPropertyViewController") as! DrawPropertyViewController
        
        viewController.mainViewController   = self
        viewController.object               = object
        
        addModalView(viewController: viewController)
    }

    public func setupDefinedPropertyView(object: Object)
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"definedPropertyViewController") as! DefinedPropertyViewController
        
        viewController.mainViewController   = self
        viewController.object               = object
        
        addModalView(viewController: viewController)
    }

    public func setupTakePictureView(fromWorkspace: Bool)
    {
        let storyboard      = UIStoryboard(name: "Main", bundle: nil)
        let viewController  = storyboard.instantiateViewController(withIdentifier :"takePictureViewController") as! TakePictureViewController
        
        viewController.callingFromWorkspace = fromWorkspace
        viewController.mainViewController   = self
        addModalView(viewController: viewController)
    }
    
    private func addOverlayView(viewController: UIViewController)
    {
        let appDelegate                     = UIApplication.shared.delegate as! AppDelegate
        appDelegate.canSupportPortraitMode  = false
        
        if (isShownProfileMenu)
        {
            hideProfileMenu()
        }
        
        if (self.prevOverViewController != nil)
        {
            removeOverlayView(viewController: self.prevOverViewController)
        }
        
        self.overlayView                = viewController.view
        self.addChildViewController(viewController)
        self.viewForWorkView.addSubview(self.overlayView)
        self.view.bringSubview(toFront: viewForHeader)
        viewController.didMove(toParentViewController: self)
        self.prevOverViewController     = viewController
    }

    public func removeOverlayView(viewController: UIViewController)
    {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
        
        if (self.modalViews.count > 0)
        {
            for modalViewController in self.modalViews
            {
                removeModelView(viewController: modalViewController)
            }
//            removeModelView(viewController: self.prevModalViewController)
        }
    }
    
    private func showModalView(viewController: UIViewController)
    {
        self.addChildViewController(viewController)
        self.viewForWorkView.addSubview(viewController.view)
//        self.view.bringSubview(toFront: viewForHeader)
        viewController.didMove(toParentViewController: self)
        //        self.prevModalViewController    = viewController
        
        let backgroundView              = viewController.view.viewWithTag(1001)
        let contentView                 = viewController.view.viewWithTag(1002)
        
        backgroundView?.alpha           = 0.0
        contentView?.alpha              = 0.0
        
        for view in (contentView?.subviews)!
        {
            view.alpha                  = 0.0
            UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseIn, animations:{
                view.alpha    = 1.0
            }, completion: {
                (value: Bool) in
            })
        }
        
        UIView.animate(withDuration: 0.2, delay: 0.1, options: .curveEaseIn, animations:{
            contentView?.alpha          = 1.0
            backgroundView?.alpha       = 1.0
        }, completion: {
            (value: Bool) in
        })
    }
    
    private func addModalView(viewController: UIViewController)
    {
        let appDelegate                     = UIApplication.shared.delegate as! AppDelegate
        appDelegate.canSupportPortraitMode  = false
        
        if (isShownProfileMenu)
        {
            hideProfileMenu()
        }
        
//        self.modalView                  = viewController.view
        self.modalViews.append(viewController)
        showModalView(viewController: viewController)
    }
    
    public func removeModelView(viewController: UIViewController)
    {
        let appDelegate                     = UIApplication.shared.delegate as! AppDelegate
        appDelegate.canSupportPortraitMode  = false
        
//        self.modalView.alpha = 1.0
//        UIView.animate(withDuration: 0.3, animations:{
//            self.modalView.alpha = 0.0
//        }, completion: {
//            (value: Bool) in
//            viewController.willMove(toParentViewController: nil)
//            viewController.view.removeFromSuperview()
//            viewController.removeFromParentViewController()
//            
//            self.modalView                      = nil
//            self.prevModalViewController        = nil
//            self.view.bringSubview(toFront: self.viewForHeader)
//        })
        var modalView:UIView!
        if let index = self.modalViews.index(of:viewController)
        {
            modalView   = viewController.view
            if modalView != nil
            {
                let backgroundView              = modalView.viewWithTag(1001)
                let contentView                 = modalView.viewWithTag(1002)
                
                backgroundView?.alpha           = 1.0
                contentView?.alpha              = 1.0
                
                UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveEaseOut, animations:{
                    contentView?.alpha          = 0.0
                    backgroundView?.alpha       = 0.0
                }, completion: {
                    (value: Bool) in
                    viewController.willMove(toParentViewController: nil)
                    viewController.view.removeFromSuperview()
                    viewController.removeFromParentViewController()
                    
                    self.modalViews.remove(at: index)
                    if (self.modalViews.count > 0)
                    {
                        let lastViewController  = self.modalViews.last
                        self.showModalView(viewController: lastViewController!)
                    }
                    else
                    {
                        self.view.bringSubview(toFront: self.viewForHeader)
                    }
                })
                
                for view in (contentView?.subviews)!
                {
                    view.alpha                  = 1.0
                    UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations:{
                        view.alpha    = 0.0
                    }, completion: {
                        (value: Bool) in
                    })
                }
            }
            else
            {
                viewController.willMove(toParentViewController: nil)
                viewController.view.removeFromSuperview()
                viewController.removeFromParentViewController()
                
                self.view.bringSubview(toFront: self.viewForHeader)
            }
        }
    }
    
    private func hideProfileMenu()
    {
        self.viewForProfileMenu.isHidden = false
        let frame = self.viewForProfileMenu.frame
        isAnimationingProfileMenu = true
        self.view.bringSubview(toFront: self.viewForProfileMenu)
        self.view.bringSubview(toFront: self.viewForHeader)
        
        UIView.animate(withDuration: 0.7, animations:
        {
            self.viewForProfileMenu.frame   = CGRect(x: frame.origin.x, y: frame.origin.y - frame.size.height, width: frame.size.width, height: frame.size.height)
        }, completion: { (finished: Bool) in
            self.isShownProfileMenu         = false
            self.isAnimationingProfileMenu  = false
        })
    }
    
    private func showProfileMenu()
    {
        self.viewForProfileMenu.isHidden = false
        let frame = self.viewForProfileMenu.frame
        isAnimationingProfileMenu = true
        self.view.bringSubview(toFront: self.viewForProfileMenu)
        self.view.bringSubview(toFront: self.viewForHeader)
        
        self.viewForProfileMenu.frame = CGRect(x: frame.origin.x, y: self.viewForHeader.frame.size.height - frame.size.height, width: frame.size.width, height: frame.size.height)
        UIView.animate(withDuration: 0.7, animations:
            {
                self.viewForProfileMenu.frame = CGRect(x: frame.origin.x, y: self.viewForHeader.frame.size.height - 4 , width: frame.size.width, height: frame.size.height)
        }, completion: { (finished: Bool) in
            self.isShownProfileMenu = true
            self.isAnimationingProfileMenu = false
        })
    }
    
    func hidePopupMenu(_ sender:UITapGestureRecognizer)
    {
        if (isShownProfileMenu)
        {
            hideProfileMenu()
        }
    }
    
    @IBAction func dashboardMenuClicked(_ sender: Any)
    {
        if (isAnimationingProfileMenu)
        {
            return
        }
        
        if (isShownProfileMenu)
        {
            hideProfileMenu()
        }
        else
        {
            showProfileMenu()
        }
    }
    
    @IBAction func productsMenuClicked(_ sender: UIButton)
    {
        setupProductsManageView()
    }
    
    @IBAction func workspaceClicked(_ sender: Any)
    {
        let clients    = ClientManager.getClients()
        
        if (clients.count == 0)
        {
            setupPanelView()
        }
        else
        {
            setupProjectPanelView()
        }
    }
    
    @IBAction func editAccountClicked(_ sender: UIButton)
    {
        setupEditAccountView()
    }
    
    @IBAction func logoutClicked(_ sender: UIButton)
    {
//        let _ = self.navigationController?.popViewController(animated: true);
        if (self.prevOverViewController != nil)
        {
            removeOverlayView(viewController: self.prevOverViewController)
        }
        dismiss(animated: true, completion: nil)
    }
}
