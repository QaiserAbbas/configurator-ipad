//
//  HairProductSelectViewController.swift
//  configurator(iPad)
//
//  Created by MobileMaster on 5/23/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit

class HairProductSelectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var scrollviewForBrand: UIScrollView!    
    @IBOutlet weak var tableviewForProducts: UITableView!

    public var mainViewController: MainViewController!
    public var subCategory: SubCategory!
    public var descriptionIndex: Int!
    
    var productList: [Product]      = []
    var selectedOneItem             = false
    var ratio:CGFloat               =  0
    var brandViews: [UIView]        = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        ratio                       =   CGFloat(self.view.frame.size.width / 1024)
        loadScrollByBrand()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    private func loadScrollByBrand()
    {
        let scrollFrame             = self.scrollviewForBrand.frame.size
        var nextPointX:CGFloat      = 0
        
        self.brandViews             = []
        
        for subView in self.scrollviewForBrand.subviews
        {
            subView.removeFromSuperview()
        }
        
        if ((self.subCategory.brands) == nil)
        {
            return
        }
        
        if (self.subCategory.brands?.count)! == 0
        {
            return
        }
        
        for index in 0 ... (self.subCategory.brands?.count)! - 1
        {
            let brand               = self.subCategory.brands?[index]
            let containerView       = UIView()
            var imageWidth: CGFloat = 0
            
            if brand?.id != "0"
            {
                let imageView           = UIImageView(frame: CGRect(x: 7 * ratio, y: 10 * ratio, width: 50 * ratio, height: 25 * ratio))
                imageView.af_setImage(withURL: URL(string:C.BASE_URL + (brand?.image_url)!)!)
                imageView.contentMode   = .scaleAspectFit
                imageWidth              = imageView.frame.size.width
                
                containerView.addSubview(imageView)
            }
            
            
            let label               = UILabel(frame: CGRect(x: 7 * ratio, y: (scrollFrame.height - 22 * ratio) / 2, width: 63 * ratio, height: 22 * ratio))
            
            label.text              = brand?.name
            label.font              = UIFont(name: "SofiaProSemiBold", size: 15)
            label.textColor         = ThemeUtil.getMainCategoryLabelColor()
            
            label.sizeToFit()
            label.frame             = CGRect(x: imageWidth + label.frame.origin.x + 7 * ratio, y: (scrollFrame.height - label.frame.size.height) / 2, width: label.frame.size.width + 9 * ratio, height: label.frame.size.height)
            
            containerView.frame     = CGRect(x: nextPointX, y: 0, width: label.frame.origin.x + label.frame.size.width, height: scrollFrame.height)
            containerView.layer.borderWidth    = 2;
            containerView.layer.borderColor    = ThemeUtil.getMainCategoryBorderColor().cgColor
            
            let button              = UIButton(frame: CGRect(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height))
            button.addTarget(self, action: #selector(brandSelected(_:)), for: .touchUpInside)
            button.tag              = index + 1000;
            
            nextPointX              += containerView.frame.size.width + 10
            
            containerView.addSubview(label)
            containerView.addSubview(button)
            
            brandViews.append(containerView)
            
            self.scrollviewForBrand.addSubview(containerView)
        }
        
        self.scrollviewForBrand.contentSize = CGSize(width: nextPointX, height: scrollFrame.height)
        brandSelect(selectIndex: 0)
    }

    @IBAction func brandSelected(_ sender: UIButton)
    {
        let selectIndex       = sender.tag - 1000
        brandSelect(selectIndex: selectIndex)
    }
    
    private func brandSelect(selectIndex: Int)
    {
        productList     = []
        
        for index in 0 ... brandViews.count - 1
        {
            let containerView                       = brandViews[index]
            var label                               = UILabel()
            
            for subView in containerView.subviews
            {
                if subView is UILabel
                {
                    label                           = subView as! UILabel
                }
            }
            
            if selectIndex == index
            {
                containerView.layer.borderWidth     = 0
                containerView.backgroundColor       = ThemeUtil.getModeColor(type: C.MainMode.MAIN_HAIR)
                label.textColor                     = UIColor.white
            }
            else
            {
                containerView.layer.borderWidth     = 2
                containerView.layer.borderColor     = ThemeUtil.getMainCategoryBorderColor().cgColor
                containerView.backgroundColor       = UIColor.clear
                label.textColor                     = ThemeUtil.getMainCategoryLabelColor()
            }
        }
        
        if (self.subCategory.brands != nil)
        {
            if (self.subCategory.brands?[selectIndex].products.count)! > 0
            {
                for index in 0 ... (self.subCategory.brands?[selectIndex].products.count)! - 1
                {
                    let product             = self.subCategory.brands![selectIndex].products[index]
                    let newProduct          = Product()
                    
                    newProduct.id           = product.id
                    newProduct.name         = product.name
                    newProduct.descriptions = product.descriptions
                    newProduct.image_url    = product.image_url
                    newProduct.selected     = product.selected
                    newProduct.category     = product.category
                    newProduct.brand_id     = product.brand_id
                    newProduct.brand_name   = product.brand_name
                    newProduct.custom       = product.custom
                    newProduct.cycle        = product.cycle
                    
                    if product.selected && !existedDescription(searchDescription: "brand:description:" + product.name)
                    {
                        newProduct.selected = false
                    }
                    
                    productList.append(newProduct)
                }
                
                self.tableviewForProducts.reloadData()
            }
        }
    }
    
    private func existedDescription(searchDescription: String) -> Bool
    {
        for description in self.subCategory.descriptions
        {
            if description == searchDescription
            {
                return true
            }
        }
        
        return false
    }
    
    private func addProductDescription()
    {
        if productList.count > 0
        {
            for index in 0 ... productList.count - 1
            {
                let product         = self.productList[index]
                let addDescription  = "brand:description:" + product.name
                if product.selected && !existedDescription(searchDescription: addDescription)
                {
                    if !selectedOneItem
                    {
                        self.subCategory.descriptions[self.descriptionIndex]    = addDescription
                        selectedOneItem = true
                    }
                    else
                    {
                        self.subCategory.descriptions.append(addDescription)
                    }
                }
                
                if !product.selected && existedDescription(searchDescription: addDescription)
                {
                    self.subCategory.descriptions[self.descriptionIndex]    = ""
                }

                
                self.subCategory.brands![0].products[index].selected    = product.selected
            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.HAIR_ADD_PRODUCT_DESCRIPTION), object: self,  userInfo: nil)
        }
    }
    
    @IBAction func navigationButtonClicked(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1:
            addProductDescription()
            break
        default:
            break
        }
        mainViewController.removeModelView(viewController: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return productList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 105
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell    = tableView.dequeueReusableCell(withIdentifier: "HairProductCell", for: indexPath) as! HairProductCell

//        let product:Product                     = self.subCategory.brands![0].products[indexPath.row]
        let product:Product                         = self.productList[indexPath.row]
        
//        cell.labelForProductName.text           = product.name
        if product.custom == "1"
        {
            let main_string                         = "\(product.name)    Eigenprodukt    "
            let string_to_color                     = "   Eigenprodukt   "
            
            let range                               = (main_string as NSString).range(of: string_to_color)
            print("range = \(range.length)")
            let attribute                           = NSMutableAttributedString.init(string: main_string)
            attribute.addAttribute(NSAttributedStringKey.backgroundColor, value: ThemeUtil.getProductDescriptionColor(custom: "1") , range: range)
            attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: range)
            attribute.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "SofiaProBold", size: 10)! , range: range)
            
            cell.labelForProductName.attributedText = attribute
            cell.imageForProduct.frame              = CGRect(x: 23 * ratio, y: 23 * ratio, width: 60 * ratio, height: 60 * ratio)
        }
        else
        {
            cell.labelForProductName.text           = product.brand_name + ", " + product.name
            cell.imageForProduct.frame              = CGRect(x: 13 * ratio, y: 13 * ratio, width: 80 * ratio, height: 80 * ratio)
        }
        
        cell.labelForProductDescription.textColor   = ThemeUtil.getProductDescriptionColor(custom: product.custom)
        cell.labelForProductDescription.text        = product.descriptions
        cell.imageForProduct.af_setImage(withURL: URL(string:C.BASE_URL + product.image_url)!)
        cell.product                                = product
        
        if product.selected
        {
            cell.buttonForSelect.setImage(UIImage(named: "image_hair_product_checked"), for: .normal)
        }
        else
        {
            cell.buttonForSelect.setImage(UIImage(named: "image_hair_product_unchecked"), for: .normal)
        }
        
        return cell
    }

}
