//
//  HairProductCalendarViewController.swift
//  configurator(iPad)
//
//  Created by MobileSuperMaster on 7/12/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Calendar_iOS
import MBProgressHUD

protocol HairProductCalendarDelegate
{
    func completeSettingCalendar(viewController: HairProductCalendarViewController)
}

class HairProductCalendarViewController: UIViewController, CalendarViewDelegate
{
    @IBOutlet weak var viewForSelectDate: UIView!
    @IBOutlet weak var viewForSelectTime: UIView!
    
    @IBOutlet weak var viewForCalendar: CalendarView!
    @IBOutlet weak var labelForDate: UILabel!
    
    @IBOutlet weak var buttonForTime: UIButton!
    @IBOutlet weak var imageForCheck: UIImageView!
    
    public var mainViewController: MainViewController!
    
    public var client:Client            = Client()
    private var selecteDate: Date       = Date()
    private var isEmailChecked: Bool    = false
    var delegate:HairProductCalendarDelegate?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.viewForCalendar.calendarDelegate   = self
        self.viewForCalendar.fontName           = "SofiaProSemiBold"
        self.viewForCalendar.selectionColor     = ThemeUtil.getModeColor(type: C.MainMode.MAIN_HAIR)
        
        let formatter               = DateFormatter()
//        let userDefaults        = UserDefaults.standard
        
//        if (userDefaults.string(forKey: C.HAIR_REMINDER_KEY) != nil)
        if self.client.beauty_treatment_date == ""
        {
            self.selecteDate        = Date()
        }
        else
        {
//            formatter.dateFormat    = "yyyy-MM-dd HH:mm:ss Z"
//            self.selecteDate        = formatter.date( from: userDefaults.string(forKey: C.HAIR_REMINDER_KEY)!)!
            self.selecteDate        = Date(timeIntervalSince1970:Double(self.client.beauty_treatment_date)!)
            self.buttonForTime.setTitleColor(.black, for: .normal)
        }

        formatter.dateFormat        = "MMMM yyyy"
        formatter.locale            = NSLocale(localeIdentifier: "de_DE") as Locale!
        let result                  = formatter.string(from: self.selecteDate)
        self.labelForDate.text      = result
        
        formatter.dateFormat        = "HH:mm"        
        self.buttonForTime.setTitle(formatter.string(from: self.selecteDate), for: .normal)
        
        self.viewForCalendar.currentDate    = self.selecteDate
        self.viewForSelectDate.isHidden     = false
        self.viewForSelectTime.isHidden     = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBDateSuccess(_:)), name: NSNotification.Name(rawValue: C.UPDATE_CLIENT_BDATE_SUCCESS), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBDateFailed(_:)), name: NSNotification.Name(rawValue: C.UPDATE_CLIENT_BDATE_FAILED), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeBDateSuccess(_:)), name: NSNotification.Name(rawValue: C.REMOVE_CLIENT_BDATE_SUCCESS), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeBDateFailed(_:)), name: NSNotification.Name(rawValue: C.REMOVE_CLIENT_BDATE_FAILED), object: nil)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    public func didChangeCalendarDate(_ date: Date!)
    {
        let formatter               = DateFormatter()
        formatter.dateFormat        = "MMMM yyyy"
        formatter.locale            = NSLocale(localeIdentifier: "de_DE") as Locale!
        let result                  = formatter.string(from: date)
        
        self.selecteDate            = date
        self.labelForDate.text      = result
    }
   
    @objc func updateBDateSuccess(_ notification: NSNotification)
    {
        MBProgressHUD.hide(for: self.view, animated: true)
        self.client.beauty_treatment_date   = String(self.selecteDate.timeIntervalSince1970)
        delegate?.completeSettingCalendar(viewController: self)
        mainViewController.removeModelView(viewController: self)
    }
    
    @objc func updateBDateFailed(_ notification: NSNotification)
    {
        self.view.makeToast("Failed to update beauty date")
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    @objc func removeBDateSuccess(_ notification: NSNotification)
    {
        MBProgressHUD.hide(for: self.view, animated: true)
        self.client.beauty_treatment_date   = ""
        delegate?.completeSettingCalendar(viewController: self)
        mainViewController.removeModelView(viewController: self)
    }
    
    @objc func removeBDateFailed(_ notification: NSNotification)
    {
        self.view.makeToast("Failed to remove beauty date")
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    private func saveDate()
    {
        let formatter           = DateFormatter()
        formatter.dateFormat    = "yyyy-MM-dd HH:mm:ss Z"

        let emailChecked        = isEmailChecked ? 1 : 0
        
        var parameters          = Dictionary<String, String>()
        parameters              = ["client_id":self.client.id, "beauty_date":String(self.selecteDate.timeIntervalSince1970), "is_email_checked": String(emailChecked)]
        
        ClientService.updateClientBeautyDate(parameters: parameters)
    }
    
    private func removeDate()
    {
        let parameters:Dictionary<String, String> = ["client_id":self.client.id]
        
        ClientService.removeClientBeautyDate(parameters: parameters)
    }
    
    @IBAction func buttonClicked(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1: // next month
            self.viewForCalendar.advanceCalendarContents()
            break
        case 2: // prev month
            self.viewForCalendar.rewindCalendarContents()
            break
        case 3: // cancel
            removeDate()
            break
        case 4: // next
            self.viewForSelectDate.isHidden = true
            self.viewForSelectTime.isHidden = false
            break
        default:
            break
        }
    }
    
    @IBAction func buttonTimeClicked(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1: // select time
            let datePicker = ActionSheetDatePicker(title: nil, datePickerMode: UIDatePickerMode.time, selectedDate: self.selecteDate, doneBlock: {
                picker, value, index in
                
                let formatter           = DateFormatter()
                formatter.dateFormat    = "yyyy-MM-dd HH:mm:ss Z"
                let dateObj             = formatter.date( from: String(describing: value!))
                self.selecteDate        = dateObj!
                
                formatter.dateFormat    = "HH:mm"
                formatter.locale        = NSLocale(localeIdentifier: "de_DE") as Locale!
                let result              = formatter.string(from: dateObj!)
                
                self.buttonForTime.setTitle(result, for: .normal)
                self.buttonForTime.setTitleColor(.black, for: .normal)
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview)
            
            datePicker?.show()
            break
        case 2: // check email notification
            if isEmailChecked
            {
                self.imageForCheck.image    = UIImage.init(named: "image_time_email_check")
                isEmailChecked              = false
            }
            else
            {
                self.imageForCheck.image    = UIImage.init(named: "image_time_email_checked")
                isEmailChecked              = true
            }
            break
        case 3: // cancel
            removeDate()
//            mainViewController.removeModelView(viewController: self)
            break
        case 4: // done
            saveDate()
            break
        default:
            break
        }
    }
}
