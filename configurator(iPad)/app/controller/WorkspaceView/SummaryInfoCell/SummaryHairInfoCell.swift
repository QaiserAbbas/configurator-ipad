//
//  SummaryHairInfoCell.swift
//  configurator(iPad)
//
//  Created by MobileMaster on 5/2/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit

class SummaryHairInfoCell: UITableViewCell
{
    public var ratio:CGFloat    = 0
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    private func findProduct(subcategory:SubCategory, descriptionString: String) -> Product
    {
        if (subcategory.brands?.count)! > 0
        {
            for brand in subcategory.brands!
            {
                if brand.products.count > 0
                {
                    for product in brand.products
                    {
                        if descriptionString == product.name
                        {
                            return product
                        }
                    }
                }
            }
        }
        
        return Product()
    }
    
    public func showHairSummary(client: Client, order: Int)
    {
        let hairQuestions           = client.hairQuestions
        for subview in contentView.subviews
        {
            subview.removeFromSuperview()
        }
        
        let orderImageView          = UIImageView(frame: CGRect(x: 27 * ratio, y: 9 * ratio, width: 28 * ratio, height: 28 * ratio))
        orderImageView.image        = UIImage(named:"image_hair_step_active.png")
        
        let orderLabel              = UILabel(frame: CGRect(x: 27 * ratio, y: 9 * ratio, width: 28 * ratio, height: 28 * ratio))
        orderLabel.text             = String(order)
        orderLabel.font             = UIFont(name: "SofiaProSemiBold", size: 18)
        orderLabel.textColor        = UIColor.white
        orderLabel.textAlignment    = .center
        
        let mainTitleLabel          = UILabel(frame: CGRect(x: 81 * ratio, y: 9 * ratio, width: 380 * ratio, height: 28 * ratio))
        mainTitleLabel.text         = "Haut & Haare"
        mainTitleLabel.font         = UIFont(name: "SofiaProSemiBold", size: 14)
        mainTitleLabel.textColor    = ThemeUtil.getStateColor(type: C.ButtonState.NORMAL_STATE)
        
        var posY:CGFloat            = 42 * ratio
        
        if (client.beauty_treatment_date != "")
        {
            let date                    = Date(timeIntervalSince1970:Double(client.beauty_treatment_date)!)
            let dateTitleLabel          = UILabel(frame: CGRect(x: 81 * ratio, y: posY, width: 350 * ratio, height: 22 * ratio))
            dateTitleLabel.text         = "Nächster Behanlungstermin"
            dateTitleLabel.font         = UIFont(name: "SofiaProSemiBold", size: 14)
            dateTitleLabel.textColor    = ThemeUtil.getModeColor(type: C.MainMode.MAIN_HAIR)
            
            posY                        += dateTitleLabel.frame.size.height
            
            var posX:CGFloat            = 0
            let dateContainerView       = UIView(frame: CGRect(x: 81 * ratio, y: posY, width: 350 * ratio, height: 40 * ratio))
            let dateImageView           = UIImageView(frame: CGRect(x: posX, y: (dateContainerView.frame.size.height - 22 * ratio) / 2, width: 21 * ratio, height: 22 * ratio))
            dateImageView.image         = UIImage(named:"image_summary_date.png")
            
            posX                        += 28 * ratio
            let formatter               = DateFormatter()
            formatter.dateFormat        = "dd.MM.yyyy"
            formatter.locale            = NSLocale(localeIdentifier: "de_DE") as Locale!
            
            let dateLabel               = UILabel(frame: CGRect(x: posX, y: (dateContainerView.frame.size.height - 22 * ratio) / 2, width: 100 * ratio, height: 22 * ratio))
            dateLabel.text              = formatter.string(from: date)
            dateLabel.font              = UIFont(name: "SofiaProLight", size: 20)
            dateLabel.textColor         = ThemeUtil.getSummaryDateColor()
            
            posX                        += 110 * ratio
            let dividerView             = UIView(frame: CGRect(x: posX, y: (dateContainerView.frame.size.height - 14 * ratio) / 2, width: 1 * ratio, height: 14 * ratio))
            dividerView.backgroundColor = ThemeUtil.getHairSectionBorderColor()
            
            posX                        += 23 * ratio
            let timeImageView           = UIImageView(frame: CGRect(x: posX, y: (dateContainerView.frame.size.height - 17 * ratio) / 2, width: 17 * ratio, height: 17 * ratio))
            timeImageView.image         = UIImage(named:"image_summary_time.png")
            
            posX                        += 27 * ratio
            formatter.dateFormat        = "h:mm 'Uhr' "
            let timeLabel               = UILabel(frame: CGRect(x: posX, y: (dateContainerView.frame.size.height - 22 * ratio) / 2, width: 100 * ratio, height: 22 * ratio))
            timeLabel.text              = formatter.string(from: date)
            timeLabel.font              = UIFont(name: "SofiaProLight", size: 20)
            timeLabel.textColor         = ThemeUtil.getSummaryDateColor()
            
            posY                        += 40 * ratio
            
            dateContainerView.addSubview(dateImageView)
            dateContainerView.addSubview(dateLabel)
            dateContainerView.addSubview(dividerView)
            dateContainerView.addSubview(timeImageView)
            dateContainerView.addSubview(timeLabel)
            
            contentView.addSubview(dateTitleLabel)
            contentView.addSubview(dateContainerView)
        }
        
        for stepIndex in 0 ... hairQuestions.steps.count - 1
        {
            let step                    = hairQuestions.steps[stepIndex]
            
            let stepTitleLabel          = UILabel(frame: CGRect(x: 81 * ratio, y: posY, width: 350 * ratio, height: 22 * ratio))
            stepTitleLabel.text         = step.name
            stepTitleLabel.font         = UIFont(name: "SofiaProSemiBold", size: 14)
            stepTitleLabel.textColor    = ThemeUtil.getModeColor(type: C.MainMode.MAIN_HAIR)
            
            posY                        += stepTitleLabel.frame.size.height + 5 * ratio
            var addedStepTitle          = false
            
            for categoryIndex in 0 ... step.categories.count - 1
            {
                let category                    = step.categories[categoryIndex]
                
                let categoryTitleLabel          = UILabel(frame: CGRect(x: 81 * ratio, y: posY, width: 350 * ratio, height: 22 * ratio))
                categoryTitleLabel.text         = category.name
                categoryTitleLabel.font         = UIFont(name: "SofiaProSemiBold", size: 13)
                categoryTitleLabel.textColor    = ThemeUtil.getHairCategoryLabelColor()
                
                posY                            += categoryTitleLabel.frame.size.height + 5 * ratio
                var addedIndex                  = 0
                var selectedFound               = false
                var subCategoryOrder            = 0
                
                for subCategoryIndex in 0 ... category.subCategories.count - 1
                {
                    let subCategory             = category.subCategories[subCategoryIndex]
                    
                    if subCategory.selected
                    {
                        if !selectedFound
                        {
                            if !addedStepTitle
                            {
                                addedStepTitle  = true
                            }
                            
                            selectedFound       = true
                        }
                        
                        if addedIndex % 3 == 0 && addedIndex != 0
                        {
                            addedIndex          = 0
                            posY                += 45 * ratio
                        }
                        
                        if stepIndex == 3
                        {
                            addedIndex                  = 3
                            subCategoryOrder            += 1
                            let subOrderImageView       = UIImageView(frame: CGRect(x: 81 * ratio, y: posY, width: 19 * ratio, height: 19 * ratio))
                            subOrderImageView.image     = UIImage(named:"image_hair_step_active.png")
                            
                            let subOrderLabel           = UILabel(frame: CGRect(x: 81 * ratio, y: posY, width: 19 * ratio, height: 19 * ratio))
                            subOrderLabel.text          = String(subCategoryOrder)
                            subOrderLabel.font          = UIFont(name: "SofiaProSemiBold", size: 14)
                            subOrderLabel.textColor     = UIColor.white
                            subOrderLabel.textAlignment = .center
                            
                            let subTitleLabel           = UILabel(frame: CGRect(x: 110 * ratio, y: posY, width: 350 * ratio, height: 19 * ratio))
                            subTitleLabel.text          = subCategory.name
                            subTitleLabel.font          = UIFont(name: "SofiaProSemiBold", size: 14)
                            subTitleLabel.textColor     = ThemeUtil.getStateColor(type: C.ButtonState.NORMAL_STATE)
                            
                            for descriptionString  in subCategory.descriptions
                            {
                                if descriptionString != ""
                                {
                                    posY                        += 25 * ratio
                                    let descriptionLabel        = UILabel(frame: CGRect(x: 110 * ratio, y: posY, width: 350 * ratio, height: 40 * ratio))
                                    
                                    if descriptionString.contains("brand:description:")
                                    {
                                        descriptionLabel.text   = descriptionString.replacingOccurrences(of: "brand:description:", with: "")

                                        let productImageView            = UIImageView(frame: CGRect(x: 110 * ratio, y: posY  , width: 63 * ratio, height: 32 * ratio))

                                        if (subCategory.brands?.count)! > 0
                                        {
                                            for brand in subCategory.brands!
                                            {
                                                if brand.products.count > 0
                                                {
                                                    for product in brand.products
                                                    {
                                                        if descriptionLabel.text == product.name
                                                        {
                                                            productImageView.contentMode    = .scaleAspectFit
                                                            productImageView.af_setImage(withURL: URL(string:C.BASE_URL + product.image_url)!)
                                                            descriptionLabel.text = brand.name + " " + descriptionLabel.text!
                                                            descriptionLabel.frame          = CGRect(x: 155 * ratio, y: posY, width: 305 * ratio, height: 40 * ratio)
                                                            contentView.addSubview(productImageView)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        descriptionLabel.text           = descriptionString
                                    }

                                    descriptionLabel.font       = UIFont(name: "SofiaProLight", size: 18)
                                    descriptionLabel.textColor  = ThemeUtil.getHairFirstStepLabelColor()
                                    descriptionLabel.numberOfLines  = 0
                                    
                                    contentView.addSubview(descriptionLabel)
                                }
                            }
                            
                            contentView.addSubview(subOrderImageView)
                            contentView.addSubview(subOrderLabel)
                            contentView.addSubview(subTitleLabel)
                        }
                        else
                        {
                            let view                = UIView(frame: CGRect(x: (81 + CGFloat(addedIndex) * 150) * ratio, y: posY, width: 145 * ratio, height: 40 * ratio))
                            view.layer.borderWidth  = 2;
                            view.layer.borderColor  = ThemeUtil.getHairSectionBorderColor().cgColor
                            
                            let reduceSize          = CGSize(width: 20 * ratio, height: 20 * ratio)
                            let subImage            = UIImage(named:"image_hair_" + String(stepIndex + 1) + "_" + String(categoryIndex + 1) + "_" + String(subCategoryIndex + 1) + ".png")?.resizeImage(toSize: reduceSize)
                            
                            var labelX:CGFloat      = 0
                            
                            if subImage != nil
                            {
                                let subImageView    = UIImageView(frame: CGRect(x: 15 * ratio, y: (40 - (subImage?.size.height)!) / 2 * ratio, width: (subImage?.size.width)! * ratio, height: (subImage?.size.height)! * ratio))
                                subImageView.image  = subImage
                                labelX              = 40
                                
                                view.addSubview(subImageView)
                            }
                            
                            let subLabel            = UILabel(frame: CGRect(x: labelX * ratio, y: 5 * ratio, width: (140 - labelX) * ratio, height: 30))
                            subLabel.numberOfLines  = 0
                            subLabel.text           = subCategory.name
                            subLabel.font           = UIFont(name: "SofiaProSemiBold", size: 13)
                            subLabel.textColor      = ThemeUtil.getStateColor(type: C.ButtonState.NORMAL_STATE)
                            subLabel.textAlignment  = .center
                            
                            print("sublabel = \(subCategory.name) addedIndex = \(addedIndex)")
                            addedIndex              += 1
                            
                            view.addSubview(subLabel)
                            
                            contentView.addSubview(view)
                        }
                    }
                }
                
                if selectedFound
                {
                    posY        += 45 * ratio
                    contentView.addSubview(categoryTitleLabel)
                }
                else
                {
                    posY        -= 27 * ratio
                }
                
            }
            
            if addedStepTitle
            {
                contentView.addSubview(stepTitleLabel)
            }
            else
            {
                posY            -= 27 * ratio
            }
        }
        
        contentView.addSubview(orderImageView)
        contentView.addSubview(orderLabel)
        contentView.addSubview(mainTitleLabel)
    }   
}
