//
//  HairProductCell.swift
//  configurator(iPad)
//
//  Created by MobileMaster on 5/23/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit

class HairProductCell: UITableViewCell
{
    @IBOutlet weak var imageForProduct: UIImageView!
    
    @IBOutlet weak var labelForProductName: UILabel!
    @IBOutlet weak var labelForProductDescription: UILabel!
    @IBOutlet weak var buttonForSelect: UIButton!
    
    public var product: Product!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func buttonClicked(_ sender: UIButton)
    {
        self.product.selected    = !self.product.selected
        
        if self.product.selected
        {
            self.buttonForSelect .setImage(UIImage(named: "image_hair_product_checked"), for: .normal)
        }
        else
        {
            self.buttonForSelect .setImage(UIImage(named: "image_hair_product_unchecked"), for: .normal)
        }
    }
}
