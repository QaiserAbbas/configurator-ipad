//
//  WorkspaceHairSectionViewController.swift
//  configurator(iPad)
//
//  Created by MobileMaster on 4/20/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol WorkspaceHairSectionDelegate
{
    func completeHairSection(viewController: WorkspaceHairSectionViewController)
}

class WorkspaceHairSectionViewController: UIViewController, UIScrollViewDelegate, HairProductCalendarDelegate
{
    @IBOutlet var viewForHairSection: UIView!
    
    @IBOutlet weak var viewForClient: UIView!
    @IBOutlet weak var imageForClient: UIImageView!
    @IBOutlet weak var imageForBlink1: UIImageView!
    @IBOutlet weak var imageForBlink2: UIImageView!
    @IBOutlet weak var imageForBlink3: UIImageView!
    @IBOutlet weak var imageForBlink4: UIImageView!
    @IBOutlet weak var imageForBlink5: UIImageView!

    @IBOutlet weak var viewForStep1: UIView!
    @IBOutlet weak var viewForStep2: UIView!
    @IBOutlet weak var viewForStep3: UIView!
    @IBOutlet weak var viewForStep4: UIView!
    
    @IBOutlet weak var scrollviewForStep2: UIScrollView!
    @IBOutlet weak var scrollviewForStep3: UIScrollView!
    @IBOutlet weak var scrollviewForStep4: UIScrollView!
    
    @IBOutlet weak var textfieldForStep_1_2: UITextField!
    @IBOutlet weak var textfieldForStep_3_1: UITextField!
    
    @IBOutlet weak var scrollviewForStep4_1: UIScrollView!
    
    @IBOutlet weak var textfieldForStep4_2_1: UITextField!
    @IBOutlet weak var textfieldForStep4_2_2: UITextField!
    @IBOutlet weak var textfieldForStep4_2_3: UITextField!
    @IBOutlet weak var textfieldForStep4_2_4: UITextField!
    
    @IBOutlet weak var buttonForStep4Prev: UIButton!
    @IBOutlet weak var buttonForStep4Next: UIButton!
    @IBOutlet weak var buttonForCalendar: UIButton!
    
    var delegate:WorkspaceHairSectionDelegate?
    
    public var mainViewController: MainViewController!
    
    public var client:Client        = Client()
    private var questionsObj        =   HairQuestions()
    
    private var step                =   1
    private var tagList             =   [[[1, 2, 3, 4, 5, 6, 7, 8, 9]],
                                         [[101, 102, 103], [131, 132], [161, 162, 163]],
                                         [[201, 202], [231, 232, 233, 234], [261, 262, 263, 264, 265], [291, 292]],
                                         [[400, 401, 402, 403, 404, 405, 406, 407, 408]]
                                        ]

    private var imageViewFrame: CGRect!
    
    var ratio:CGFloat               =   0
    var stopBlink                   =   false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        ratio                       =   CGFloat(self.view.frame.size.height / 768)
        NotificationCenter.default.addObserver(self, selector: #selector(self.stopBlink(_:)), name: NSNotification.Name(rawValue: C.HAIR_STOP_BLINK), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideAnimation(_:)), name: NSNotification.Name(rawValue: C.HAIR_HIDE_ANIMATION), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.addDescription(_:)), name: NSNotification.Name(rawValue: C.HAIR_ADD_PRODUCT_DESCRIPTION), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.productsDownloaded(_:)), name: NSNotification.Name(rawValue: C.CBP_LIST_DOWNLOADED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.productEmptyDownloaded(_:)), name: NSNotification.Name(rawValue: C.CBP_LIST_EMPTY_DOWNLOADED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.productDownloadedFailed(_:)), name: NSNotification.Name(rawValue: C.CBP_LIST_FAILED_DOWNLOADED), object: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func stopBlink(_ notification: NSNotification)
    {
        stopBlink   = true
    }
    
    @objc func hideAnimation(_ notification: NSNotification)
    {
        stopBlink                   = true
        let imageFrame              = self.imageForClient.frame
        var stepView                = UIView()
        
        if !self.viewForStep1.isHidden
        {
            stepView                = self.viewForStep1
        }
        else if !self.viewForStep2.isHidden
        {
            stepView                = self.viewForStep2
        }
        else if !self.viewForStep3.isHidden
        {
            stepView                = self.viewForStep3
        }
        else if !self.viewForStep4.isHidden
        {
            stepView                = self.viewForStep4
        }
        
        UIView.animate(withDuration: 0.7, animations:
        {
            self.imageForClient.frame   = CGRect(x: self.imageViewFrame.origin.x, y: self.imageViewFrame.origin.y, width: imageFrame.width, height: imageFrame.height)
        }, completion: { (finished: Bool) in
            let mode: Int                       =  notification.userInfo?["mode"] as! Int
            print("mode = \(mode)")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.HAIR_HINE_ANIMATION_DONE), object: self,  userInfo: notification.userInfo)
        })
        
        let frame                   = stepView.frame
        
        UIView.animate(withDuration: 0.7, animations:
        {
            stepView.frame     = CGRect(x: self.viewForHairSection.frame.size.width, y: frame.origin.y, width: frame.size.width, height: frame.size.height)
        }, completion: { (finished: Bool) in
        })
    }
    
    @objc func addDescription(_ notification: NSNotification)
    {
        for subview in self.scrollviewForStep4.subviews
        {
            if subview.tag != 422999
            {
                subview.removeFromSuperview()
            }
        }
        
        initStep4_2ScrollView()
    }
    
    public func initBackgroundImage(image: UIImage, frame: CGRect)
    {
        let imageFrame              = image.size
        let centerPosX              = (self.viewForClient.frame.size.width  - imageFrame.width * UIScreen.main.scale) / 2

        self.imageForClient.image   = image
        
        self.imageForClient.frame   = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.size.width, height: frame.size.height)
        
        UIView.animate(withDuration: 0.7, animations:
        {
            self.imageForClient.frame   = CGRect(x: centerPosX, y: frame.origin.y, width: imageFrame.width * UIScreen.main.scale, height: imageFrame.height * UIScreen.main.scale)
        }, completion: { (finished: Bool) in
            self.startBlinkStarAnimation()
        })
        
        self.imageViewFrame         = frame
    }
    
    public func initQuestionsObj(hairQuestions: HairQuestions)
    {
        questionsObj    = hairQuestions
        stopBlink       = false
        
        self.imageForBlink1.alpha   = 0
        self.imageForBlink2.alpha   = 0
        self.imageForBlink3.alpha   = 0
        self.imageForBlink4.alpha   = 0
        self.imageForBlink5.alpha   = 0
        
        if questionsObj.steps.count == 0
        {
            createQuestionsObj()
        }
        
        initAllStepsButtons()
        initStep4_1ScrollView()
        initStep4_2ScrollView()
        loadCBPList()
        
        let paddingView_1_2                     = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.textfieldForStep_1_2.frame.height))
        let paddingView_3_1                     = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.textfieldForStep_3_1.frame.height))
        
        self.textfieldForStep_1_2.leftView      = paddingView_1_2
        self.textfieldForStep_1_2.leftViewMode  = UITextFieldViewMode.always
        self.textfieldForStep_3_1.leftView      = paddingView_3_1
        self.textfieldForStep_3_1.leftViewMode  = UITextFieldViewMode.always
        
        self.textfieldForStep_1_2.text  = questionsObj.steps[0].categories[0].description
        self.textfieldForStep_3_1.text  = questionsObj.steps[2].categories[0].description

        let frame                   = self.viewForStep1.frame
        self.viewForStep1.frame     = CGRect(x: self.viewForHairSection.frame.size.width, y: frame.origin.y, width: frame.size.width, height: frame.size.height)

        UIView.animate(withDuration: 0.7, animations:
        {
            self.viewForStep1.frame   = frame
        }, completion: { (finished: Bool) in
            
        })
//        print("hairSteps = \(questionsObj.toJson())")
    }
    
    private func startBlinkStarAnimation()
    {
        blink1()
        blink2()
        blink3()
        blink4()
        blink5()
    }
    
    func blink1()
    {
        self.imageForBlink1.alpha = 1.0
        UIView.animate(withDuration: 1, animations:{
            self.imageForBlink1.alpha = 0.3
        }, completion: {
            (value: Bool) in
            UIView.animate(withDuration: 1.3, animations:{
                self.imageForBlink1.alpha = 1.0
            }, completion: {
                (value: Bool) in
                if !self.stopBlink
                {                    
                    self.blink1()
                }
            })
        })
    }
    
    func blink2()
    {
        self.imageForBlink2.alpha = 0.5
        UIView.animate(withDuration: 1.2, animations:{
            self.imageForBlink2.alpha = 1.0
        }, completion: {
            (value: Bool) in
            UIView.animate(withDuration: 1.7, animations:{
                self.imageForBlink2.alpha = 0.5
            }, completion: {
                (value: Bool) in
                if !self.stopBlink
                {
                    self.blink2()
                }
            })
        })
    }
    
    func blink3()
    {
        self.imageForBlink3.alpha = 1.0
        UIView.animate(withDuration: 1, animations:{
            self.imageForBlink3.alpha = 0.3
        }, completion: {
            (value: Bool) in
            UIView.animate(withDuration: 1.5, animations:{
                self.imageForBlink3.alpha = 1.0
            }, completion: {
                (value: Bool) in
                if !self.stopBlink
                {
                    self.blink3()
                }
            })
        })
    }
    
    func blink4()
    {
        self.imageForBlink4.alpha = 0.6
        UIView.animate(withDuration: 1.3, animations:{
            self.imageForBlink4.alpha = 1.0
        }, completion: {
            (value: Bool) in
            UIView.animate(withDuration: 1.2, animations:{
                self.imageForBlink4.alpha = 0.6
            }, completion: {
                (value: Bool) in
                if !self.stopBlink
                {
                    self.blink4()
                }
            })
        })
    }
    
    func blink5()
    {
        self.imageForBlink5.alpha = 1.0
        UIView.animate(withDuration: 1.1, animations:{
            self.imageForBlink5.alpha = 0.3
        }, completion: {
            (value: Bool) in
            UIView.animate(withDuration:1.8, animations:{
                self.imageForBlink5.alpha = 1.0
            }, completion: {
                (value: Bool) in
                if !self.stopBlink
                {
                    self.blink5()
                }
            })
        })
    }
    
    private func hideAllViews()
    {
        self.viewForStep1.isHidden  = true
        self.viewForStep2.isHidden  = true
        self.viewForStep3.isHidden  = true
        self.viewForStep4.isHidden  = true
    }
    
    private func showActiveView()
    {
        hideAllViews()
        switch step
        {
        case 1:
            self.viewForStep1.isHidden  = false
            break
        case 2:
            self.viewForStep2.isHidden  = false
            break
        case 3:
            self.viewForStep3.isHidden  = false
            self.scrollviewForStep3.contentSize  = CGSize(width: self.scrollviewForStep3.frame.width, height: self.scrollviewForStep3.frame.height + 105)
            break
        case 4:
            if self.client.beauty_treatment_date != ""
            {
                self.buttonForCalendar.setImage(UIImage.init(named: "button_calendar_selected"), for: .normal)
            }
            
            self.viewForStep4.isHidden  = false
            break
        case 5:
            stopBlink   = true
            questionsObj.steps[0].categories[0].description                     = self.textfieldForStep_1_2.text!
            questionsObj.steps[2].categories[0].description                     = self.textfieldForStep_3_1.text!

            saveStep4_2Descriptions()
            
            delegate?.completeHairSection(viewController: self)
            break
        default:
            break
        }
    }
    
    private func loadCBPList ()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ClientService.getCategoryBrandProductList(user: UserManager.getLoggedInUser()!)
    }
    
    @objc func productsDownloaded(_ notification: NSNotification)
    {
        MBProgressHUD.hide(for: self.view, animated: true)
        
        let cbpList     = ClientManager.getCBPList()
        for index in 0 ... C.CATEGORY_LIST.count - 1
        {
            let name                    = C.CATEGORY_LIST[index]
            
            for cbpCategory in cbpList
            {
                if cbpCategory.name == name
                {
                    questionsObj.steps[3].categories[0].subCategories[index]         = cbpCategory
                }
            }
        }
    }
    
    @objc func productEmptyDownloaded(_ notification: NSNotification)
    {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    @objc func productDownloadedFailed(_ notification: NSNotification)
    {
        self.view.makeToast("Failed to download products")
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    private func createQuestionsObj()
    {
        let stepNameList            = ["Allgemein", "Hautanamese", "Hautdiagnostik", "Empfehlung"]
        let categoryNameList        = [["Was möchten Sie gerne an ihrem Hautbild verbessern?"],
                                       ["Hauttyp", "Durchblutung", "Hautspannung"],
                                       ["Feuchtigkeit", "Komedonen", "Affektionen", "Pigmentstörungen"],
                                       ["Individueller kosmetischer Behandlungsplan"]]
        let subCategoryNameList     = [[["Glattes Hautbild", "Unreine Haut", "Poren Verfeinerung", "Feuchtigkeit Glanz", "Pickel / Akne", "empfindliche Haut Allergien", "Pigmentierung", "Hautalterung", "Narben Hautschädigung"]],
                                       [["Normal", "Fettig", "Trocken"], ["Gute Durchblutung", "Schlechte Durchblutung"], ["Gut", "Schwach", "Alternd"]],
                                       [["Ingesamt gut", "Ingesamt dehydriert"], ["Akne", "verstopfte Poren", "Jugendliche Akne", "Alters Akne"], ["Rosazea", "Teleangiektasie", "Spinnennävus", "Feuermale", "Sonstige"], ["Hyperpigmentierung", "Hypopigmentierung"]],
                                       [["Reinigung", "Peeling", "Toner", "Tagespflege", "Nachtpflege", "Augenpflege", "Serum", "Maske",
                                         "Individuelle Empfehlung"]]]
        
//        let productsList            = [[["Teoxane RHA™ Micellar Solution", "Reinigungslösung für alle Hauttypen"]],
//                                       [["Teoxane Radiant Night Peel", "Sanfte Peelingcreme mit Glykolsäure (Klasse I Medizinprodukt)                                        mit 15% A.H.A.-Glycolsäure"]],
//                                       [[]],
//                                       [["Teoxane RHA® X VCIP Serum", "Hochkonzentriertes Vitamin C Serum kombiniert mit resilienter Hyaluronsäure (RHA®)"], ["Teoxane Advanced Filler Perfecting Shield SPF 30", "Schützende Pflege für einen perfekten Teint mit LSF 30"], ["Teoxane Advanced Filler - Trockene bis sehr trockene Haut", "Hautstrukturierende Anti-Aging-Creme für die tägliche Anwendung"], ["Teoxane  Advanced Filler – Normale bis Mischhaut", "Hautstrukturierende Anti-Aging-Creme für die tägliche Anwendung"]],
//                                       [["Teoxane Perfect Skin Refiner", "Creme zur Hautregeneration. Mit 10% A.H.A.-Glykolsäure"], ["Teoxane RHA® X VCIP Serum", "Hochkonzentriertes Vitamin C Serum kombiniert mit resilienter Hyaluronsäure (RHA®)"], ["Teoxane Radiant Night Peel", "Sanfte Peelingcreme mit Glykolsäure. Mit 15% A.H.A.-Glycolsäure"], ["Teoxane Deep Repair Balm", "Hautberuhigender Balsam"]],
//                                       [["Teoxane RHA® X VCIP Serum", "Hochkonzentriertes Vitamin C Serum kombiniert mit resilienter Hyaluronsäure (RHA®)"], ["Teoxane RHA™ Serum", "Hautregenerierendes Serum"]],
//                                       [["Teoxane RHA Advanced Eye Contour R [II] Eyes", "Brighten Up Your Eyes."]],
//                                       [["Teoxane RHA® Hydrogel Mask", "Feuchtigkeitsmaske für Gesicht und Hals."]],
//                                       [[]]
//                                        ]
        
        for stepIndex in 0 ... tagList.count - 1
        {
            let step                = Step()
            step.name               = stepNameList[stepIndex]
            
            for categoryIndex in 0 ... tagList[stepIndex].count - 1
            {
                let category        = Category()
                category.name       = categoryNameList[stepIndex][categoryIndex]
                
                for tagIndex in 0 ... tagList[stepIndex][categoryIndex].count - 1
                {
                    let subCategory     = SubCategory()
                    subCategory.tag     = tagList[stepIndex][categoryIndex][tagIndex]
                    subCategory.name    = subCategoryNameList[stepIndex][categoryIndex][tagIndex]
                    
//                    if stepIndex == 3
//                    {
//                        let brand       = Brand()
//                        brand.name      = "Teoxane"
//                        
//                        for productIndex in 0 ... productsList[tagIndex].count - 1
//                        {
//                            if productsList[tagIndex][productIndex].count > 0
//                            {
//                                let product     = Product()
//                                product.name    = productsList[tagIndex][productIndex][0]
//                                product.descriptions    = productsList[tagIndex][productIndex][1]
//                                product.image_url       = "image_hair_product_" + String(tagIndex + 1) + "_1_" + String(productIndex + 1)
//                                
//                                brand.products.append(product)
//                            }
//                        }
//                        
//                        subCategory.brands      = []
//                        subCategory.brands?.append(brand)
//                    }
                    
                    category.subCategories.append(subCategory)
                }
                
                step.categories.append(category)
            }
            questionsObj.steps.append(step)
        }
        print("hairSteps = \(questionsObj.toJson())")
    }
    
    private func initAllStepsButtons()
    {
        for stepIndex in 1 ... questionsObj.steps.count
        {
            for categoryIndex in 1 ... questionsObj.steps[stepIndex - 1].categories.count
            {
                initStepCategoryButtons(stepIndex: stepIndex, categoryIndex: categoryIndex)
            }
        }
    }
    
    private func initStepCategoryButtons(stepIndex: Int, categoryIndex: Int)
    {
        for tagIndex in 1 ... questionsObj.steps[stepIndex - 1].categories[categoryIndex - 1].subCategories.count
        {
            let subCategory     = questionsObj.steps[stepIndex - 1].categories[categoryIndex - 1].subCategories[tagIndex - 1]
            let tag             = subCategory.tag
            print("stepIndex = \(stepIndex) categoryIndex = \(categoryIndex) tag = \(tag)")
            if subCategory.selected
            {
                selectStepCategorySubcategory(stepIndex: stepIndex, categoryIndex: categoryIndex, tagIndex: tagIndex)
            }
            else
            {
                initStepButton(stepIndex: stepIndex, categoryIndex: categoryIndex, tagIndex: tagIndex, tag: tag)
            }
        }
    }
    
    private func initStepButton(stepIndex: Int, categoryIndex: Int, tagIndex: Int, tag :Int)
    {
        let button  = self.view.viewWithTag(tag) as? UIButton
        let image   = self.view.viewWithTag(10 + tag) as? UIImageView
        let label   = self.view.viewWithTag(10 * 2 + tag) as? UILabel
        
        button?.backgroundColor     = ThemeUtil.getModeColor(type: C.MainMode.MAIN_INIT)
        button?.layer.borderWidth   = 2;
        button?.layer.borderColor   = ThemeUtil.getHairSectionBorderColor().cgColor
        
        if (label != nil)
        {
            if (stepIndex == 1)
            {
                label?.textColor    = ThemeUtil.getHairFirstStepLabelColor()
            }
            else
            {
                label?.textColor    = ThemeUtil.getHairOtherStepLabelColor()
            }
        }
        
        if (image != nil)
        {
            image?.image            = UIImage(named: "image_hair_" + String(stepIndex) + "_" + String(categoryIndex) + "_" + String(tagIndex) + ".png")
        }
    }
    
    private func selectStepCategorySubcategory(stepIndex: Int, categoryIndex: Int, tagIndex: Int)
    {
        let tag         = questionsObj.steps[stepIndex - 1].categories[categoryIndex - 1].subCategories[tagIndex - 1].tag
        let button      = self.view.viewWithTag(tag) as? UIButton
        let image       = self.view.viewWithTag(10 + tag) as? UIImageView
        let label       = self.view.viewWithTag(10 * 2 + tag) as? UILabel
        
        button?.backgroundColor     = ThemeUtil.getModeColor(type: C.MainMode.MAIN_HAIR)
        button?.layer.borderWidth   = 0;
        
        if (label != nil)
        {
            label?.textColor        = ThemeUtil.getStateColor(type: C.ButtonState.SELECTED_STATE)
        }
        
        if (image != nil)
        {
            image?.image            = UIImage(named: "image_hair_" + String(stepIndex) + "_" + String(categoryIndex) + "_" + String(tagIndex) + "_selected.png")
        }
    }
    
    private func initStep4_1ScrollView()
    {
        let viewWidth:CGFloat       = 140
        let viewHeight:CGFloat      = 48
       
        for index in 0 ... C.CATEGORY_LIST.count - 1
        {
            let name                    = C.CATEGORY_LIST[index]
            let subCategory             = questionsObj.steps[3].categories[0].subCategories[index]
            subCategory.name            = name
            
            let view                    = UIView(frame: CGRect(x: CGFloat(index) * 145 * ratio, y: 0, width: viewWidth * ratio, height: viewHeight * ratio))
            
            let button                  = UIButton(frame: CGRect(x: 0, y: 0, width: viewWidth * ratio, height: viewHeight * ratio))
            button.layer.borderWidth    = 2;
            button.layer.borderColor    = ThemeUtil.getMainCategoryBorderColor().cgColor
            
            button.addTarget(self, action: #selector(step4_1ButtonsClicked(_:)), for: .touchUpInside)
            
            let image                   = UIImage(named:"image_hair_checked.png")
            let imageView               = UIImageView(frame: CGRect(x: 17 * ratio, y: (viewHeight - image!.size.height) / 2 * ratio, width: image!.size.width * ratio, height: image!.size.height * ratio))
            imageView.image             = image
            
            let labelX                  = 25 * ratio + imageView.frame.width
            let label                   = UILabel(frame: CGRect(x: labelX, y: 0, width: view.frame.width - labelX - 5, height: viewHeight * ratio))
            label.numberOfLines         = 0
            label.text                  = name
            label.font                  = UIFont(name: "SofiaProSemiBold", size: 15)
            label.textColor             = ThemeUtil.getMainCategoryLabelColor()
            
            button.tag                  = 400 + index
            imageView.tag               = 410 + index
            label.tag                   = 420 + index
            
            view.addSubview(button)
            view.addSubview(imageView)
            view.addSubview(label)
            
            self.scrollviewForStep4_1.addSubview(view)
            
            if subCategory.selected
            {
                label.textColor             = ThemeUtil.getStateColor(type: C.ButtonState.SELECTED_STATE)
                button.backgroundColor      = ThemeUtil.getModeColor(type: C.MainMode.MAIN_HAIR)
                button.layer.borderWidth    = 0
                button.isSelected           = true
            }
            else
            {
                label.textColor             = ThemeUtil.getMainCategoryLabelColor()
                button.backgroundColor      = ThemeUtil.getModeColor(type: C.MainMode.MAIN_INIT)
                button.layer.borderWidth    = 2
                button.isSelected           = false
                
                let descriptionView         = self.scrollviewForStep4.viewWithTag(400 + index)
                descriptionView?.isHidden   = true
            }
        }
        
        self.scrollviewForStep4_1.contentSize  = CGSize(width: 145 * CGFloat(C.CATEGORY_LIST.count) * ratio, height: viewHeight * ratio)
    }
   
    private func initStep4_2ScrollView()
    {
        var uniqueIndex             = 0
        var posY:CGFloat            = 52
        
        for index in 0 ... questionsObj.steps[3].categories[0].subCategories.count - 1
        {
            let subCategory             = questionsObj.steps[3].categories[0].subCategories[index]
            
            if subCategory.selected
            {
                uniqueIndex             += 1
                let descriptionView     = createDescriptionView4_2(index: index, uniqueIndex: uniqueIndex)
                descriptionView.frame   = CGRect(x: 47 * ratio, y: posY * ratio, width: descriptionView.frame.size.width, height: descriptionView.frame.size.height)
                posY                    += descriptionView.frame.size.height + 20
                
                self.scrollviewForStep4.addSubview(descriptionView)
            }
            else
            {
                subCategory.descriptions    = [""]
            }
        }
        
        self.scrollviewForStep4.contentSize  = CGSize(width: self.scrollviewForStep4.frame.width, height: posY)
    }
    
    private func findBrandImageUrl(productName : String) -> String
    {
        let subCategories           = questionsObj.steps[3].categories[0].subCategories
        var brandImage              = ""
        
        for subCategory in subCategories
        {
            if (subCategory.brands != nil)
            {
                for brand in subCategory.brands!
                {
                    for product in brand.products
                    {
                        if product.name == productName
                        {
                            if brand.image_url == ""
                            {
                                brandImage  = product.image_url
                            }
                            else
                            {
                                brandImage  = brand.image_url
                            }
                        }
                    }
                }
            }
        }
        
        return brandImage
    }
    
    private func createDescriptionView4_2(index: Int, uniqueIndex: Int) -> UIView
    {
        let subCategory             = questionsObj.steps[3].categories[0].subCategories[index]
        let view                    = UIView(frame: CGRect(x: 0, y: 0, width: 471 * ratio, height: 97 * ratio))
        
        let image                   = UIImage(named:"image_hair_step_active.png")
        let imageView               = UIImageView(frame: CGRect(x: 0, y: 7 * ratio, width: 29 * ratio, height: 29 * ratio))
        imageView.image             = image
        
        let countLabel              = UILabel(frame: CGRect(x: 0, y: 7 * ratio, width: 29 * ratio, height: 29 * ratio))
        countLabel.text             = String(uniqueIndex)
        countLabel.font             = UIFont(name: "SofiaProSemiBold", size: 18)
        countLabel.textColor        = UIColor.white
        countLabel.textAlignment    = .center

        let titleLabel              = UILabel(frame: CGRect(x: 50 * ratio, y: 10 * ratio, width: 350 * ratio, height: 22 * ratio))
        titleLabel.text             = subCategory.name
        titleLabel.font             = UIFont(name: "SofiaProSemiBold", size: 18)
        titleLabel.textColor        = ThemeUtil.getStateColor(type: C.ButtonState.NORMAL_STATE)
        
        for descriptionIndex in 0 ... subCategory.descriptions.count - 1
        {
            let textfield           = UITextField(frame: CGRect(x: 50 * ratio, y: (38 + 73 * CGFloat(descriptionIndex)) * ratio, width: 370 * ratio, height: 58 * ratio))
            var description         = subCategory.descriptions[descriptionIndex]
            textfield.font          = UIFont(name: "SofiaProRegular", size: 18)
            textfield.background    = UIImage(named:"textfield_login.png")
            textfield.borderStyle   = .none
            
            textfield.leftViewMode  = UITextFieldViewMode.always
            textfield.tag           = 421000 + index * 100 + descriptionIndex
            
            let rightPaddingView    = UIView(frame: CGRect(x: 0, y: 0, width: 70 * ratio, height: 39 * ratio))
            let productButton       = UIButton(frame: CGRect(x: 15, y: 0, width: 39 * ratio, height: 39 * ratio))
            
            if description.contains("brand:description:")
            {
                description             = description.replacingOccurrences(of: "brand:description:", with: "")
                let leftPaddingView     = UIView(frame: CGRect(x: 0, y: 0, width: 45 * ratio, height: textfield.frame.height))
                let brandImageView      = UIImageView(frame: CGRect(x: 4, y: (textfield.frame.height - 18 * ratio) / 2, width: 36 * ratio, height: 18 * ratio))
//                brandImageView.image    = UIImage(named: "image_brand_teoxane")
                brandImageView.af_setImage(withURL: URL(string:C.BASE_URL + findBrandImageUrl(productName: description))!)
                brandImageView.contentMode  = .scaleAspectFit
                leftPaddingView.addSubview(brandImageView)
                textfield.leftView          = leftPaddingView
                
                productButton.setImage(UIImage(named:"button_remove_hair_products.png"), for: .normal)
                productButton.addTarget(self, action: #selector(removeHairProductViewClicked(_:)), for: .touchUpInside)
            }
            else
            {
                let leftPaddingView     = UIView(frame: CGRect(x: 0, y: 0, width: 15 * ratio, height: textfield.frame.height))
                textfield.leftView      = leftPaddingView
                productButton.setImage(UIImage(named:"button_add_hair_products.png"), for: .normal)
                productButton.addTarget(self, action: #selector(addHairProductViewClicked(_:)), for: .touchUpInside)
            }
            
            productButton.tag       = 425000 + index * 100 + descriptionIndex
            rightPaddingView.addSubview(productButton)            
            
            textfield.rightView     = rightPaddingView
            textfield.rightViewMode = UITextFieldViewMode.always
            textfield.text          = description
            let button              = UIButton(frame: CGRect(x: 433 * ratio, y: (48 + 73 * CGFloat(descriptionIndex)) * ratio, width: 39 * ratio, height: 39 * ratio))
            
            if descriptionIndex == 0
            {
                button.setImage(UIImage(named:"image_hair_add.png"), for: .normal)
                button.addTarget(self, action: #selector(addDescriptionViewClicked(_:)), for: .touchUpInside)
            }
            else
            {
                button.setImage(UIImage(named:"image_hair_remove.png"), for: .normal)
                button.addTarget(self, action: #selector(removeDescriptionViewClicked(_:)), for: .touchUpInside)
            }
            
            button.tag              = 422000 + index * 100 + descriptionIndex
            
            view.frame              = CGRect(x: 0, y: 0, width: view.frame.size.width, height: button.frame.origin.y + button.frame.size.height + 1)
            view.addSubview(textfield)
            view.addSubview(button)
        }
        
        view.addSubview(imageView)
        view.addSubview(countLabel)
        view.addSubview(titleLabel)
        
        return view
    }
    
    private func saveStep4_2Descriptions()
    {
        for index in 0 ... questionsObj.steps[3].categories[0].subCategories.count - 1
        {
            let subCategory     = questionsObj.steps[3].categories[0].subCategories[index]
            
            if subCategory.selected
            {
                for descriptionIndex in 0 ... subCategory.descriptions.count - 1
                {
                    let tag         = 421000 + index * 100 + descriptionIndex
                    let textfield   = self.scrollviewForStep4.viewWithTag(tag) as? UITextField
                    
                    if !subCategory.descriptions[descriptionIndex].contains("brand:description:")
                    {
                        subCategory.descriptions[descriptionIndex]  = (textfield?.text)!
                    }
                }
            }
        }
    }
    
    func completeSettingCalendar(viewController: HairProductCalendarViewController)
    {
        if self.client.beauty_treatment_date == ""
        {
            self.buttonForCalendar.setImage(UIImage.init(named: "button_calendar"), for: .normal)
        }
        else
        {
            self.buttonForCalendar.setImage(UIImage.init(named: "button_calendar_selected"), for: .normal)
        }
    }
    
    @IBAction func step4_1ButtonsClicked(_ sender: UIButton)
    {
        let tag             = sender.tag
        let subCategory     = questionsObj.steps[3].categories[0].subCategories[tag - 400]

        saveStep4_2Descriptions()
        
        for subview in self.scrollviewForStep4.subviews
        {
            if subview.tag != 422999
            {
                subview.removeFromSuperview()
            }
        }
        
        for subview in self.scrollviewForStep4_1.subviews
        {
            subview.removeFromSuperview()
        }
        
        subCategory.selected = !subCategory.selected
        initStep4_1ScrollView()
        initStep4_2ScrollView()
    }

    @IBAction func addHairProductViewClicked(_ sender: UIButton)
    {
        let descriptionIndex    = sender.tag % 100
        let tagIndex            = (sender.tag / 100) % 10
        print("tag = \(sender.tag) tagIndex = \(tagIndex)")
        let subCategory         = questionsObj.steps[3].categories[0].subCategories[tagIndex]
        
        mainViewController.setupHairProductSelectView(subCategory: subCategory, descriptionIndex: descriptionIndex)
    }

    @IBAction func removeHairProductViewClicked(_ sender: UIButton)
    {
        let descriptionIndex    = sender.tag % 100
        let tagIndex            = (sender.tag / 100) % 10
        print("tag = \(sender.tag) tagIndex = \(tagIndex)")
        let subCategory         = questionsObj.steps[3].categories[0].subCategories[tagIndex]
        
        saveStep4_2Descriptions()
        
        for subview in self.scrollviewForStep4.subviews
        {
            if subview.tag != 422999
            {
                subview.removeFromSuperview()
            }
        }
        
        subCategory.descriptions[descriptionIndex] = ""
        initStep4_2ScrollView()
    }
    
    @IBAction func addDescriptionViewClicked(_ sender: UIButton)
    {
        let tagIndex        = (sender.tag / 100) % 10
        print("tag = \(sender.tag) tagIndex = \(tagIndex)")
        let subCategory     = questionsObj.steps[3].categories[0].subCategories[tagIndex]
        saveStep4_2Descriptions()
        
        for subview in self.scrollviewForStep4.subviews
        {
            if subview.tag != 422999
            {
                subview.removeFromSuperview()
            }
        }

        subCategory.descriptions.append("")
        initStep4_2ScrollView()
    }

    @IBAction func removeDescriptionViewClicked(_ sender: UIButton)
    {
        let descriptionIndex    = sender.tag % 100
        let tagIndex            = (sender.tag / 100) % 10
        
        print("tag = \(sender.tag) tagIndex = \(tagIndex)")
        let subCategory         = questionsObj.steps[3].categories[0].subCategories[tagIndex]
        saveStep4_2Descriptions()
        
        for subview in self.scrollviewForStep4.subviews
        {
            if subview.tag != 422999
            {
                subview.removeFromSuperview()
            }
        }
        
        subCategory.descriptions.remove(at: descriptionIndex)
        initStep4_2ScrollView()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let contentOffsetX              = scrollView.contentOffset.x
        
        if (contentOffsetX <= 0)
        {
            self.buttonForStep4Prev.isHidden    = true
        }
        else if (contentOffsetX >= 108 * ratio)
        {
            self.buttonForStep4Next.isHidden    = true
        }
        else
        {
            self.buttonForStep4Prev.isHidden    = false
            self.buttonForStep4Next.isHidden    = false
        }
    }
    
    @IBAction func questionClicked(_ sender: UIButton)
    {
        let tag             = sender.tag
        let stepIndex       = tag / 100 + 1
        let categoryIndex   = (tag - (stepIndex - 1) * 100) / 30 + 1
        let tagIndex        = tag  - (stepIndex - 1) * 100 - (categoryIndex - 1) * 30
        
        let category        = questionsObj.steps[stepIndex - 1].categories[categoryIndex - 1]
        
        for index in 0 ... category.subCategories.count - 1
        {
            let subCategory = category.subCategories[index]
            if index == tagIndex - 1
            {
                if (stepIndex == 1 || (stepIndex == 3 && (categoryIndex == 2 || categoryIndex == 3)))
                {
                    subCategory.selected    = !subCategory.selected
                }
                else
                {
                    subCategory.selected    = true
                }
            }
            else if(stepIndex != 1 && !(stepIndex == 3 && (categoryIndex == 2 || categoryIndex == 3)))
            {
                subCategory.selected    = false
            }
        }

        initStepCategoryButtons(stepIndex: stepIndex, categoryIndex: categoryIndex)
    }
    
    @IBAction func addDescriptionStep4_2(_ sender: UIButton)
    {
        let tag             = sender.tag
        
        switch tag
        {
        case 491:
            break
        case 492:
            break
        default:
            break
        }
    }
    
    @IBAction func prevOrNextButtonClicked(_ sender: UIButton)
    {
        step    = sender.tag - 1000
        showActiveView()
        print("hairSteps = \(questionsObj.toJson())")
    }
    
    @IBAction func buttonCalendarClicked(_ sender: UIButton)
    {
        mainViewController.setupHairProductCalendarView(hairSectionView: self)
    }
}
