//
//  EditClientViewController.swift
//  configurator(iPad)
//
//  Created by MobileMaster on 1/20/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit
import Toast_Swift
import ActionSheetPicker_3_0

class EditClientViewController: UIViewController, UITextFieldDelegate
{
    @IBOutlet weak var buttonForFemale: UIButton!
    @IBOutlet weak var imageForFemale: UIImageView!
    @IBOutlet weak var labelForFemale: UILabel!
    @IBOutlet weak var buttonForMale: UIButton!
    @IBOutlet weak var imageForMale: UIImageView!
    @IBOutlet weak var labelForMale: UILabel!
    @IBOutlet weak var imageForModelMask: UIImageView!
    @IBOutlet weak var textForClientName: UITextField!
    @IBOutlet weak var textForPhone: UITextField!
    @IBOutlet weak var textForEmail: UITextField!
    @IBOutlet weak var textForAddress: UITextField!
    @IBOutlet weak var textForPostal: UITextField!
    @IBOutlet weak var textForCity: UITextField!
    @IBOutlet weak var textForCountry: UITextField!
    @IBOutlet weak var textForBirthdate: UITextField!
    @IBOutlet weak var viewForEditAccount: UIView!
    @IBOutlet weak var viewForDialog: UIView!
    
    @IBOutlet weak var buttonForSave: UIButton!
    
    public  var mainViewController: MainViewController!
    
    private var selectedModelUrl: String    = "/upload/model_1.png"
    private var genderIndex: Int            = 0
    private var clientImage: UIImage?
    private var upload: Bool                = false
    private var updateImage: Bool           = false
    private var client:Client               = Client()
    private var targetSize:CGSize?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.targetSize                     = self.viewForEditAccount.frame.size
        
        setupView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.createClientSuccess(_:)), name: NSNotification.Name(rawValue: C.CLIENT_CREATED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.backgroundImageChanged(_:)), name: NSNotification.Name(rawValue: C.NEW_PHOTO_CHANGED), object: nil)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
//        
//        self.viewForDialog.alpha        = 0.0
//        self.viewForEditAccount.alpha   = 0.0
//        
//        for view in self.viewForDialog.subviews
//        {
//            view.alpha                  = 0.0
//            UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseIn, animations:{
//                view.alpha    = 1.0
//            }, completion: {
//                (value: Bool) in
//            })
//        }
//        
//        UIView.animate(withDuration: 0.2, delay: 0.1, options: .curveEaseIn, animations:{
//            self.viewForDialog.alpha        = 1.0
//            self.viewForEditAccount.alpha   = 1.0
//        }, completion: {
//            (value: Bool) in
////            UIView.animate(withDuration: 0.1, delay: 0.2, options: .curveEaseIn, animations:{
////                self.viewForEditAccount.alpha    = 1.0
////            }, completion: {
////                (value: Bool) in
////            })
//        })
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.viewForDialog.alpha        = 1.0
        self.viewForEditAccount.alpha   = 1.0

        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations:{
            self.viewForDialog.alpha        = 0.0
            self.viewForEditAccount.alpha   = 0.0
        }, completion: {
            (value: Bool) in
        })
        
        for view in self.viewForDialog.subviews
        {
            view.alpha                  = 1.0
            UIView.animate(withDuration: 0.1, delay: 5, options: .curveEaseIn, animations:{
                view.alpha    = 0.0
            }, completion: {
                (value: Bool) in
                
            })
        }
    }   
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setClient(client: Client)
    {
        self.client             = client
    }
    
    private func setupView()
    {
        if (self.client.clientname == "")
        {
            self.buttonForSave.titleLabel?.text = "Anlegen"
            return
        }
        else
        {
            self.buttonForSave.titleLabel?.text = "Speichern"
        }
        
        var index:CGFloat           = 0
        let frame:CGRect            = self.imageForModelMask.frame
        var imageName               = self.client.image
        
        self.textForClientName.text = self.client.clientname
        
        if (self.client.phonenumber != "0")
        {
            self.textForPhone.text  = self.client.phonenumber
        }
        
        self.textForEmail.text      = self.client.email
        self.textForAddress.text    = self.client.address
        self.textForCountry.text    = self.client.country
        self.textForCity.text       = self.client.city
        self.textForBirthdate.text  = self.client.birthdate
        
        if (self.client.postal      != "0")
        {
            self.textForPostal.text = self.client.postal
        }
        
        if (self.client.gender      == "0")
        {
            buttonForFemale.backgroundColor = ThemeUtil.getGenderButtonColor(type: C.ButtonState.SELECTED_STATE)
            labelForFemale.textColor        = ThemeUtil.getGenderLabelColor(type: C.ButtonState.SELECTED_STATE)
            imageForFemale.image            = UIImage(named: "image_female_selected")
            
            buttonForMale.backgroundColor   = ThemeUtil.getGenderButtonColor(type: C.ButtonState.NORMAL_STATE)
            labelForMale.textColor          = ThemeUtil.getGenderLabelColor(type: C.ButtonState.NORMAL_STATE)
            imageForMale.image              = UIImage(named: "image_male")
        }
        else
        {
            buttonForMale.backgroundColor   = ThemeUtil.getGenderButtonColor(type: C.ButtonState.SELECTED_STATE)
            labelForMale.textColor          = ThemeUtil.getGenderLabelColor(type: C.ButtonState.SELECTED_STATE)
            imageForMale.image              = UIImage(named: "image_male_selected")
            
            buttonForFemale.backgroundColor = ThemeUtil.getGenderButtonColor(type: C.ButtonState.NORMAL_STATE)
            labelForFemale.textColor        = ThemeUtil.getGenderLabelColor(type: C.ButtonState.NORMAL_STATE)
            imageForFemale.image            = UIImage(named: "image_female")
        }
        
        if (self.client.objects != nil && (self.client.objects?.objects.count)! > 0)
        {
            imageName   = (self.client.objects?.objects[0].src.replacingOccurrences(of: C.BASE_URL, with: ""))!
        }
        
        switch imageName
        {
        case "/upload/model_1.png":
            index           = 1
            break
        case "/upload/model_2.png":
            index           = 2
            break
        case "/upload/model_3.png":
            index           = 3
            break
        case "/upload/model_4.png":
            index           = 4
            break
        default:
            break
        }
        
        if (index   == 0)
        {
            self.imageForModelMask.isHidden = true
        }
        else
        {
            self.imageForModelMask.frame    = CGRect(x: frame.origin.x + frame.size.width * (index - 1), y: frame.origin.y, width: frame.size.width, height: frame.size.height)
        }
    }
    
    private func createClient ()
    {
        if (self.textForClientName.text == "")
        {
            self.view.makeToast("Input name")
            return
        }

        let client: Client  = Client()
        
        client.userid       = (UserManager.getLoggedInUser()?.id)!
        client.clientname   = self.textForClientName.text!
        client.phonenumber  = self.textForPhone.text!
        client.email        = self.textForEmail.text!
        client.address      = self.textForAddress.text!
        client.postal       = self.textForPostal.text!
        client.city         = self.textForCity.text!
        client.country      = self.textForCountry.text!
        client.birthdate    = self.textForBirthdate.text!
        client.gender       = String(genderIndex)
        client.image        = selectedModelUrl
        
        if (!self.upload)
        {
            self.clientImage    = UIImage(named: "model_1.png")
        }
        
        ClientService.createClient(client: client, image:self.clientImage!, upload: self.upload)
    }
    
    private func updateClient()
    {
        if (self.textForClientName.text == "")
        {
            self.view.makeToast("Input name")
            return
        }
        
        let client: Client  = Client()
        
        client.id           = self.client.id
        client.userid       = (UserManager.getLoggedInUser()?.id)!
        client.clientname   = self.textForClientName.text!
        client.phonenumber  = self.textForPhone.text!
        client.email        = self.textForEmail.text!
        client.address      = self.textForAddress.text!
        client.postal       = self.textForPostal.text!
        client.city         = self.textForCity.text!
        client.country      = self.textForCountry.text!
        client.birthdate    = self.textForBirthdate.text!
        client.gender       = String(genderIndex)
        
        if (self.updateImage)
        {
            print(selectedModelUrl)

            client.image    = selectedModelUrl
        }
        else
        {
            client.image    = self.client.image
        }
        
        if (!self.upload)
        {
            let delimiter = "/"
            let token = selectedModelUrl.components(separatedBy: delimiter)
            print (token.last ?? "model_1.png")

            self.clientImage    = UIImage(named: token.last ?? "model_1.png")
        }
        
        ClientService.updateClient(client: client, image:self.clientImage!, uploadImage: self.upload)
        mainViewController.removeModelView(viewController: self)
    }
    
    @objc func createClientSuccess(_ notification: NSNotification)
    {
        let newClient:Client    = ClientManager.getLastNewClientAdded()!
//        mainViewController.setupWorkspaceView(client:newClient)
        mainViewController.removeModelView(viewController: self)
        mainViewController.setupDrawSignView(client: newClient, isCreating: true)
    }
    
    @objc func backgroundImageChanged(_ notification: NSNotification)
    {
        if let image = notification.userInfo?["image"] as? UIImage
        {
            self.clientImage    = image.resizeImage(toSize: self.targetSize!)
            self.upload         = true
            self.updateImage    = true
        }
    }
    
    private func hideAllKeyboard()
    {
        self.textForClientName.resignFirstResponder()
        self.textForBirthdate.resignFirstResponder()
        self.textForCountry.resignFirstResponder()
        self.textForCity.resignFirstResponder()
        self.textForPostal.resignFirstResponder()
        self.textForEmail.resignFirstResponder()
        self.textForPhone.resignFirstResponder()
        self.textForAddress.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField    == self.textForCountry
        {
            hideAllKeyboard()

        }
        else if textField   == self.textForBirthdate
        {
            hideAllKeyboard()

        }
    }

    @IBAction func popUpClicked(_ sender: UIButton)
    {
        let tag                 = sender.tag
        
        switch tag
        {
        case 1:
            ActionSheetStringPicker.show(withTitle: "Land", rows: ["Deutschland", "Österreich", "Schweiz", "Liechtenstein", "Luxemburg", "Ungarn"], initialSelection: 0, doneBlock: {
                picker, value, index in
                
                self.textForCountry.text  = String(describing: index!)
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview)
            break
        case 2:
            let datePicker = ActionSheetDatePicker(title: "Geburtsdatum:", datePickerMode: UIDatePickerMode.date, selectedDate: Date(), doneBlock: {
                picker, value, index in
                
                let formatter           = DateFormatter()
                formatter.dateFormat    = "yyyy-MM-dd HH:mm:ss Z"
                let dateObj             = formatter.date( from: String(describing: value!))
                
                formatter.dateFormat    = "d. MMMM yyyy"
                formatter.locale        = NSLocale(localeIdentifier: "de_DE") as Locale!
                let result              = formatter.string(from: dateObj!)
                
                self.textForBirthdate.text          = result
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview)
            datePicker?.maximumDate = Date()
            
            datePicker?.show()
            break
        default:
            break
        }
        
        hideAllKeyboard()
    }
    
    @IBAction func genderClicked(_ sender: UIButton)
    {
        let tag                 = sender.tag
        
        switch tag
        {
        case 1:
            buttonForFemale.backgroundColor = ThemeUtil.getGenderButtonColor(type: C.ButtonState.SELECTED_STATE)
            labelForFemale.textColor        = ThemeUtil.getGenderLabelColor(type: C.ButtonState.SELECTED_STATE)
            imageForFemale.image            = UIImage(named: "image_female_selected")
            
            buttonForMale.backgroundColor   = ThemeUtil.getGenderButtonColor(type: C.ButtonState.NORMAL_STATE)
            labelForMale.textColor          = ThemeUtil.getGenderLabelColor(type: C.ButtonState.NORMAL_STATE)
            imageForMale.image              = UIImage(named: "image_male")
            genderIndex                     = 0
            break
        case 2:
            buttonForMale.backgroundColor   = ThemeUtil.getGenderButtonColor(type: C.ButtonState.SELECTED_STATE)
            labelForMale.textColor          = ThemeUtil.getGenderLabelColor(type: C.ButtonState.SELECTED_STATE)
            imageForMale.image              = UIImage(named: "image_male_selected")
            
            buttonForFemale.backgroundColor = ThemeUtil.getGenderButtonColor(type: C.ButtonState.NORMAL_STATE)
            labelForFemale.textColor        = ThemeUtil.getGenderLabelColor(type: C.ButtonState.NORMAL_STATE)
            imageForFemale.image            = UIImage(named: "image_female")
            genderIndex                     = 1
            break
        default: break
        }
    }
    
    @IBAction func modelClicked(_ sender: UIButton)
    {
        let tag                     = sender.tag
        
        if tag == 5
        {
//            ClientService.uploadClientImage(image: UIImage(named: "button_point.png")!)
            mainViewController.setupTakePictureView(fromWorkspace: false)
        }
        else
        {
            imageForModelMask.isHidden  = false
            imageForModelMask.frame = sender.frame
            selectedModelUrl        = "/upload/model_" + String(tag) + ".png"
            
            self.upload             = false
            self.updateImage        = true
        }
    }
    
    @IBAction func controlClicked(_ sender: UIButton)
    {
        let tag                 = sender.tag
        
        switch tag
        {
        case 1:
            if (self.client.clientname  != "")
            {
                updateClient()
            }
            else
            {
                createClient()
            }
            
            break
        case 2:
            mainViewController.removeModelView(viewController: self)
            break
        default: break
        }
    }
}
