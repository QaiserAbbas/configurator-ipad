//
//  ProductManageCell.swift
//  configurator(iPad)
//
//  Created by MobileSuperMaster on 8/17/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit

class ProductManageCell: UITableViewCell
{
    @IBOutlet weak var imageForProduct: UIImageView!
    
    @IBOutlet weak var labelForProductName: UILabel!
    @IBOutlet weak var labelForProductDescription: UILabel!
    @IBOutlet weak var buttonForSelect: UIButton!
    
    public var product: Product!
    public var parentViewController: ProductsManageViewController!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func buttonClicked(_ sender: UIButton)
    {
        if sender.tag == 1
        {
            if parentViewController.tempProducts.contains(product)
            {
                self.buttonForSelect.setImage(UIImage(named: "image_hair_product_unchecked"), for: .normal)
                if let index = parentViewController.tempProducts.index(of:product)
                {
                    parentViewController.tempProducts.remove(at: index)
                }
            }
            else
            {
                self.buttonForSelect.setImage(UIImage(named: "image_success_sign"), for: .normal)
                parentViewController.tempProducts.append(product)
            }
        }
        else
        {
            parentViewController.remove(product: product)
            parentViewController.refreshScroll()
            parentViewController.tableviewForProducts.reloadData()
        }
    }
}
