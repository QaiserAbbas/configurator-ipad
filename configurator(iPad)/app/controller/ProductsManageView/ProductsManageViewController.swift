//
//  ProductsManageViewController.swift
//  configurator(iPad)
//
//  Created by MobileSuperMaster on 8/12/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProductsManageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ProductSelectViewControllerDelegate, ProductCreatViewControllerDelegate
{
    public var mainViewController: MainViewController!
    
    @IBOutlet weak var viewForNoneProducts: UIView!
    @IBOutlet weak var viewForProducts: UIView!
    
    @IBOutlet weak var scrollviewForCategory: UIScrollView!
    @IBOutlet weak var tableviewForProducts: UITableView!
    
//    var categoryList: [SubCategory] = []
    var productList: [Product]      = []
    var categoryViews: [UIView]     = []
    var tempProducts: [Product]     = []
    
    var ratio:CGFloat               =  0
    var firstAddedCategoryId:Int    =  0
    var lastAddedCategoryId:Int     =  0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        ratio                       =   CGFloat(self.view.frame.size.width / 1024)

        NotificationCenter.default.addObserver(self, selector: #selector(self.productsDownloaded(_:)), name: NSNotification.Name(rawValue: C.PRODUCTS_DOWNLOADED), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.productEmptyDownloaded(_:)), name: NSNotification.Name(rawValue: C.PRODUCTS_EMPTY_DOWNLOADED), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.productDownloadedFailed(_:)), name: NSNotification.Name(rawValue: C.PRODUCTS_FAILED_DOWNLOADED), object: nil)
        
        loadSelectedProducts()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func loadSelectedProducts ()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ClientService.getSelectProducts(user: UserManager.getLoggedInUser()!)
    }
    
    @objc func productsDownloaded(_ notification: NSNotification)
    {
        MBProgressHUD.hide(for: self.view, animated: true)
        
        setupSelectedProducts()
    }
    
    private func setupSelectedProducts()
    {
        let selectedProducts    = ClientManager.getSelectedProducts()
        
        for category in mainViewController.categoryList
        {
            category.products       = []
        }
        
        for category in mainViewController.categoryList
        {
            for product in selectedProducts
            {
                print("product custom = \(product.custom)")
                if category.name == product.category
                {
                    category.products?.append(product)
                }
            }
        }
        
        if selectedProducts.count > 0
        {
            self.viewForProducts.isHidden   = false
            loadScrollByCategory()
        }
    }
    
    @objc func productEmptyDownloaded(_ notification: NSNotification)
    {
        MBProgressHUD.hide(for: self.view, animated: true)
    }

    @objc func productDownloadedFailed(_ notification: NSNotification)
    {
        self.view.makeToast("Failed to download products")
        MBProgressHUD.hide(for: self.view, animated: true)
    }

    func completeSelectProducts(viewController: ProductSelectViewController)
    {
        let selectedProducts:[Product]  = viewController.selectedProducts

//        var isEmpty                     = true
        var setFirstAdded               = false
        
        for categoryIndex in 0...mainViewController.categoryList.count - 1
        {
            let category        = mainViewController.categoryList[categoryIndex]
            
            for product in selectedProducts
            {
                if product.category == category.name //&& !containsProduct(category: category, product: product)
                {
                    category.products?.append(product)
                    lastAddedCategoryId = categoryIndex
                    if !setFirstAdded
                    {
                        firstAddedCategoryId    = categoryIndex
                        setFirstAdded           = true
                    }
                }
            }
//            
//            if (category.products?.count)! > 0 && isEmpty
//            {
//                isEmpty                 = false
//            }
        }
//
//        if isEmpty
//        {
//            self.viewForProducts.isHidden   = true
//        }
//        else
//        {
//            self.viewForProducts.isHidden   = false
//            loadScrollByCategory()
//        }
        
        loadSelectedProducts()
        mainViewController.removeModelView(viewController: viewController)
    }

    func completeCreateProduct(viewController: ProductCreatViewController)
    {
        let selectedProduct:Product     = viewController.product
        var setFirstAdded               = false
        
        for categoryIndex in 0...mainViewController.categoryList.count - 1
        {
            let category        = mainViewController.categoryList[categoryIndex]

            if selectedProduct.category == category.name && !containsProduct(category: category, product: selectedProduct)
            {
//                category.products?.append(selectedProduct)
                lastAddedCategoryId = categoryIndex
                if !setFirstAdded
                {
                    firstAddedCategoryId    = categoryIndex
                    setFirstAdded           = true
                }
            }
        }
        
//        self.viewForProducts.isHidden   = false
        loadSelectedProducts()
        
        mainViewController.removeModelView(viewController: viewController)
    }
    
    private func containsProduct(category: SubCategory, product: Product) ->Bool
    {
        let predicate   = NSPredicate(format: "name == %@", product.name)
        let arr         = (category.products! as NSArray).filtered(using: predicate)
        
        return arr.count > 0
    }
    
    private func loadScrollByCategory()
    {
        let scrollFrame             = self.scrollviewForCategory.frame.size
        var nextPointX:CGFloat      = 0
        
        self.categoryViews          = []
        
        for subView in self.scrollviewForCategory.subviews
        {
            subView.removeFromSuperview()
        }
        
        for index in 0 ... mainViewController.categoryList.count - 1
        {
            let category            = mainViewController.categoryList[index]
            let containerView       = UIView()
            
            let label               = UILabel(frame: CGRect(x: 9 * ratio, y: (scrollFrame.height - 22 * ratio) / 2, width: 63 * ratio, height: 22 * ratio))
            
            label.text              = "\(category.name) (\(category.products!.count))"
            label.font              = UIFont(name: "SofiaProSemiBold", size: 15)
            label.textColor         = ThemeUtil.getMainCategoryLabelColor()
            
            label.sizeToFit()
            label.frame             = CGRect(x: label.frame.origin.x, y: (scrollFrame.height - label.frame.size.height) / 2, width: label.frame.size.width + 9, height: label.frame.size.height)
            
            containerView.frame     = CGRect(x: nextPointX, y: 0, width: label.frame.origin.x + label.frame.size.width, height: scrollFrame.height)
            containerView.layer.borderWidth    = 2;
            containerView.layer.borderColor    = ThemeUtil.getMainCategoryBorderColor().cgColor
            
            let button              = UIButton(frame: CGRect(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height))
            button.addTarget(self, action: #selector(categorySelected(_:)), for: .touchUpInside)
            button.tag              = index + 1000;
            
            nextPointX              += containerView.frame.size.width + 10
            
            containerView.addSubview(label)
            containerView.addSubview(button)
            
            categoryViews.append(containerView)
            
            self.scrollviewForCategory.addSubview(containerView)
        }
        
        self.scrollviewForCategory.contentSize = CGSize(width: nextPointX, height: scrollFrame.height)
//        categorySelect(selectIndex: lastAddedCategoryId)
        categorySelect(selectIndex: firstAddedCategoryId)
    }

    public func refreshScroll()
    {
        for index in 0...categoryViews.count - 1
        {
            let containerView   = categoryViews[index]
            let category        = mainViewController.categoryList[index]
            var label           = UILabel()
            
            for subView in containerView.subviews
            {
                if subView is UILabel
                {
                    label       = subView as! UILabel
                    label.text  = "\(category.name) (\(category.products!.count))"
                }
            }
        }
    }
    
    private func categorySelect ( selectIndex: Int)
    {
        for index in 0 ... categoryViews.count - 1
        {
            let containerView                       = categoryViews[index]
            var label                               = UILabel()
            
            for subView in containerView.subviews
            {
                if subView is UILabel
                {
                    label                           = subView as! UILabel
                }
            }
            
            if selectIndex == index
            {
                containerView.layer.borderWidth     = 0
                containerView.backgroundColor       = ThemeUtil.getPaymentSelectedBorderColor()
                label.textColor                     = UIColor.white
            }
            else
            {
                containerView.layer.borderWidth     = 2
                containerView.layer.borderColor     = ThemeUtil.getMainCategoryBorderColor().cgColor
                containerView.backgroundColor       = UIColor.clear
                label.textColor                     = ThemeUtil.getMainCategoryLabelColor()
            }
        }
        
        let selectcategory  = mainViewController.categoryList[selectIndex]
        productList         = selectcategory.products!
        
        self.tableviewForProducts.reloadData()
    }
    
    private func selectAll()
    {
        for product in productList
        {
            tempProducts.append(product)
        }
        
        self.tableviewForProducts.reloadData()
    }
    
    private func toJSonString(data : Any) -> String
    {
        var jsonString = "";
        
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        }
        catch
        {
            print(error.localizedDescription)
        }
        
        return jsonString;
    }
    
    private func removeSelectedProduct()
    {
        var product_ids: [String]    = []
        
        if tempProducts.count > 0
        {
            for product in tempProducts
            {
                removeProductFromCategory(product: product)
                product_ids.append(product.id)
            }

            var dataDict    = Dictionary<String, Any>()
            var parameters  = Dictionary<String, Any>()
            
            dataDict        = ["user_id": UserManager.getLoggedInUser()!.id, "product_ids": product_ids]
            parameters      = ["param":toJSonString(data: dataDict)]
            
            ClientService.removeProducts(parameters: parameters)
            tempProducts = []
        }
        
        refreshScroll()
        self.tableviewForProducts.reloadData()
    }
    
    private func removeProductFromCategory(product: Product)
    {
        if let index = productList.index(of:product)
        {
            productList.remove(at: index)
        }
        
        for category in mainViewController.categoryList
        {
            if (category.products?.contains(product))!
            {
                if let index = category.products?.index(of:product)
                {
                    category.products?.remove(at: index)
                }
            }
        }
    }
    
    public func remove(product: Product)
    {
        var product_ids: [String]    = []
        product_ids.append(product.id)
        
        var dataDict    = Dictionary<String, Any>()
        var parameters  = Dictionary<String, Any>()
        
        dataDict        = ["user_id": UserManager.getLoggedInUser()!.id, "product_ids": product_ids]
        parameters      = ["param":toJSonString(data: dataDict)]
        
        ClientService.removeProducts(parameters: parameters)
       
        removeProductFromCategory(product: product)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return productList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 74
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell    = tableView.dequeueReusableCell(withIdentifier: "ProductManageCell", for: indexPath) as! ProductManageCell
        
        //        let product:Product                     = self.subCategory.brands![0].products[indexPath.row]
        let product:Product                         = self.productList[indexPath.row]
        
        if product.custom == "1"
        {
            let main_string                         = "\(product.name)    Eigenprodukt    "
            let string_to_color                     = "   Eigenprodukt   "
            
            let range                               = (main_string as NSString).range(of: string_to_color)
            print("range = \(range.length)")
            let attribute                           = NSMutableAttributedString.init(string: main_string)
            attribute.addAttribute(NSAttributedStringKey.backgroundColor, value: ThemeUtil.getProductDescriptionColor(custom: "1") , range: range)
            attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: range)
            attribute.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "SofiaProBold", size: 10)! , range: range)
            
            cell.labelForProductName.attributedText = attribute
            cell.imageForProduct.frame              = CGRect(x: 13 * ratio, y: 13 * ratio, width: 40 * ratio, height: 40 * ratio)
        }
        else
        {
            cell.labelForProductName.text           = product.brand_name + ", " + product.name
            cell.imageForProduct.frame              = CGRect(x: 8 * ratio, y: 8 * ratio, width: 50 * ratio, height: 50 * ratio)
        }
        
        cell.labelForProductDescription.textColor   = ThemeUtil.getProductDescriptionColor(custom: product.custom)
        cell.labelForProductDescription.text        = product.descriptions
        cell.imageForProduct.af_setImage(withURL: URL(string:C.BASE_URL + product.image_url)!)
        cell.product                                = product
        cell.parentViewController                   = self
        
        if tempProducts.contains(product)
        {
            cell.buttonForSelect.setImage(UIImage(named: "image_success_sign"), for: .normal)
        }
        else
        {
            cell.buttonForSelect.setImage(UIImage(named: "image_hair_product_unchecked"), for: .normal)
        }
        
        return cell
    }
    
    @IBAction func categorySelected(_ sender: UIButton)
    {
        let selectIndex       = sender.tag - 1000
        categorySelect(selectIndex: selectIndex)
    }
    
    @IBAction func productsButtonClicked(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1: // Select
            mainViewController.setupProductSelectView(productManageView: self)
            break
        case 2: // Create
            mainViewController.setupProductCreateView(productManageView: self)
            break
        case 3: // Select All
            selectAll()
            break
        case 4: // Remove Selected
            removeSelectedProduct()
            break            
        default:
            break
        }
    }
    
    @IBAction func noneProductsButtonClicked(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1: // Select
            mainViewController.setupProductSelectView(productManageView: self)
            break
        case 2: // Create
            mainViewController.setupProductCreateView(productManageView: self)
            break
        case 3: // Help
            break
        case 4: // Create new
            break
        default:
            break
        }
    }
}
