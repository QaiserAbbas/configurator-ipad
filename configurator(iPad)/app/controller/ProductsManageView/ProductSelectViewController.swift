//
//  ProductSelectViewController.swift
//  configurator(iPad)
//
//  Created by MobileSuperMaster on 8/13/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol ProductSelectViewControllerDelegate
{
    func completeSelectProducts(viewController: ProductSelectViewController)
}

class ProductSelectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate
{
    public var mainViewController: MainViewController!
    
    @IBOutlet weak var viewForOptions: UIView!
    @IBOutlet weak var buttonForByBrand: UIButton!
    @IBOutlet weak var buttonForByCategory: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableViewForProducts: UITableView!
    @IBOutlet weak var viewForSearchText: UIView!
    @IBOutlet weak var textfieldForSearch: UITextField!
    @IBOutlet weak var labelForSearch: UILabel!
    @IBOutlet weak var buttonForSelectAll: UIButton!
    @IBOutlet weak var viewForSearchNone: UIView!
    @IBOutlet weak var buttonForSearch: UIButton!
    
    var delegate:ProductSelectViewControllerDelegate?
    var productList: [Product]      = []
    
    var brandList: [Brand]          = ClientManager.getBrands()
    var categoryList: [SubCategory] = ClientManager.getCategories()
    
    var allData: [Product]          = []
    
    var brandViews: [UIView]        = []
    var categoryViews: [UIView]     = []
    
    var ratio:CGFloat               =  0
    var selectedProducts: [Product] = ClientManager.getSelectedProducts()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupUI()
//        loadProductsByBrand()
        self.buttonForByBrand.isSelected = true
        loadBrands()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.brandsDownloaded(_:)), name: NSNotification.Name(rawValue: C.BRANDS_DOWNLOADED), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.categoriesDownloaded(_:)), name: NSNotification.Name(rawValue: C.CATEGORIES_DOWNLOADED), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.selectProducts(_:)), name: NSNotification.Name(rawValue: C.PRODUCTS_SELECTED), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.selectProductsFailed(_:)), name: NSNotification.Name(rawValue: C.PRODUCTS_FAILED_SELECTED), object: nil)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupUI()
    {
        ratio                                   =   CGFloat(self.view.frame.size.height / 768)
        
        self.viewForOptions.layer.borderWidth   = 2
        self.viewForOptions.layer.borderColor   = ThemeUtil.getPaymentSelectedBorderColor().cgColor
        
        let rightPaddingView    = UIView(frame: CGRect(x: 0, y: 0, width: 70 * ratio, height: 39 * ratio))
        let productButton       = UIButton(frame: CGRect(x: 15, y: 0, width: 39 * ratio, height: 39 * ratio))
        
        productButton.setImage(#imageLiteral(resourceName: "button_search_product"), for: .normal)
//        productButton.addTarget(self, action: #selector(removeHairProductViewClicked(_:)), for: .touchUpInside)

        rightPaddingView.addSubview(productButton)
        
        self.textfieldForSearch.rightView     = rightPaddingView
        self.textfieldForSearch.rightViewMode = UITextFieldViewMode.always
    }
    
    private func loadBrands()
    {
        ClientService.getProductsByBrands(user: UserManager.getLoggedInUser()!)
        reloadBrands()
    }
    
    @objc func brandsDownloaded(_ notification: NSNotification)
    {
        reloadBrands()
    }
    
    private func reloadBrands()
    {
        if self.buttonForByBrand.isSelected
        {
            self.brandList      = ClientManager.getBrands()
            allData             = []
            
            for brand in self.brandList
            {
                for product in brand.products
                {
                    allData.append(product)
                }
            }
            
            loadScrollByBrand()
        }
    }

    private func loadCategories()
    {
        ClientService.getProductsByCategories(user: UserManager.getLoggedInUser()!)
        reloadCategories()
    }
    
    @objc func categoriesDownloaded(_ notification: NSNotification)
    {
        reloadCategories()
    }
    
    private func reloadCategories()
    {
        if self.buttonForByCategory.isSelected
        {
            self.categoryList       = ClientManager.getCategories()
            allData                 = []
            
            for category in self.categoryList
            {
                for product in category.products!
                {
                    allData.append(product)
                }
            }
            
            loadScrollByCategory()
        }
    }
    
    private func loadProductsByBrand()
    {
        brandList               = []
        allData                 = []
        
        for brandIndex in 0...10
        {
            let brand       = Brand()
            
            brand.name      = "Test Brand \(brandIndex)"
            brand.image_url = "image_brand_teoxane"
            
            for productIndex in 0...C.CATEGORY_LIST.count - 1
            {
                let product             = Product()
                
                product.name            = "Brand \(brandIndex) - description"
                product.descriptions    = "\(C.CATEGORY_LIST[productIndex])"
                product.image_url       = "image_hair_product_1_1_1"
                product.category        = "\(C.CATEGORY_LIST[productIndex])"
                
                brand.products.append(product)
                allData.append(product)
            }
            
            brandList.append(brand)
        }
        
        loadScrollByBrand()
    }
    
    private func loadProductsByCategory()
    {
        categoryList            = []
        allData                 = []
        
        for categoryIndex in 0...C.CATEGORY_LIST.count - 1
        {
            let category        = SubCategory()
            
            category.name       = C.CATEGORY_LIST[categoryIndex]
            category.products   = []
            
            for productIndex in 0...10
            {
                let product             = Product()
                
                product.name            = "Brand \(productIndex) - description"
                product.descriptions    = "\(C.CATEGORY_LIST[categoryIndex])"
                product.image_url       = "image_hair_product_1_1_1"
                product.category        = "\(C.CATEGORY_LIST[productIndex])"
                
                category.products?.append(product)
                allData.append(product)
            }
            
            categoryList.append(category)
        }
        
        loadScrollByCategory()
    }

    private func loadScrollByBrand()
    {
        let scrollFrame             = self.scrollView.frame.size
        var nextPointX:CGFloat      = 0

        self.brandViews             = []
        
        for subView in self.scrollView.subviews
        {
            subView.removeFromSuperview()
        }
        
        if brandList.count <= 0
        {
            return
        }
        
        for index in 0 ... brandList.count - 1
        {
            let brand               = brandList[index]
            let containerView       = UIView()
            
            let imageView           = UIImageView(frame: CGRect(x: 9 * ratio, y: (scrollFrame.height - 32 * ratio) / 2, width: 63 * ratio, height: 32 * ratio))
            
            imageView.contentMode   = .scaleAspectFit
//            imageView.image         = UIImage(named:brand.image_url)
            imageView.af_setImage(withURL: URL(string:C.BASE_URL + brand.image_url)!)
            
            let label               = UILabel(frame: CGRect(x: imageView.frame.size.width + 15 * ratio, y: (scrollFrame.height - 22 * ratio) / 2, width: 63 * ratio, height: 22 * ratio))
            
            label.text              = brand.name
            label.font              = UIFont(name: "SofiaProSemiBold", size: 15)
            label.textColor         = ThemeUtil.getMainCategoryLabelColor()
            
            label.sizeToFit()
            label.frame             = CGRect(x: label.frame.origin.x, y: (scrollFrame.height - label.frame.size.height) / 2, width: label.frame.size.width, height: label.frame.size.height)
            
            containerView.frame     = CGRect(x: nextPointX, y: 0, width: label.frame.origin.x + label.frame.size.width + 9, height: scrollFrame.height)
            containerView.layer.borderWidth    = 2;
            containerView.layer.borderColor    = ThemeUtil.getMainCategoryBorderColor().cgColor
            
            let button              = UIButton(frame: CGRect(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height))
            button.addTarget(self, action: #selector(brandSelected(_:)), for: .touchUpInside)
            button.tag              = index + 1000;
            
            nextPointX              += containerView.frame.size.width + 10
            
            containerView.addSubview(imageView)
            containerView.addSubview(label)
            containerView.addSubview(button)
            
            brandViews.append(containerView)
            
            self.scrollView.addSubview(containerView)
        }
        
        self.scrollView.contentSize = CGSize(width: nextPointX, height: scrollFrame.height)
        brandSelect(selectIndex: 0)
    }
    
    private func loadScrollByCategory()
    {
        let scrollFrame             = self.scrollView.frame.size
        var nextPointX:CGFloat      = 0

        self.categoryViews          = []
        
        for subView in self.scrollView.subviews
        {
            subView.removeFromSuperview()
        }
        
        if categoryList.count <= 0
        {
            return
        }
        
        for index in 0 ... categoryList.count - 1
        {
            let category            = categoryList[index]
            let containerView       = UIView()
            
            let label               = UILabel(frame: CGRect(x: 9 * ratio, y: (scrollFrame.height - 22 * ratio) / 2, width: 63 * ratio, height: 22 * ratio))
            
            label.text              = category.name
            label.font              = UIFont(name: "SofiaProSemiBold", size: 15)
            label.textColor         = ThemeUtil.getMainCategoryLabelColor()
            
            label.sizeToFit()
            label.frame             = CGRect(x: label.frame.origin.x, y: (scrollFrame.height - label.frame.size.height) / 2, width: label.frame.size.width, height: label.frame.size.height)
            
            containerView.frame     = CGRect(x: nextPointX, y: 0, width: label.frame.origin.x + label.frame.size.width + 9, height: scrollFrame.height)
            containerView.layer.borderWidth    = 2;
            containerView.layer.borderColor    = ThemeUtil.getMainCategoryBorderColor().cgColor
            
            let button              = UIButton(frame: CGRect(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height))
            button.addTarget(self, action: #selector(categorySelected(_:)), for: .touchUpInside)
            button.tag              = index + 1000;
            
            nextPointX              += containerView.frame.size.width + 10
            
            containerView.addSubview(label)
            containerView.addSubview(button)
            
            categoryViews.append(containerView)
            
            self.scrollView.addSubview(containerView)
        }
        
        self.scrollView.contentSize = CGSize(width: nextPointX, height: scrollFrame.height)
        categorySelect(selectIndex: 0)
    }
    
    private func brandSelect ( selectIndex: Int)
    {
        for index in 0 ... brandViews.count - 1
        {
            let containerView                       = brandViews[index]
            var label                               = UILabel()
            
            for subView in containerView.subviews
            {
                if subView is UILabel
                {
                    label                           = subView as! UILabel
                }
            }
            
            if selectIndex == index
            {
                containerView.layer.borderWidth     = 0
                containerView.backgroundColor       = ThemeUtil.getPaymentSelectedBorderColor()
                label.textColor                     = UIColor.white
            }
            else
            {
                containerView.layer.borderWidth     = 2
                containerView.layer.borderColor     = ThemeUtil.getMainCategoryBorderColor().cgColor
                containerView.backgroundColor       = UIColor.clear
                label.textColor                     = ThemeUtil.getMainCategoryLabelColor()
            }
        }
        
        let selectBrand         = brandList[selectIndex]
        productList             = []
        
        for product in selectBrand.products
        {
            for selectedProduct in selectedProducts
            {
                if product.id   == selectedProduct.id
                {
                    product.selected    = true
                }
            }
            
            product.brand_name  = selectBrand.name
            productList.append(product)
        }
        
        self.tableViewForProducts.reloadData()
    }
    
    private func categorySelect ( selectIndex: Int)
    {        
        for index in 0 ... categoryViews.count - 1
        {
            let containerView                       = categoryViews[index]
            var label                               = UILabel()
            
            for subView in containerView.subviews
            {
                if subView is UILabel
                {
                    label                           = subView as! UILabel
                }
            }
            
            if selectIndex == index
            {
                containerView.layer.borderWidth     = 0
                containerView.backgroundColor       = ThemeUtil.getPaymentSelectedBorderColor()
                label.textColor                     = UIColor.white
            }
            else
            {
                containerView.layer.borderWidth     = 2
                containerView.layer.borderColor     = ThemeUtil.getMainCategoryBorderColor().cgColor
                containerView.backgroundColor       = UIColor.clear
                label.textColor                     = ThemeUtil.getMainCategoryLabelColor()
            }
        }
        
        let selectcategory  = categoryList[selectIndex]
        productList             = []
        
        for product in selectcategory.products!
        {
            for selectedProduct in selectedProducts
            {
                if product.id   == selectedProduct.id
                {
                    product.selected    = true
                }
            }
            
            productList.append(product)
        }
        
        self.tableViewForProducts.reloadData()
    }
    
    private func selectAllProducts()
    {
        for product in productList
        {
            product.selected    = true
            if !selectedProducts.contains(product)
            {
                selectedProducts.append(product)
            }
        }
        
        self.tableViewForProducts.reloadData()
    }
    
    private func enterSearchMode ()
    {
        self.labelForSearch.text            = "Suchergebnisse"
        self.buttonForSelectAll.isHidden    = true
        
        self.textfieldForSearch.isHidden    = false
        self.viewForSearchText.isHidden     = false
        self.viewForSearchNone.isHidden     = false
        
        self.buttonForByBrand.backgroundColor       = UIColor.white
        self.buttonForByBrand.setTitleColor(ThemeUtil.getPaymentSelectedBorderColor(), for: .normal)
        
        self.buttonForByCategory.backgroundColor    = UIColor.white
        self.buttonForByCategory.setTitleColor(ThemeUtil.getPaymentSelectedBorderColor(), for: .normal)
        
        self.buttonForByBrand.layer.borderWidth     = 2
        self.buttonForByBrand.layer.borderColor     = ThemeUtil.getPaymentSelectedBorderColor().cgColor
        
        self.buttonForByBrand.isSelected            = false
        self.buttonForByCategory.isSelected         = false
        
        self.buttonForSearch.setImage(#imageLiteral(resourceName: "button_searching_product"), for: .normal)
    }
    
    private func outSearchMode ()
    {
        self.labelForSearch.text            = "Produkte"
        self.buttonForSelectAll.isHidden    = false
        
        self.textfieldForSearch.isHidden    = true
        self.viewForSearchText.isHidden     = true
        self.viewForSearchNone.isHidden     = true
        
        self.buttonForSearch.setImage(#imageLiteral(resourceName: "button_search_product"), for: .normal)
    }
    
    private func toJSonString(data : Any) -> String
    {
        var jsonString = "";
        
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        }
        catch
        {
            print(error.localizedDescription)
        }
        
        return jsonString;
    }

    private func addSelectedProducts ()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        var product_ids: [String]       = []
        for selectedProduct in selectedProducts
        {
            product_ids.append(selectedProduct.id)
        }
        
        var dataDict : [String : Any] = [:];
        
        dataDict                = ["user_id": UserManager.getLoggedInUser()!.id, "product_ids": product_ids]
        
        var parameters          = Dictionary<String, Any>()
        parameters              = ["param":toJSonString(data: dataDict)]
        
        ClientService.selectProducts(parameters: parameters)
    }
    
    @objc func selectProducts(_ notification: NSNotification)
    {
        MBProgressHUD.hide(for: self.view, animated: true)
        self.delegate?.completeSelectProducts(viewController: self)
        NotificationCenter.default.removeObserver(self)
    }

    @objc func selectProductsFailed(_ notification: NSNotification)
    {
        self.view.makeToast("Failed to select products")
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let search = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        let predicate   = NSPredicate(format: "name CONTAINS[cd] %@", search)
        let arr         = (allData as NSArray).filtered(using: predicate)
        
        if arr.count > 0
        {
            productList.removeAll(keepingCapacity: true)
            productList     = arr as! [Product]
            
            self.viewForSearchNone.isHidden = true
            self.tableViewForProducts.reloadData()
        }
        else
        {
            self.viewForSearchNone.isHidden = false
        }
        
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return productList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 74
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell    = tableView.dequeueReusableCell(withIdentifier: "ProductSelectCell", for: indexPath) as! ProductSelectCell
        
        //        let product:Product                     = self.subCategory.brands![0].products[indexPath.row]
        let product:Product                     = self.productList[indexPath.row]
        
        cell.labelForProductName.text           = product.brand_name + ", " + product.name
        cell.labelForProductDescription.text    = product.descriptions
//        cell.imageForProduct.image              = UIImage(named: product.image_url)
        
        cell.imageForProduct.af_setImage(withURL: URL(string:C.BASE_URL + product.image_url)!)
        cell.product                            = product
        cell.parentViewController               = self
        
        if product.selected
        {
            cell.buttonForSelect.setImage(UIImage(named: "image_success_sign"), for: .normal)
        }
        else
        {
            cell.buttonForSelect.setImage(UIImage(named: "image_hair_product_unchecked"), for: .normal)
        }
        
        return cell
    }
    
    @IBAction func brandSelected(_ sender: UIButton)
    {
        let selectIndex       = sender.tag - 1000
        brandSelect(selectIndex: selectIndex)
    }
    
    @IBAction func categorySelected(_ sender: UIButton)
    {
        let selectIndex       = sender.tag - 1000
        categorySelect(selectIndex: selectIndex)
    }
    
    @IBAction func optionClicked(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1: // By Brand
            self.buttonForByBrand.backgroundColor       = ThemeUtil.getPaymentSelectedBorderColor()
            self.buttonForByBrand.setTitleColor(UIColor.white, for: .normal)
            
            self.buttonForByCategory.backgroundColor    = UIColor.white
            self.buttonForByCategory.setTitleColor(ThemeUtil.getPaymentSelectedBorderColor(), for: .normal)
            
            self.buttonForByBrand.isSelected            = true
            self.buttonForByCategory.isSelected         = false
            
//            loadProductsByBrand()
            loadBrands()
            break
        case 2: // By Category
            self.buttonForByBrand.backgroundColor       = UIColor.white
            self.buttonForByBrand.setTitleColor(ThemeUtil.getPaymentSelectedBorderColor(), for: .normal)

            self.buttonForByCategory.backgroundColor    = ThemeUtil.getPaymentSelectedBorderColor()
            self.buttonForByCategory.setTitleColor(UIColor.white, for: .normal)
            
            self.buttonForByBrand.isSelected            = false
            self.buttonForByCategory.isSelected         = true
            
//            loadProductsByCategory()
            loadCategories()
            break
        default:
            break
        }
        
        outSearchMode()
    }
    
    @IBAction func buttonClicked(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1: // Add
            addSelectedProducts()
            break
        case 2: // Delete
            NotificationCenter.default.removeObserver(self)
            mainViewController.removeModelView(viewController: self)
            break
        case 3: // Select All
            selectAllProducts()
            break
        case 4: // Search
            enterSearchMode()
            break
        default:
            break
        }
    }
}
