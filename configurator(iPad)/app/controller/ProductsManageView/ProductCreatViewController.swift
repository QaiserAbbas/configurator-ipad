//
//  ProductCreatViewController.swift
//  configurator(iPad)
//
//  Created by MobileSuperMaster on 8/16/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol ProductCreatViewControllerDelegate
{
    func completeCreateProduct(viewController: ProductCreatViewController)
}

class ProductCreatViewController: UIViewController
{
    public var mainViewController: MainViewController!
    @IBOutlet weak var textfieldForName: UITextField!
    @IBOutlet weak var textfieldForDescription: UITextField!
    
    var delegate:ProductCreatViewControllerDelegate?
    
    public var selectedCategoryId: String   = ""
    public var selectedCycle: String        = ""
    public var product: Product             = Product()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.productCreated(_:)), name: NSNotification.Name(rawValue: C.PRODUCT_CREATED), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.productCreatedFailed(_:)), name: NSNotification.Name(rawValue: C.PRODUCT_FAILED_CREATED), object: nil)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupUI()
    {
        let paddingView1                    = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.textfieldForName.frame.height))
        let paddingView2                    = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.textfieldForDescription.frame.height))
        
        self.textfieldForName.leftView      = paddingView1
        self.textfieldForName.leftViewMode  = UITextFieldViewMode.always
        
        self.textfieldForDescription.leftView       = paddingView2
        self.textfieldForDescription.leftViewMode   = UITextFieldViewMode.always
        
        initCategoryButtons()
        initCycleButtons()
    }
    
    @objc func productCreated(_ notification: NSNotification)
    {
        MBProgressHUD.hide(for: self.view, animated: true)
        self.delegate?.completeCreateProduct(viewController: self)
    }

    @objc func productCreatedFailed(_ notification: NSNotification)
    {
        self.view.makeToast("Failed to create new product")
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    private func initCategoryButtons()
    {
        for tag in 101...109
        {
            let button  = self.view.viewWithTag(tag) as? UIButton
            
            button?.setTitleColor(ThemeUtil.getMainCategoryLabelColor(), for: .normal)
            button?.backgroundColor     = UIColor.white
            button?.layer.borderWidth   = 2
            button?.layer.borderColor   = ThemeUtil.getMainCategoryBorderColor().cgColor
        }
    }
    
    private func initCycleButtons()
    {
        for tag in 201...203
        {
            let button  = self.view.viewWithTag(tag) as? UIButton
            
            button?.setTitleColor(ThemeUtil.getMainCategoryLabelColor(), for: .normal)
            button?.backgroundColor     = UIColor.white
            button?.layer.borderWidth   = 2
            button?.layer.borderColor   = ThemeUtil.getMainCategoryBorderColor().cgColor
        }
    }
    
    private func createProduct()
    {
        if selectedCategoryId == ""
        {
            self.view.makeToast("Select Category")
            return
        }
        
        if (self.textfieldForName.text?.isEmpty)!
        {
            self.view.makeToast("Input Fields")
            return
        }
        
        if (self.textfieldForDescription.text?.isEmpty)!
        {
            product.name            =  self.textfieldForName.text!
        }
        else
        {
            product.name            = "\(self.textfieldForName.text!) - \(self.textfieldForDescription.text!)"
        }        
        
        product.descriptions    = C.CATEGORY_LIST[Int(selectedCategoryId)!]
        product.image_url       = "image_custom_product"
        product.category        = C.CATEGORY_LIST[Int(selectedCategoryId)!];
        product.custom          = "1"
        
        var parameters          = Dictionary<String, String>()
        parameters              = ["user_id":UserManager.getLoggedInUser()!.id, "description":product.descriptions, "category":product.category,"name":product.name,"cycle":selectedCycle]
        
        ClientService.createProduct(parameters: parameters)
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    @IBAction func categoryButtonClicked(_ sender: UIButton)
    {
        initCategoryButtons()
        
        sender.setTitleColor(UIColor.white, for: .normal)
        sender.backgroundColor      = ThemeUtil.getPaymentSelectedBorderColor()
        sender.layer.borderWidth    = 0
        
        selectedCategoryId          = String(sender.tag - 101)
    }
    
    @IBAction func cycleButtonClicked(_ sender: UIButton)
    {
        initCycleButtons()
        
        sender.setTitleColor(UIColor.white, for: .normal)
        sender.backgroundColor      = ThemeUtil.getPaymentSelectedBorderColor()
        sender.layer.borderWidth    = 0
        
        selectedCycle               = String(sender.tag - 201)
    }
    
    @IBAction func buttonClicked(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1:
            createProduct()
            break
        case 2:
            mainViewController.removeModelView(viewController: self)
            break
            
        default:
            break
        }
    }
}
