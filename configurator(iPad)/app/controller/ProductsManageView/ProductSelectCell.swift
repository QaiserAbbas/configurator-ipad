//
//  ProductSelectCell.swift
//  configurator(iPad)
//
//  Created by MobileSuperMaster on 8/15/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit

class ProductSelectCell: UITableViewCell
{
    @IBOutlet weak var imageForProduct: UIImageView!
    
    @IBOutlet weak var labelForProductName: UILabel!
    @IBOutlet weak var labelForProductDescription: UILabel!
    @IBOutlet weak var buttonForSelect: UIButton!
    
    public var parentViewController: ProductSelectViewController!    
    public var product: Product!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func buttonClicked(_ sender: UIButton)
    {
        self.product.selected    = !self.product.selected
        
        if self.product.selected
        {
            self.buttonForSelect.setImage(UIImage(named: "image_success_sign"), for: .normal)
            parentViewController.selectedProducts.append(self.product)
        }
        else
        {
            self.buttonForSelect.setImage(UIImage(named: "image_hair_product_unchecked"), for: .normal)
            for selectedProduct in parentViewController.selectedProducts
            {
                if selectedProduct.id == self.product.id
                {
                    parentViewController.selectedProducts   = parentViewController.selectedProducts.filter() { $0 != selectedProduct }
                }
            }
        }
    }
}
