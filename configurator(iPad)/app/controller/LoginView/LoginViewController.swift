//
//  LoginViewController.swift
//  configurator(iPad)
//
//  Created by MobileMaster on 1/12/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit
import Toast_Swift

class LoginViewController: UIViewController
{
    @IBOutlet weak var textfieldForUsername: UITextField!
    @IBOutlet weak var textfieldForPassword: UITextField!
    @IBOutlet weak var viewForLoading: UIView!
    @IBOutlet weak var activityForIndicator: UIActivityIndicatorView!
    @IBOutlet var rememberMeButton: UIButton!
    var isRemember = false

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        self.textfieldForUsername.text      = "test@test.com"
//        self.textfieldForPassword.text      = "asd"
        if let savedValue = UserDefaults.standard.string(forKey: "email") {
            self.textfieldForUsername.text=savedValue;
        }
        if let savedValue = UserDefaults.standard.string(forKey: "password") {
            self.textfieldForPassword.text=savedValue
        }
        rememberMeButton .setImage(UIImage(named:"check_purple"), for: UIControlState.normal)
        isRemember = true


        let paddingView1                    = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.textfieldForUsername.frame.height))
        let paddingView2                    = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.textfieldForPassword.frame.height))
        
        self.textfieldForUsername.leftView  = paddingView1
        self.textfieldForUsername.leftViewMode = UITextFieldViewMode.always
        self.textfieldForPassword.leftView  = paddingView2
        self.textfieldForPassword.leftViewMode = UITextFieldViewMode.always
        
        self.viewForLoading.isHidden        = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.loginSuccess(_:)), name: NSNotification.Name(rawValue: C.USER_LOGGED_IN), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.loginFailed(_:)), name: NSNotification.Name(rawValue: C.USER_NOT_LOGGED_IN), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.clientsDownloaded(_:)), name: NSNotification.Name(rawValue: C.CLIENTS_DOWNLOADED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.clientsEmptyDownloaded(_:)), name: NSNotification.Name(rawValue: C.CLIENTS_EMPTY_DOWNLOADED), object: nil)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    override var prefersStatusBarHidden: Bool
        {
        get
        {
            return true
        }
    }
    
    @objc func loginSuccess(_ notification: NSNotification)
    {
        let user:User   = UserManager.getLoggedInUser()!
        saveUserEmail()
        ClientService.getClients(user: user)
        if (UserDefaults.standard.dictionary(forKey: C.PAYLOG_FAILED_CREATED) != nil)
        {
            ClientService.setupPayment(parameters: UserDefaults.standard.dictionary(forKey: C.PAYLOG_FAILED_CREATED) as! Dictionary<String, String>)
        }
    }

    @objc func loginFailed(_ notification: NSNotification)
    {
        self.view.makeToast("Login Failed")
        self.viewForLoading.isHidden    = true
        self.activityForIndicator.stopAnimating()
    }
    
    @objc func clientsDownloaded(_ notification: NSNotification)
    {
        let clients:Array<Client>       = ClientManager.getClients()
        
        let storyBoard : UIStoryboard   = UIStoryboard(name: "Main", bundle:nil)
        let mainViewController          = storyBoard.instantiateViewController(withIdentifier: "mainViewController") as! MainViewController
        mainViewController.view.isHidden = true
        mainViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

        self.present(mainViewController, animated:false, completion:{
            mainViewController.view.isHidden    = false
            if (clients.count > 0)
            {
                mainViewController.setupProjectPanelView()
            }
            else
            {
                mainViewController.setupPanelView()
            }
            
            self.viewForLoading.isHidden    = true
            self.activityForIndicator.stopAnimating()
        })
    }
    
    @objc func clientsEmptyDownloaded(_ notification: NSNotification)
    {
        let storyBoard : UIStoryboard   = UIStoryboard(name: "Main", bundle:nil)
        let mainViewController          = storyBoard.instantiateViewController(withIdentifier: "mainViewController") as! MainViewController
        mainViewController.view.isHidden = true
        mainViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

        self.present(mainViewController, animated:false, completion:{
            mainViewController.view.isHidden    = false
            mainViewController.setupPanelView()
            
            self.viewForLoading.isHidden    = true
            self.activityForIndicator.stopAnimating()
        })
    }
    func saveUserEmail() -> Void {
        if isRemember
        {
            UserDefaults.standard.set(textfieldForUsername.text, forKey: "email")
            UserDefaults.standard.set(textfieldForPassword.text, forKey: "password")
            UserDefaults.standard.synchronize()
        }
        else
        {
            UserDefaults.standard.removeObject(forKey: "email")
            UserDefaults.standard.removeObject(forKey: "password")
            UserDefaults.standard.synchronize()
        }
    }
    @IBAction func rememberMeButtonTap(_ sender: UIButton) {
        if (sender.currentImage?.isEqual(UIImage(named: "check_purple")))! {
            //do something here
            rememberMeButton .setImage(UIImage(named:"signup_icon_unchecked"), for: UIControlState.normal)
            isRemember = false
        }
        else
        {
            rememberMeButton .setImage(UIImage(named:"check_purple"), for: UIControlState.normal)
            isRemember = true
        }
    }
    @IBAction func buttonClicked(_ sender: UIButton)
    {
        if (self.textfieldForUsername.text == "")
        {
            self.view.makeToast("Input Email")
            return
        }
        
        if (self.textfieldForPassword.text == "")
        {
            self.view.makeToast("Input Password")
            return
        }
        
        self.viewForLoading.isHidden        = false
        self.activityForIndicator.startAnimating()
        
        let user:User   = User()
        user.username   = self.textfieldForUsername.text!
        user.password   = self.textfieldForPassword.text!
        
        UserManager.loginUser(user: user)
    }
}
