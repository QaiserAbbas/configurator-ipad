//
//  AddPaymentViewController.swift
//  configurator(iPad)
//
//  Created by MobileSuperMaster on 7/6/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import UIKit
import AlamofireImage
import ActionSheetPicker_3_0
import ACEDrawingView
import MBProgressHUD
import Toast_Swift

class AddPaymentViewController: UIViewController, ACEDrawingViewDelegate, PayPalPaymentDelegate
{
    @IBOutlet weak var viewForStep1: UIView!
    @IBOutlet weak var viewForStep2: UIView!
    @IBOutlet weak var viewForStep3: UIView!
    @IBOutlet weak var viewForStep4: UIView!
    @IBOutlet weak var viewForStep5: UIView!
    @IBOutlet weak var viewForStep6: UIView!
    
    @IBOutlet weak var imageForUser: UIImageView!
    
    @IBOutlet weak var buttonForDebit: UIButton!
    @IBOutlet weak var buttonForCreditCard: UIButton!
    @IBOutlet weak var buttonForPaypal: UIButton!
    @IBOutlet weak var labelForPaymentTitleInStep3: UILabel!
    @IBOutlet weak var labelForPaymentTypeInStep3: UILabel!
    @IBOutlet weak var labelForPaymentDescriptionInStep3: UILabel!
    
    @IBOutlet weak var viewForPaymentBank: UIView!
    @IBOutlet weak var viewForPaymentPaypal: UIView!
    @IBOutlet weak var viewForPaymentCreditCard: UIView!
    @IBOutlet weak var textForClientName: UITextField!
    @IBOutlet weak var textForIBAN: UITextField!
    @IBOutlet weak var textForBIC: UITextField!
    @IBOutlet weak var textForEmail: UITextField!
    @IBOutlet weak var textForBillingAddress: UITextField!
    @IBOutlet weak var textForPostal: UITextField!
    @IBOutlet weak var textForCity: UITextField!
    @IBOutlet weak var textForCountry: UITextField!
    @IBOutlet weak var textForBirthdate: UITextField!
    
    @IBOutlet weak var textForHolderNameCC: UITextField!
    @IBOutlet weak var textForNumberCC: UITextField!
    @IBOutlet weak var textForExpDateCC: UITextField!
    @IBOutlet weak var textForCVCCC: UITextField!
    @IBOutlet weak var textForBillingAddressCC: UITextField!
    @IBOutlet weak var textForPostcodeCC: UITextField!
    @IBOutlet weak var textForCityCC: UITextField!
    @IBOutlet weak var textForCountryCC: UITextField!
    @IBOutlet weak var textForBirthdateCC: UITextField!
    @IBOutlet weak var textForEmailCC: UITextField!
    
    @IBOutlet weak var viewForLabelBank: UIView!
    @IBOutlet weak var viewForLabelCC: UIView!
    
    @IBOutlet weak var labelForClientName: UILabel!
    @IBOutlet weak var labelForIBAN: UILabel!
    @IBOutlet weak var labelForBank: UILabel!
    @IBOutlet weak var labelForBic: UILabel!
    @IBOutlet weak var labelForBillingAddress: UILabel!
    @IBOutlet weak var labelForPostal: UILabel!
    @IBOutlet weak var labelForCity: UILabel!
    @IBOutlet weak var labelForCountry: UILabel!
    @IBOutlet weak var labelForBirthdate: UILabel!
    @IBOutlet weak var labelForEmail: UILabel!
    
    @IBOutlet weak var labelForHolderNameCC: UILabel!
    @IBOutlet weak var labelForNumberCC: UILabel!
    @IBOutlet weak var labelForExpDateCC: UILabel!
    @IBOutlet weak var labelForCVCCC: UILabel!
    @IBOutlet weak var labelForBillingAddressCC: UILabel!
    @IBOutlet weak var labelForPostalCC: UILabel!
    @IBOutlet weak var labelForCityCC: UILabel!
    @IBOutlet weak var labelForCountryCC: UILabel!
    @IBOutlet weak var labelForBirthdateCC: UILabel!
    @IBOutlet weak var labelForEmailCC: UILabel!
    
    @IBOutlet weak var imageForIBANCheck: UIImageView!
    
    @IBOutlet weak var labelForPaymentTypeInStep4: UILabel!
    @IBOutlet weak var labelForPaymentDescriptionInStep4: UILabel!
    
    @IBOutlet weak var viewForDrawingArea: ACEDrawingView!
    @IBOutlet weak var labelForUserNameInDrawing: UILabel!
    @IBOutlet weak var labelForDate: UILabel!
    @IBOutlet weak var labelForSuccessTitle: UILabel!
    
    public  var mainViewController: MainViewController!
    private var user: User!
    private var payment:Payment!
    
    private var paymentOption:Int   = 2
    private var paymentMethod:Int   = 1
    private var paymentAmount:Float = 0
    
    private var payPalConfig    = PayPalConfiguration() // default
    private var environment:String  = PayPalEnvironmentNoNetwork{
        willSet(newEnvironment)
        {
            if (newEnvironment != environment)
            {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    private var expDate: Date!
    
    private var signImage:UIImage?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.user                                   = UserManager.getLoggedInUser()!
        let userImageUrl: URL                       = URL(string:C.BASE_URL + self.user.avatar)!
        
        let size                                    = CGSize(width: 90.0, height: 90.0)
        let imageFilter                             = AspectScaledToFillSizeCircleFilter(size: size)
        self.imageForUser.af_setImage(withURL: userImageUrl, placeholderImage: nil, filter: imageFilter, imageTransition: .crossDissolve(0.2))
        
        initView()
        self.viewForStep1.isHidden                  = false
        
        payPalConfig.acceptCreditCards              = false
        payPalConfig.merchantName                   = "Awesome Shirts, Inc."
        payPalConfig.merchantPrivacyPolicyURL       = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL       = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        payPalConfig.payPalShippingAddressOption    = .payPal;
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.successSetupPayment(_:)), name: NSNotification.Name(rawValue: C.PAYLOG_CREATED), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.failedSetupPayment(_:)), name: NSNotification.Name(rawValue: C.PAYLOG_FAILED_CREATED), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    private func initView()
    {
        hideAllView()
        
        initStep3PaymentMethodButtons()
        self.buttonForDebit.layer.borderColor       = ThemeUtil.getPaymentSelectedBorderColor().cgColor
        
        initStep5View()
        initFieldForTest()
    }
    
    private func hideAllView()
    {
        self.viewForStep1.isHidden  = true
        self.viewForStep2.isHidden  = true
        self.viewForStep3.isHidden  = true
        self.viewForStep4.isHidden  = true
        self.viewForStep5.isHidden  = true
        self.viewForStep6.isHidden  = true
    }

    private func hideAllPaymentView()
    {
        self.viewForPaymentBank.isHidden        = true
        self.viewForPaymentPaypal.isHidden      = true
        self.viewForPaymentCreditCard.isHidden  = true
    }
    
    private func initFieldForTest()
    {
        self.textForIBAN.text           = "DE12500105170648489890"
        self.textForBIC.text            = "BENEDEPPYYY"
        self.textForClientName.text     = "Alex Tabo"
        
        self.textForHolderNameCC.text   = "Max Musterman"
        self.textForNumberCC.text       = "4111111111111111"
        self.textForCVCCC.text          = "333"
    }
    
    private func initStep3PaymentMethodButtons()
    {
        self.buttonForDebit.layer.borderColor       = ThemeUtil.getPaymentNormalBorderColor().cgColor
        self.buttonForPaypal.layer.borderColor      = ThemeUtil.getPaymentNormalBorderColor().cgColor
        self.buttonForCreditCard.layer.borderColor  = ThemeUtil.getPaymentNormalBorderColor().cgColor
        
        self.buttonForDebit.layer.borderWidth       = 2
        self.buttonForPaypal.layer.borderWidth      = 2
        self.buttonForCreditCard.layer.borderWidth  = 2
    }
    
    private func initStep3View()
    {
        hideAllPaymentView()
        self.viewForPaymentBank.isHidden            = false
        
        switch self.paymentOption
        {
        case 1:
            self.labelForPaymentTypeInStep3.text        = "6 Monats Paket"
            self.labelForPaymentDescriptionInStep3.text = "€\(C.PAYMENT_OPTION_6)"
            self.paymentAmount                          = 79 * 6
            break
        case 2:
            self.labelForPaymentTypeInStep3.text        = "1 Jahres Paket"
            self.labelForPaymentDescriptionInStep3.text = "€\(C.PAYMENT_OPTION_12)"
            self.paymentAmount                          = 69 * 12
            break
        case 3:
            self.labelForPaymentTypeInStep3.text        = "2 Jahres Paket"
            self.labelForPaymentDescriptionInStep3.text = "€\(C.PAYMENT_OPTION_24)"
            self.paymentAmount                          = 59 * 24
            break
        default:
            break
        }
    }
    
    private func initStep4View()
    {
        switch self.paymentOption
        {
        case 1:
            self.labelForPaymentTypeInStep4.text        = "6 Monats Paket"
            self.labelForPaymentDescriptionInStep4.text = "€\(C.PAYMENT_OPTION_6)"
            break
        case 2:
            self.labelForPaymentTypeInStep4.text        = "1 Jahres Paket"
            self.labelForPaymentDescriptionInStep4.text = "€\(C.PAYMENT_OPTION_12)"
            break
        case 3:
            self.labelForPaymentTypeInStep4.text        = "2 Jahres Paket"
            self.labelForPaymentDescriptionInStep4.text = "€\(C.PAYMENT_OPTION_24)"
            break
        default:
            break
        }
        
        switch self.paymentMethod
        {
        case 1: // Bank
            self.viewForLabelBank.isHidden      = false
            self.viewForLabelCC.isHidden        = true
            
            self.labelForClientName.text        = payment.holdername
            self.labelForIBAN.text              = payment.iban
            self.labelForBic.text               = payment.bic
            self.labelForBillingAddress.text    = payment.address
            self.labelForPostal.text            = payment.postal
            self.labelForCity.text              = payment.city
            self.labelForCountry.text           = payment.country
            self.labelForBirthdate.text         = payment.birthdate
            self.labelForEmail.text             = payment.email
            
            break
        case 2: // Paypal
            hideAllView()
            self.viewForStep3.isHidden          = false
            break
        case 3: // Credit Card
            self.viewForLabelBank.isHidden      = true
            self.viewForLabelCC.isHidden        = false
            
            self.labelForHolderNameCC.text      = payment.holdername
            self.labelForNumberCC.text          = payment.ccnumber
            self.labelForExpDateCC.text         = self.textForExpDateCC.text
            self.labelForCVCCC.text             = payment.cccvc
            self.labelForBillingAddressCC.text  = payment.address
            self.labelForPostalCC.text          = payment.postal
            self.labelForCityCC.text            = payment.city
            self.labelForCountryCC.text         = payment.country
            self.labelForBirthdateCC.text       = payment.birthdate
            self.labelForEmailCC.text           = payment.email
            
            break
        default:
            break
        }
    }

    private func initStep5View()
    {
        self.labelForUserNameInDrawing.text     = self.user.username
        
        let date                                = Date()
        let formatter                           = DateFormatter()
        formatter.dateFormat                    = "d. MMMM yyyy"
        formatter.locale                        = NSLocale(localeIdentifier: "de_DE") as Locale!
        let result                              = formatter.string(from: date)
        self.labelForDate.text                  = result
        
        self.viewForDrawingArea.delegate        = self
        self.viewForDrawingArea.lineColor       = UIColor.black
        self.viewForDrawingArea.drawTool        = ACEDrawingToolTypePen
//        self.viewForDrawingArea.usingDashArray  = false
        self.viewForDrawingArea.lineWidth       = 6
    }
    
    private func hideAllKeyboard()
    {
        self.textForClientName.resignFirstResponder()
        self.textForBirthdate.resignFirstResponder()
        self.textForCountry.resignFirstResponder()
        self.textForCity.resignFirstResponder()
        self.textForPostal.resignFirstResponder()
        self.textForEmail.resignFirstResponder()
        self.textForIBAN.resignFirstResponder()
        self.textForBillingAddress.resignFirstResponder()
    }

    private func checkField()
    {
        switch self.paymentMethod
        {
        case 1: // Bank
            checkFieldBank()
            break
        case 2: // Paypal
            payFromPayPal()
            break
        case 3: // Credit Card
            checkFieldCreditCard()
            break
        default:
            break
        }
    }
    
    private func sendPayment()
    {
        switch self.paymentMethod
        {
        case 1: // Bank
            transactionFromBank()
            break
        case 2: // Paypal
            payFromPayPal()
            break
        case 3: // Credit Card
            transactionFromCreditCard()
            break
        default:
            break
        }
    }
    
    private func moveStep4View()
    {
        hideAllView()
        self.viewForStep4.isHidden  = false
        initStep4View()
    }
    
    private func moveStep5View()
    {
        hideAllView()
        self.viewForStep5.isHidden  = false
    }
    
    private func checkFieldBank()
    {
        if self.textForClientName.text      == "" ||
            self.textForIBAN.text           == "" ||
            self.textForBIC.text            == "" ||
            self.textForEmail.text          == "" ||
            self.textForBillingAddress.text == "" ||
            self.textForPostal.text         == "" ||
            self.textForCity.text           == "" ||
            self.textForCountry.text        == "" ||
            self.textForBirthdate.text      == ""
        {
            self.view.makeToast("Please input all fields")
            return
        }
        
        payment             = Payment()
        
        payment.userid      = self.user.id
        payment.holdername  = self.textForClientName.text!
        payment.iban        = self.textForIBAN.text!
        payment.bic         = self.textForBIC.text!
        payment.email       = self.textForEmail.text!
        payment.address     = self.textForBillingAddress.text!
        payment.postal      = self.textForPostal.text!
        payment.city        = self.textForCity.text!
        payment.country     = self.textForCountry.text!
        payment.birthdate   = self.textForBirthdate.text!
        
        moveStep4View()
    }
    
    private func checkFieldCreditCard()
    {
        if self.textForHolderNameCC.text        == "" ||
            self.textForNumberCC.text           == "" ||
            self.textForExpDateCC.text          == "" ||
            self.textForCVCCC.text              == "" ||
            self.textForBillingAddressCC.text   == "" ||
            self.textForPostcodeCC.text         == "" ||
            self.textForCityCC.text             == "" ||
            self.textForCountryCC.text          == "" ||
            self.textForBirthdateCC.text        == ""
        {
            self.view.makeToast("Please input all fields")
            return
        }
        
        payment             = Payment()
        
        payment.userid      = self.user.id
        payment.holdername  = self.textForHolderNameCC.text!
        payment.ccnumber    = self.textForNumberCC.text!
        payment.cccvc       = self.textForCVCCC.text!
        payment.ccexpmonth  = String(Calendar.current.component(.month, from: self.expDate))
        payment.ccexpyear   = String(Calendar.current.component(.year, from: self.expDate))
        payment.email       = self.textForEmailCC.text!
        payment.address     = self.textForBillingAddressCC.text!
        payment.postal      = self.textForPostcodeCC.text!
        payment.city        = self.textForCityCC.text!
        payment.country     = self.textForCountryCC.text!
        payment.birthdate   = self.textForBirthdateCC.text!
        
        moveStep4View()
    }
    
    private func transactionFromBank()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        PMManager.initWithTestMode(C.PAYMILL_TEST_MODE, merchantPublicKey: C.PAYMILL_PUBLIC_KEY, newDeviceId: nil)
        { (success, error) -> Void in
            if success{
                print("successfully initialized PayMillSDK")
                do {
                    let paymentMethod   =  try PMFactory.genNationalPayment(withIban: self.textForIBAN.text, andBic: self.textForBIC.text, accHolder: self.textForClientName.text)
                    let params          = try PMFactory.genPaymentParams(withCurrency: "EUR", amount: Int32(self.paymentAmount), description: C.PAYMENT_DESCRIPTION)
                    
                    PMManager.transaction(with: paymentMethod, parameters: params, consumable: true, success:
                        { (transaction) in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            self.moveStep5View()
                    })
                    { (error) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.view.makeToast("Transaction Failed")
                    }
                }
                catch
                {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.view.makeToast("Transaction Failed")
                    print(error)
                }
            }
            else{
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast("Transaction Failed")
            }
        }
    }
    
    private func transactionFromCreditCard()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        PMManager.initWithTestMode(C.PAYMILL_TEST_MODE, merchantPublicKey: C.PAYMILL_PUBLIC_KEY, newDeviceId: nil)
        { (success, error) -> Void in
            if success{
                print("successfully initialized PayMillSDK")
                do {
                    print("month = \(Calendar.current.component(.month, from: self.expDate)) year = \(Calendar.current.component(.year, from: self.expDate))")
                    let paymentMethod   = try PMFactory.genCardPayment(withAccHolder: self.textForHolderNameCC.text, cardNumber: self.textForNumberCC.text, expiryMonth: String(Calendar.current.component(.month, from: self.expDate)), expiryYear: String(Calendar.current.component(.year, from: self.expDate)), verification: self.textForCVCCC.text)
                    
                    let params          = try PMFactory.genPaymentParams(withCurrency: "EUR", amount: Int32(self.paymentAmount), description: C.PAYMENT_DESCRIPTION)
                    
                    PMManager.transaction(with: paymentMethod, parameters: params, consumable: true, success:
                        { (transaction) in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            self.moveStep5View()
                    })
                    { (error) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.view.makeToast("Transaction Failed")
                    }
                }
                catch
                {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.view.makeToast("Transaction Failed")
                    print(error)
                }
            }
            else{
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast("Transaction Failed")
            }
        }
    }
   
    private func payFromPayPal()
    {
        let payment = PayPalPayment(amount: NSDecimalNumber(string:String(self.paymentAmount)), currencyCode: "EUR", shortDescription: C.PAYMENT_DESCRIPTION, intent: .sale)
        
        if (payment.processable)
        {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else
        {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
    }

    @objc func successSetupPayment(_ notification: NSNotification)
    {
        let user            = UserManager.getLoggedInUser()
        user?.accounttype   = "1"
        UserManager.updateLogInUser(user: user!)
        
        mainViewController.removeModelView(viewController: self)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    @objc func failedSetupPayment(_ notification: NSNotification)
    {
        self.view.makeToast("Failed to setup payment")
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    // PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController)
    {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment)
    {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            self.moveStep5View()
        })
    }
    
    public func drawingView(_ view: ACEDrawingView!, didEndDrawUsing tool: ACEDrawingTool!)
    {
        self.signImage          = view.image
    }

    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField    == self.textForCountry
        {
            hideAllKeyboard()
        }
        else if textField   == self.textForBirthdate
        {
            hideAllKeyboard()
        }
    }
    
    @IBAction func buttonStep1Clicked(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1:
            hideAllView()
            self.viewForStep2.isHidden  = false
            break
        default:
            break
        }
    }
    
    @IBAction func buttonStep2Clicked(_ sender: UIButton)
    {
        hideAllView()
        self.paymentOption  = sender.tag
        self.viewForStep3.isHidden  = false
        initStep3View()
    }
    
    @IBAction func buttonStep3Clicked(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1:
            hideAllView()
            self.viewForStep2.isHidden  = false
            break
        case 2:
            checkField()
            break
        default:
            break
        }
    }
    
    @IBAction func buttonStep4Clicked(_ sender: UIButton)
    {        
        switch sender.tag
        {
        case 1:
            hideAllView()
            self.viewForStep3.isHidden  = false
            break
        case 2:
            sendPayment()
            break            
        default:
            break
        }
    }
    
    @IBAction func buttonStep5Clicked(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1:
            mainViewController.removeModelView(viewController: self)
            break
        case 2:
            hideAllView()
            let image                   = UIImage(view: self.viewForDrawingArea)
            let reduceSize              = CGSize(width: 300, height: 300)
            self.signImage              = image.resizeImage(toSize: reduceSize)
            self.viewForStep6.isHidden  = false
            break
        case 3: // erase
            self.viewForDrawingArea.clear()
            break
        default:
            break
        }
    }
    
    @IBAction func buttonStep6Clicked(_ sender: UIButton)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let paylog              = Paylog()
        paylog.user_id          = self.user.id
        
        let formatter           = DateFormatter()
        formatter.dateFormat    = "yyyy-MM-dd HH:mm:ss Z"
        
        switch self.paymentMethod
        {
        case 1:
            paylog.method   = "Bank"
            break
        case 2:
            paylog.method   = "Paypal"
            break
        case 3:
            paylog.method   = "Credit Card"
            break
        default:
            break
        }
        
        switch self.paymentOption
        {
        case 1:
            paylog.duration = "6"
            break
        case 2:
            paylog.duration = "12"
            break
        case 3:
            paylog.duration = "24"
            break
        default:
            break
        }
        paylog.amount       = String(self.paymentAmount)
        paylog.paid_date    = formatter.string(from: Date())
        
        ClientService.setupPayment(parameters: paylog.toDictionary()!)
    }
    
    @IBAction func buttonPaymentSelectClicked(_ sender: UIButton)
    {
        initStep3PaymentMethodButtons()
        
        sender.layer.borderColor    = ThemeUtil.getPaymentSelectedBorderColor().cgColor
        self.paymentMethod          = sender.tag
        
        hideAllPaymentView()
        
        switch sender.tag
        {
        case 1: // bank
            self.viewForPaymentBank.isHidden        = false
            break
        case 2: // paypal
            payFromPayPal()
            break
        case 3: // credit card
            self.viewForPaymentCreditCard.isHidden  = false
            break
        default:
            break
        }
    }
    
    @IBAction func popUpClicked(_ sender: UIButton)
    {
        let tag                 = sender.tag
        
        switch tag
        {
        case 1:
            ActionSheetStringPicker.show(withTitle: "Land", rows: ["Deutschland", "Österreich", "Schweiz", "Liechtenstein", "Luxemburg", "Ungarn"], initialSelection: 0, doneBlock: {
                picker, value, index in
                
                self.textForCountry.text  = String(describing: index!)
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview)
            break
        case 2:
            let datePicker = ActionSheetDatePicker(title: "Geburtsdatum:", datePickerMode: UIDatePickerMode.date, selectedDate: Date(), doneBlock: {
                picker, value, index in
                
                let formatter           = DateFormatter()
                formatter.dateFormat    = "yyyy-MM-dd HH:mm:ss Z"
                let dateObj             = formatter.date( from: String(describing: value!))
                
                formatter.dateFormat    = "d. MMMM yyyy"
                formatter.locale        = NSLocale(localeIdentifier: "de_DE") as Locale!
                let result              = formatter.string(from: dateObj!)
                
                self.textForBirthdate.text          = result
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview)
            datePicker?.maximumDate = Date()
            
            datePicker?.show()
            break
        case 3:
            let datePicker = ActionSheetDatePicker(title: nil, datePickerMode: UIDatePickerMode.date, selectedDate: Date(), doneBlock: {
                picker, value, index in
                
                let formatter           = DateFormatter()
                formatter.dateFormat    = "yyyy-MM-dd HH:mm:ss Z"
                let dateObj             = formatter.date( from: String(describing: value!))
                
                formatter.dateFormat    = "MMMM yyyy"
                formatter.locale        = NSLocale(localeIdentifier: "de_DE") as Locale!
                let result              = formatter.string(from: dateObj!)
                
                self.expDate                = dateObj
                self.textForExpDateCC.text  = result
                
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview)
            datePicker?.minimumDate = Date()
            
            datePicker?.show()
            break
        case 4:
            ActionSheetStringPicker.show(withTitle: "Land", rows: ["Deutschland", "Österreich", "Schweiz", "Liechtenstein", "Luxemburg", "Ungarn"], initialSelection: 0, doneBlock: {
                picker, value, index in
                
                self.textForCountryCC.text  = String(describing: index!)
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview)
            break
        case 5:
            let datePicker = ActionSheetDatePicker(title: "Geburtsdatum:", datePickerMode: UIDatePickerMode.date, selectedDate: Date(), doneBlock: {
                picker, value, index in
                
                let formatter           = DateFormatter()
                formatter.dateFormat    = "yyyy-MM-dd HH:mm:ss Z"
                let dateObj             = formatter.date( from: String(describing: value!))
                
                formatter.dateFormat    = "d. MMMM yyyy"
                formatter.locale        = NSLocale(localeIdentifier: "de_DE") as Locale!
                let result              = formatter.string(from: dateObj!)
                
                self.textForBirthdateCC.text          = result
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview)
            datePicker?.maximumDate = Date()
            
            datePicker?.show()
            break
        default:
            break
        }
        
        hideAllKeyboard()
    }
}
