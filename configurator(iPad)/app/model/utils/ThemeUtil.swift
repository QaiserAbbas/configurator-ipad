//
// Copyright (c) 2016 Frazzle. All rights reserved.
//

import Foundation
import UIColor_Hex_Swift

class ThemeUtil {

    static func getModeColor(type:C.MainMode)->UIColor
    {
        switch type
        {
        case C.MainMode.MAIN_EMOTION:
            return UIColor(C.COLOR_MAIN_EMOTION)
        case C.MainMode.MAIN_POINT:
            return UIColor(C.COLOR_MAIN_POINT)
        case C.MainMode.MAIN_DRAW:
            return UIColor(C.COLOR_MAIN_DRAW)
        case C.MainMode.MAIN_DEFINED:
            return UIColor(C.COLOR_MAIN_DEFINED)
        case C.MainMode.MAIN_HAIR:
            return UIColor(C.COLOR_MAIN_HAIR)
        case C.MainMode.MAIN_INIT:
            return UIColor(C.COLOR_MAIN_INIT)
        }
    }

    static func getStateColor(type:C.ButtonState)->UIColor
    {
        switch type
        {
        case C.ButtonState.NORMAL_STATE:
            return UIColor(C.COLOR_LABEL_NORMAL)
        case C.ButtonState.SELECTED_STATE:
            return UIColor(C.COLOR_LABEL_SELECTED)
        }
    }
    
    static func getGenderButtonColor(type:C.ButtonState)->UIColor
    {
        switch type
        {
        case C.ButtonState.NORMAL_STATE:
            return UIColor(C.COLOR_GENDER_BUTTON_NORMAL)
        case C.ButtonState.SELECTED_STATE:
            return UIColor(C.COLOR_GENDER_BUTTON_SELECTED)
        }
    }
    
    static func getGenderLabelColor(type:C.ButtonState)->UIColor
    {
        switch type
        {
        case C.ButtonState.NORMAL_STATE:
            return UIColor(C.COLOR_GENDER_LABEL_NORMAL)
        case C.ButtonState.SELECTED_STATE:
            return UIColor(C.COLOR_GENDER_LABEL_SELECTED)
        }
    }
    
    static func getMainCategoryBorderColor()->UIColor
    {
        return UIColor(C.COLOR_MAIN_CATEGORY_BORDER)
    }
    
    static func getMainCategoryLabelColor()->UIColor
    {
        return UIColor(C.COLOR_MAIN_CATEGORY_LABEL)
    }
    
    static func getSubCategoryBorderColor()->UIColor
    {
        return UIColor(C.COLOR_MAIN_CATEGORY_BORDER)
    }
    
    static func getHairSectionBorderColor()->UIColor
    {
        return UIColor(C.COLOR_HAIR_SECTION_BORDER)
    }
    
    static func getHairFirstStepLabelColor()->UIColor
    {
        return UIColor(C.COLOR_HAIR_FIRST_STEP_LABEL)
    }
    
    static func getHairOtherStepLabelColor()->UIColor
    {
        return UIColor(C.COLOR_HAIR_OTHER_STEP_LABEL)
    }
    
    static func getHairCategoryLabelColor()->UIColor
    {
        return UIColor(C.COLOR_HAIR_CATEGORY_LABEL)
    }
    
    static func getPointInfoLineLabelColor()->UIColor
    {
        return UIColor(C.COLOR_POINT_INFO_LINE_LABEL)
    }
    
    static func getPointInfoImageBorderColor()->UIColor
    {
        return UIColor(C.COLOR_POINT_INFO_IMAGE_BORDER)
    }
    
    static func getPaymentNormalBorderColor()->UIColor
    {
        return UIColor(C.COLOR_PAYMENT_MORMAL_BORDER)
    }
    
    static func getPaymentSelectedBorderColor()->UIColor
    {
        return UIColor(C.COLOR_PAYMENT_SELECTED_BORDER)
    }
    
    static func getCalendarHeaderColor()->UIColor
    {
        return UIColor(C.COLOR_CALENDAR_HEADER)
    }

    static func getSummaryDateColor()->UIColor
    {
        return UIColor(C.COLOR_SUMMARY_DATE)
    }
    
    static func getProductDescriptionColor(custom:String)->UIColor
    {
        if custom == "1"
        {
            return UIColor(C.COLOR_CATEGORY_CUSTOM)
        }
        else
        {
            return UIColor(C.COLOR_CATEGORY_GENERAL)
        }        
    }
}
