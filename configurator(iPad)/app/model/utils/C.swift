//
// Copyright (c) 2016 Frazzle. All rights reserved.
//

import Foundation
import UIKit

class C
{
    static let DEBUG:Bool = true

//    static let BASE_URL:String              = "http://192.168.0.11"
    static let BASE_URL:String              = "http://83.169.4.108"
    
    static let WEB_SERVICES_VERSION:String  = "/user/api"
    static let URL_SERVER:String            = BASE_URL + WEB_SERVICES_VERSION
    static let PAYMILL_TEST_MODE:Bool       = true
    static let PAYMILL_PUBLIC_KEY:String    = "805618977950f34e2a1d0ba1c352a956"
    
    static let PAYPAL_SANDBOX_ID:String     = "AXoXdDxk5WysTceN45sqMG6lZ_d0XxG6zF4wMgvSiR5xkAk3K07Uj-utA-xgD20BZXD58m-u5tR0FpSe"
    static let PAYPAL_LIVE_ID:String        = "805618977950f34e2a1d0ba1c352a956"
    static let PAYMENT_DESCRIPTION:String   = "Description"
    
    static let HEADERS = [
            "Content-Type": "application/json"
    ]

    enum TimelineType
    {
        case SHOPS
        case HERBS
        case TUNES
        case VIBES
        case LIVE
        case LIVE_PAST
        case SEARCH
    }

    enum UsersType
    {
        case PROFILES
        case FOLLOWERS
        case FOLLOWING
        case SEARCH
    }

    enum TabType: String
    {
        case HOME_HERBS
        case HOME_SHOPS
        case HOME_TUNES
        case HOME_VIBES
        case SHOPS
        case USERS_PROFILES
        case USERS_FOLLOWERS
        case USERS_FOLLOWING
        case LIVE
        case SEARCH
    }

    enum MainMode: String
    {
        case MAIN_EMOTION
        case MAIN_POINT
        case MAIN_DRAW
        case MAIN_DEFINED
        case MAIN_HAIR
        case MAIN_INIT
    }

    enum ButtonState: String
    {
        case NORMAL_STATE
        case SELECTED_STATE
    }
    
    static let COLOR_LABEL_NORMAL:String            = "#1F2833"
    static let COLOR_LABEL_SELECTED:String          = "#FFFFFF"
    
    static let COLOR_MAIN_CATEGORY_BORDER:String    = "#EBEBEB"
    static let COLOR_MAIN_CATEGORY_LABEL:String     = "#A6A9AD"
    
    static let COLOR_SUB_CATEGORY_BORDER:String     = "#F2EFEF"
    
    static let COLOR_GENDER_BUTTON_NORMAL:String    = "#F3F4F2"
    static let COLOR_GENDER_BUTTON_SELECTED:String  = "#E6648E"
    static let COLOR_GENDER_LABEL_NORMAL:String     = "#A8BEC4"
    static let COLOR_GENDER_LABEL_SELECTED:String   = "#FFFFFF"
    
    static let COLOR_MAIN_EMOTION:String            = "#5972CB"
    static let COLOR_MAIN_POINT:String              = "#2BB0D4"
    static let COLOR_MAIN_DRAW:String               = "#83D7A4"
    static let COLOR_MAIN_DEFINED:String            = "#F5AF62"
    static let COLOR_MAIN_HAIR:String               = "#F67272"
    static let COLOR_MAIN_INIT:String               = "#FFFFFF"
    
    static let COLOR_HAIR_SECTION_BORDER:String     = "#F2F2F2"
    static let COLOR_HAIR_FIRST_STEP_LABEL:String   = "#1D1E1C"
    static let COLOR_HAIR_OTHER_STEP_LABEL:String   = "#A6A9AD"
    static let COLOR_HAIR_CATEGORY_LABEL:String     = "#B6BCBD"
    static let COLOR_POINT_INFO_LINE_LABEL:String   = "#37B1D3"
    static let COLOR_POINT_INFO_IMAGE_BORDER:String = "#F2F3F4"

    static let COLOR_PAYMENT_MORMAL_BORDER:String   = "#EBEBEB"
    static let COLOR_PAYMENT_SELECTED_BORDER:String = "#2BB0D4"
    
    static let COLOR_CATEGORY_CUSTOM:String         = "#7BCAAB"
    static let COLOR_CATEGORY_GENERAL:String        = "#2BB0D4"
    
    static let COLOR_CALENDAR_HEADER:String         = "#A0A3A8"
    static let COLOR_SUMMARY_DATE:String            = "#262627"
    
    static let NEW_PHOTO_CHANGED:String             = "NEW_PHOTO_CHANGED"
    static let NEW_PHOTO_CHANGED_WORKSPACE:String   = "NEW_PHOTO_CHANGED_WORKSPACE"
    
    static let REMOVE_SUB_VIEW:String               = "REMOVE_SUB_VIEW"
    
    static let USER_LOGGED_IN:String                = "USER_LOGGED_IN"
    static let USER_NOT_LOGGED_IN:String            = "USER_NOT_LOGGED_IN"
    static let USER_SIGNED_UP:String                = "USER_SIGNED_UP"
    static let USER_NOT_SIGNED_UP:String            = "USER_NOT_SIGNED_UP"
    static let USER_LOGGED_OUT:String               = "USER_LOGGED_OUT"
    static let USER_UPDATED:String                  = "USER_UPDATED"
    static let USER_FAILED_UPDATED:String           = "USER_FAILED_UPDATED"
    
    static let CLIENTS_DOWNLOADED:String            = "CLIENTS_DOWNLOADED"
    static let CLIENTS_EMPTY_DOWNLOADED:String      = "CLIENTS_EMPTY_DOWNLOADED"
    static let CLIENTS_FAILED_DOWNLOADED:String     = "CLIENTS_FAILED_DOWNLOADED"
    
    static let CLIENT_CREATED:String                = "CLIENT_CREATED"
    static let CLIENT_FAILED_CREATED:String         = "CLIENT_FAILED_CREATED"
    
    static let CLIENT_UPDATED:String                = "CLIENT_UPDATED"
    static let CLIENT_UPDATED_WITH_IMAGE:String     = "CLIENT_UPDATED_WITH_IMAGE"
    static let CLIENT_FAILED_UPDATED:String         = "CLIENT_FAILED_UPDATED"
    
    static let CLIENT_REMOVED:String                = "CLIENT_REMOVED"
    static let CLIENT_FAILED_REMOVED:String         = "CLIENT_FAILED_REMOVED"

    static let BRANDS_DOWNLOADED:String             = "BRANDS_DOWNLOADED"
    static let BRANDS_EMPTY_DOWNLOADED:String       = "BRANDS_EMPTY_DOWNLOADED"
    static let BRANDS_FAILED_DOWNLOADED:String      = "BRANDS_FAILED_DOWNLOADED"

    static let CATEGORIES_DOWNLOADED:String         = "CATEGORIES_DOWNLOADED"
    static let CATEGORIES_EMPTY_DOWNLOADED:String   = "CATEGORIES_EMPTY_DOWNLOADED"
    static let CATEGORIES_FAILED_DOWNLOADED:String  = "CATEGORIES_FAILED_DOWNLOADED"
    
    static let CBP_LIST_DOWNLOADED:String           = "CATEGORIES_DOWNLOADED"
    static let CBP_LIST_EMPTY_DOWNLOADED:String     = "CATEGORIES_EMPTY_DOWNLOADED"
    static let CBP_LIST_FAILED_DOWNLOADED:String    = "CATEGORIES_FAILED_DOWNLOADED"

    static let PRODUCTS_DOWNLOADED:String           = "PRODUCTS_DOWNLOADED"
    static let PRODUCTS_EMPTY_DOWNLOADED:String     = "PRODUCTS_EMPTY_DOWNLOADED"
    static let PRODUCTS_FAILED_DOWNLOADED:String    = "PRODUCTS_FAILED_DOWNLOADED"

    static let PRODUCTS_SELECTED:String             = "PRODUCTS_SELECTED"
    static let PRODUCTS_FAILED_SELECTED:String      = "PRODUCTS_FAILED_SELECTED"

    static let PRODUCTS_REMOVED:String              = "PRODUCTS_REMOVED"
    static let PRODUCTS_FAILED_REMOVED:String       = "PRODUCTS_FAILED_REMOVED"
    
    static let PRODUCT_CREATED:String               = "PRODUCT_CREATED"
    static let PRODUCT_FAILED_CREATED:String        = "PRODUCT_FAILED_CREATED"

    static let PAYLOG_CREATED:String                = "PAYLOG_CREATED"
    static let PAYLOG_FAILED_CREATED:String         = "PAYLOG_FAILED_CREATED"

    static let UPDATE_CLIENT_BDATE_SUCCESS:String   = "UPDATE_CLIENT_BDATE_SUCCESS"
    static let UPDATE_CLIENT_BDATE_FAILED:String    = "UPDATE_CLIENT_BDATE_FAILED"

    static let REMOVE_CLIENT_BDATE_SUCCESS:String   = "REMOVE_CLIENT_BDATE_SUCCESS"
    static let REMOVE_CLIENT_BDATE_FAILED:String    = "REMOVE_CLIENT_BDATE_FAILED"
    
    static let START_INDEX:Int                      = 100
    static let HAIR_STOP_BLINK:String               = "HAIR_STOP_BLINK"
    static let HAIR_HIDE_ANIMATION:String           = "HAIR_HIDE_ANIMATION"
    static let HAIR_HINE_ANIMATION_DONE:String      = "HAIR_HIDE_ANIMATION_DONE"
    static let HAIR_ADD_PRODUCT_DESCRIPTION:String  = "HAIR_ADD_PRODUCT_DESCRIPTION"
    static let HAIR_REMINDER_KEY:String             = "HAIR_REMINDER_KEY"
    
    static let CATEGORY_LIST:[String]               = ["Reinigung", "Peeling", "Toner", "Tagespflege", "Nachtpflege", "Augenpflege",
                                                       "Serum", "Maske", "Individuelle Empfehlung"]
    
    static let BOTTOM_BAR_ANIMATION_DURATION        = 0.4
    
    static let PAYMENT_OPTION_6                     = 79
    static let PAYMENT_OPTION_12                    = 69
    static let PAYMENT_OPTION_24                    = 59
}
