//
// Copyright (c) 2016 Frazzle. All rights reserved.
//

import Foundation
import ObjectMapper

class Client : Mappable
{
    var id                      :String
    var userid                  :String
    var clientname              :String
    var phonenumber             :String
    var email                   :String
    var address                 :String
    var postal                  :String
    var city                    :String
    var country                 :String
    var birthdate               :String
    var age                     :String
    var gender                  :String
    var image                   :String
    var points                  :String
    var objects                 :Objects?
    var questions               :String
    var hairQuestions           :HairQuestions
    var finished                :String
    var draft                   :String
    var points_count            :String
    var points_image            :String
    var signature               :String
    var emotions_shown          :String
    var beauty_treatment        :String
    var beauty_treatment_date   :String
    var is_email_checked        :String
    var date_last_saved         :String
    var appointment_date        :String

    init()
    {
        id                      = ""
        userid                  = ""
        clientname              = ""
        phonenumber             = ""
        email                   = ""
        address                 = ""
        postal                  = ""
        city                    = ""
        country                 = ""
        birthdate               = ""
        age                     = ""
        gender                  = ""
        image                   = ""
        points                  = ""
        objects                 = Objects()
        questions               = ""
        hairQuestions           = HairQuestions()
        finished                = "0"
        draft                   = ""
        points_count            = ""
        points_image            = ""
        signature               = ""
        emotions_shown          = ""
        beauty_treatment        = ""
        beauty_treatment_date   = ""
        is_email_checked        = ""
        date_last_saved         = ""
        appointment_date        = ""
    }

    required init?(map: Map)
    {
        id                      = ""
        userid                  = ""
        clientname              = ""
        phonenumber             = ""
        email                   = ""
        address                 = ""
        postal                  = ""
        city                    = ""
        country                 = ""
        birthdate               = ""
        age                     = ""
        gender                  = ""
        image                   = ""
        points                  = ""
        objects                 = Objects()
        questions               = ""
        hairQuestions           = HairQuestions()
        finished                = "0"
        draft                   = ""
        points_count            = ""
        points_image            = ""
        signature               = ""
        emotions_shown          = ""
        beauty_treatment        = ""
        beauty_treatment_date   = ""
        is_email_checked        = ""
        date_last_saved         = ""
        appointment_date        = ""
    }

    // Mappable
    func mapping(map: Map)
    {
        id                      <- map["id"]
        userid                  <- map["userid"]
        clientname              <- map["clientname"]
        phonenumber             <- map["phonenumber"]
        email                   <- map["email"]
        address                 <- map["address"]
        postal                  <- map["postal"]
        city                    <- map["city"]
        country                 <- map["country"]
        birthdate               <- map["birthdate"]
        age                     <- map["age"]
        gender                  <- map["gender"]
        image                   <- map["image"]
        points                  <- map["points"]
        questions               <- map["hairQuestions"]
//        objects               <- map["points"]
        finished                <- map["finished"]
        draft                   <- map["draft"]
        points_count            <- map["points_count"]
        points_image            <- map["points_image"]
        signature               <- map["signature"]
        emotions_shown          <- map["emotions_shown"]
        beauty_treatment        <- map["beauty_treatment"]
        beauty_treatment_date   <- map["beauty_treatment_date"]
        is_email_checked        <- map["is_email_checked"]
        date_last_saved         <- map["date_last_saved"]
        appointment_date        <- map["appointment_date"]
    }

    static func fromJson(json:String)->Client
    {
        let client:Client = Mapper<Client>().map(JSONString: json)!
        client.setObjects()
        client.setHairQuestions()
        
        return client
    }

    static func fromJsonToList(json:String) throws ->Array<Client>
    {
        var items   = Array<Client>()

        if(json     != "")
        {
            //var error: NSError
            if let data     = json.data(using: String.Encoding.utf8)
            {
                if let _    = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [[String: AnyObject]]
                {
                    items   = Mapper<Client>().mapArray(JSONString: json)!
                }
            }
        }
        return items
    }

    func toJson()->String
    {
        return Mapper().toJSONString(self, prettyPrint: true)!
    }
    
    func toDictionary()->[String: Any]?
    {
        print("json = \(self.toJson())")
        if let data = self.toJson().data(using: .utf8)
        {
            do
            {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func toJsonFromList(items:Array<Client>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: true)!
    }
    
    func setObjects()
    {
        var objs:Objects = Objects()
        if (self.points != "")
        {
            if let data     = self.points.data(using: String.Encoding.utf8)
            {
                objs        = Objects.fromJson(json: String.init(data: data, encoding: String.Encoding.utf8)!)
//                print(objs.toJson())
            }
        }
        
        self.objects    = objs
    }
    
    func setHairQuestions()
    {
        var objs:HairQuestions = HairQuestions()
        if (self.questions != "")
        {
            if let data     = self.questions.data(using: String.Encoding.utf8)
            {
                objs        = HairQuestions.fromJson(json: String.init(data: data, encoding: String.Encoding.utf8)!)
                print(objs.toJson())
            }
        }
        
        self.hairQuestions    = objs
    }
    /*
     {
     "type":"image",
     "originX":"left",
     "originY":"top",
     "left":65,
     "top":-1.5,
     "width":794,
     "height":711,
     "fill":"rgb(0,0,0)",
     "stroke":null,
     "strokeWidth":0,
     "strokeDashArray":null,
     "strokeLineCap":"butt",
     "strokeLineJoin":"miter",
     "strokeMiterLimit":10,
     "scaleX":1,
     "scaleY":1,
     "angle":0,
     "flipX":false,
     "flipY":false,
     "opacity":1,
     "shadow":null,
     "visible":true,
     "clipTo":null,
     "backgroundColor":"",
     "fillRule":"nonzero",
     "globalCompositeOperation":"source-over",
     "transformMatrix":null,
     "skewX":0,
     "skewY":0,
     "src":"http://192.168.0.123/upload/model_2.png",
     "filters":[
     ],
     "resizeFilters":[
     ],
     "crossOrigin":"",
     "alignX":"none",
     "alignY":"none",
     "meetOrSlice":"meet",
     "id":0,
     "createdWidth":924,
     "hasControls":false,
     "hasBorders":false,
     "lockMovementX":false,
     "lockMovementY":false,
     "selectable":false
     }
    */


}
