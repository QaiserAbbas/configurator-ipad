//
//  payment.swift
//  configurator(iPad)
//
//  Created by MobileSuperMaster on 7/14/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import Foundation
import ObjectMapper

class Payment : Mappable
{
    var id              :String
    var userid          :String
    var holdername      :String
    var iban            :String // bank
    var bic             :String // bank
    
    var ccnumber        :String // CC
    var ccexpmonth      :String // CC
    var ccexpyear       :String // CC
    var cccvc           :String // CC
    
    var email           :String
    var address         :String
    var postal          :String
    var city            :String
    var country         :String
    var birthdate       :String
    var age             :String
    var gender          :String
    var method          :String
    var type            :String
    
    init()
    {
        id              = ""
        userid          = ""
        holdername      = ""
        iban            = ""
        ccnumber        = ""
        ccexpmonth      = ""
        ccexpyear       = ""
        cccvc           = ""
        bic             = ""
        email           = ""
        address         = ""
        postal          = ""
        city            = ""
        country         = ""
        birthdate       = ""
        age             = ""
        gender          = ""
        method          = ""
        type            = ""
    }
    
    required init?(map: Map)
    {
        id              = ""
        userid          = ""
        holdername      = ""
        iban            = ""
        bic             = ""
        ccnumber        = ""
        ccexpmonth      = ""
        ccexpyear       = ""
        cccvc           = ""
        email           = ""
        address         = ""
        postal          = ""
        city            = ""
        country         = ""
        birthdate       = ""
        age             = ""
        gender          = ""
        method          = ""
        type            = ""
    }
    
    // Mappable
    func mapping(map: Map)
    {
        id              <- map["id"]
        userid          <- map["userid"]
        holdername      <- map["holdername"]
        iban            <- map["iban"]
        bic             <- map["bic"]
        email           <- map["email"]
        address         <- map["address"]
        ccnumber        <- map["ccnumber"]
        ccexpmonth      <- map["ccexpmonth"]
        ccexpyear       <- map["ccexpyear"]
        cccvc           <- map["cccvc"]
        postal          <- map["postal"]
        city            <- map["city"]
        country         <- map["country"]
        birthdate       <- map["birthdate"]
        age             <- map["age"]
        gender          <- map["gender"]
        method          <- map["method"]
        type            <- map["type"]
    }
    
    static func fromJson(json:String)->Payment
    {
        let payment:Payment = Mapper<Payment>().map(JSONString: json)!
        
        return payment
    }
    
    static func fromJsonToList(json:String) throws ->Array<Payment>
    {
        var items   = Array<Payment>()
        if(json     != "")
        {
            //var error: NSError
            if let data     = json.data(using: String.Encoding.utf8)
            {
                if let _    = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [[String: AnyObject]]
                {
                    items   = Mapper<Payment>().mapArray(JSONString: json)!
                }
            }
        }
        return items
    }
    
    func toJson()->String
    {
        return Mapper().toJSONString(self, prettyPrint: true)!
    }
    
    func toDictionary()->[String: Any]?
    {
        print("json = \(self.toJson())")
        if let data = self.toJson().data(using: .utf8)
        {
            do
            {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func toJsonFromList(items:Array<Payment>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: true)!
    }
}
