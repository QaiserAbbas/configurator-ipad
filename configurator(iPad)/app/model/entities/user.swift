//
// Copyright (c) 2016 Frazzle. All rights reserved.
//

import Foundation
import ObjectMapper

class User : Mappable
{
    var id              :String
    var username        :String
    var password        :String
    var usertype        :String
    var company         :String
    var fullname        :String
    var street          :String
    var postal          :String
    var city            :String
    var country         :String
    var avatar          :String
    var category        :String
    var language        :String
    var state           :String
    var qualification   :String
    var website         :String
    var phonenumber     :String
    var accounttype     :String
    var freecount       :String

    init()
    {
        id              = ""
        username        = ""
        password        = ""
        usertype        = ""
        company         = ""
        fullname        = ""
        street          = ""
        postal          = ""
        city            = ""
        country         = ""
        avatar          = ""
        category        = ""
        language        = ""
        state           = ""
        qualification   = ""
        website         = ""
        phonenumber     = ""
        accounttype     = ""
        freecount       = ""
    }

    required init?(map: Map)
    {
        id              = ""
        username        = ""
        password        = ""
        usertype        = ""
        company         = ""
        fullname        = ""
        street          = ""
        postal          = ""
        city            = ""
        country         = ""
        avatar          = ""
        category        = ""
        language        = ""
        state           = ""
        qualification   = ""
        website         = ""
        phonenumber     = ""
        accounttype     = ""
        freecount       = ""
    }

    // Mappable
    func mapping(map: Map)
    {
        id              <- map["id"]
        username        <- map["username"]
        password        <- map["password"]
        usertype        <- map["usertype"]
        company         <- map["company"]
        fullname        <- map["fullname"]
        street          <- map["street"]
        postal          <- map["postal"]
        city            <- map["city"]
        country         <- map["country"]
        avatar          <- map["avatar"]
        category        <- map["category"]
        language        <- map["language"]
        state           <- map["state"]
        qualification   <- map["qualification"]
        website         <- map["website"]
        phonenumber     <- map["phonenumber"]
        accounttype     <- map["account_type"]
        freecount       <- map["free_count"]
    }

    static func fromJson(json:String)->User
    {
        return Mapper<User>().map(JSONString: json)!;
    }

    static func fromJsonToList(json:String) throws ->Array<User>
    {
        var items   = Array<User>()
        if(json     != "")
        {
            //var error: NSError
            if let data     = json.data(using: String.Encoding.utf8)
            {
                if let _    = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: AnyObject]
                {
                    items   = Mapper<User>().mapArray(JSONString: json)!
                }
            }
        }
        return items
    }



    func toJson()->String
    {
        return Mapper().toJSONString(self, prettyPrint: true)!
    }
    
    static func toJsonFromList(items:Array<User>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: true)!
    }
    
    func toDictionary()->[String: Any]?
    {
        if let data = self.toJson().data(using: .utf8)
        {
            do
            {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
