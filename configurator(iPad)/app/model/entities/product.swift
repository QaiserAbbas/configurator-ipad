//
// Copyright (c) 2016 Frazzle. All rights reserved.
//

import Foundation
import ObjectMapper

class Product : NSObject, Mappable
{
    var id              :String
    var name            :String
    var descriptions    :String
    var image_url       :String
    var selected        :Bool
    var category        :String
    var brand_id        :String
    var brand_name      :String
    var custom          :String
    var cycle           :String

    override init()
    {
        id              = ""
        name            = ""
        descriptions    = ""
        image_url       = ""
        selected        = false
        category        = ""
        brand_id        = ""
        brand_name      = ""
        custom          = ""
        cycle           = ""
    }

    required init?(map: Map)
    {
        id              = ""
        name            = ""
        descriptions    = ""
        image_url       = ""
        selected        = false
        category        = ""
        brand_id        = ""
        brand_name      = ""
        custom          = ""
        cycle           = ""
    }

    // Mappable
    func mapping(map: Map)
    {
        id              <- map["id"]
        name            <- map["name"]
        descriptions    <- map["description"]
        image_url       <- map["image"]
        selected        <- map["selected"]
        category        <- map["category"]
        brand_id        <- map["brand_id"]
        brand_name      <- map["brand_name"]
        custom          <- map["custom"]
        cycle           <- map["cycle"]
    }
    
    func toJson()->String
    {
        return Mapper().toJSONString(self, prettyPrint: true)!
    }
    
    static func fromJson(json:String)->Product
    {
        return Mapper<Product>().map(JSONString: json)!;
    }
    
    static func fromJsonToList(json:String) throws ->Array<Product>
    {
        var items   = Array<Product>()
        if(json     != "")
        {
            //var error: NSError
            if let data     = json.data(using: String.Encoding.utf8)
            {
                if let _    = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [[String: AnyObject]]
                {
                    items   = Mapper<Product>().mapArray(JSONString: json)!
                }
            }
        }
        return items
    }
    
    static func toJsonFromList(items:Array<Product>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: true)!
    }
}
