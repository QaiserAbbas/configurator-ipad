//
// Copyright (c) 2016 Frazzle. All rights reserved.
//

import Foundation
import ObjectMapper

class PathOffset : Mappable
{
    var x       :Float
    var y       :Float

    init()
    {
        x       = 0
        y       = 0
    }

    required init?(map: Map)
    {
        x       = 0
        y       = 0
    }

    // Mappable
    func mapping(map: Map)
    {
        x       <- map["x"]
        y       <- map["y"]
    }

    static func fromJson(json:String)->User
    {
        return Mapper<User>().map(JSONString: json)!;
    }

    func toJson()->String
    {
        return Mapper().toJSONString(self, prettyPrint: true)!
    }
}
