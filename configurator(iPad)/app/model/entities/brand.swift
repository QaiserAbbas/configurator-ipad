//
// Copyright (c) 2016 Frazzle. All rights reserved.
//

import Foundation
import ObjectMapper

class Brand : Mappable
{
    var id              :String
    var name            :String
    var image_url       :String

    var products        :Array<Product>

    init()
    {
        id              = ""
        name            = ""
        image_url       = ""
        products        = []
    }

    required init?(map: Map)
    {
        id              = ""
        name            = ""
        image_url       = ""
        products        = []
    }

    // Mappable
    func mapping(map: Map)
    {
        id              <- map["id"]        
        name            <- map["name"]
        image_url       <- map["image"]
        products        <- map["products"]
    }
    
    static func fromJsonToList(json:String) throws ->Array<Brand>
    {
        var items   = Array<Brand>()
        if(json     != "")
        {
            //var error: NSError
            if let data     = json.data(using: String.Encoding.utf8)
            {
                if let _    = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [[String: AnyObject]]
                {
                    items   = Mapper<Brand>().mapArray(JSONString: json)!
                }
            }
        }
        return items
    }
    
    func toJson()->String
    {
        return Mapper().toJSONString(self, prettyPrint: true)!
    }
    
    static func toJsonFromList(items:Array<Brand>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: true)!
    }
    
    static func toJsonNonPrettyFromList(items:Array<Brand>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: false)!
    }
}
