//
//  paylog.swift
//  configurator(iPad)
//
//  Created by MobileSuperMaster on 10/11/17.
//  Copyright © 2017 MobileMaster. All rights reserved.
//

import Foundation
import ObjectMapper

class Paylog : Mappable
{
    var id              :String
    var user_id         :String
    var method          :String
    var amount          :String
    var paid_date       :String
    var duration        :String
    
    init()
    {
        id              = ""
        user_id         = ""
        method          = ""
        amount          = ""
        paid_date       = ""
        duration        = ""
    }
    
    required init?(map: Map)
    {
        id              = ""
        user_id         = ""
        method          = ""
        amount          = ""
        paid_date       = ""
        duration        = ""
    }
    
    // Mappable
    func mapping(map: Map)
    {
        id              <- map["id"]
        user_id         <- map["user_id"]
        method          <- map["method"]
        amount          <- map["amount"]
        paid_date       <- map["paid_date"]
        duration        <- map["duration"]
    }
    
    static func fromJson(json:String)->Paylog
    {
        let paylog:Paylog = Mapper<Paylog>().map(JSONString: json)!
        
        return paylog
    }
    
    func toJson()->String
    {
        return Mapper().toJSONString(self, prettyPrint: true)!
    }
    
    func toDictionary()->[String: String]?
    {
        if let data = self.toJson().data(using: .utf8)
        {
            do
            {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
