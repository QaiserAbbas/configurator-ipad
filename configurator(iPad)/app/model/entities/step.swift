//
// Copyright (c) 2016 Frazzle. All rights reserved.
//

import Foundation
import ObjectMapper

class Step : Mappable
{
    var name            :String
    var categories      :Array<Category>

    init()
    {
        name            = ""
        categories      = []
    }

    required init?(map: Map)
    {
        name            = ""
        categories      = []
    }

    // Mappable
    func mapping(map: Map)
    {
        name            <- map["name"]
        categories      <- map["categories"]
    }

    static func fromJsonToList(json:String) throws ->Array<Category>
    {
        var items   = Array<Category>()
        if(json     != "")
        {
            //var error: NSError
            if let data     = json.data(using: String.Encoding.utf8)
            {
                if let _    = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: AnyObject]
                {
                    items   = Mapper<Category>().mapArray(JSONString: json)!
                }
            }
        }
        return items
    }
    
    func toJson()->String
    {
        return Mapper().toJSONString(self, prettyPrint: true)!
    }
    
    static func toJsonFromList(items:Array<Category>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: true)!
    }
    
    static func toJsonNonPrettyFromList(items:Array<Category>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: false)!
    }
}
