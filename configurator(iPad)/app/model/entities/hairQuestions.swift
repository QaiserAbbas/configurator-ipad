//
// Copyright (c) 2016 Frazzle. All rights reserved.
//

import Foundation
import ObjectMapper

class HairQuestions : Mappable
{
    var steps     : Array<Step>

    init()
    {
        steps                            = []
    }

    required init?(map: Map)
    {
        steps                            = []
    }

    // Mappable
    func mapping(map: Map)
    {
        steps                            <- map["steps"]
    }

    static func fromJson(json:String)->HairQuestions
    {
        return Mapper<HairQuestions>().map(JSONString: json)!;
    }

    static func fromJsonToList(json:String) throws ->Array<Step>
    {
        var items   = Array<Step>()
        if(json     != "")
        {
            //var error: NSError
            if let data     = json.data(using: String.Encoding.utf8)
            {
                if let _    = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: AnyObject]
                {
                    items   = Mapper<Step>().mapArray(JSONString: json)!
                }
            }
        }
        return items
    }

    func toJson()->String
    {
        return Mapper().toJSONString(self, prettyPrint: true)!
    }
    func toNonPrettyJson()->String
    {
        return Mapper().toJSONString(self, prettyPrint: false)!
    }
    
    static func toJsonFromList(items:Array<Step>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: true)!
    }

    static func toJsonNonPrettyFromList(items:Array<Step>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: false)!
    }
    /*
     {
     "type":"image",
     "originX":"left",
     "originY":"top",
     "left":65,
     "top":-1.5,
     "width":794,
     "height":711,
     "fill":"rgb(0,0,0)",
     "stroke":null,
     "strokeWidth":0,
     "strokeDashArray":null,
     "strokeLineCap":"butt",
     "strokeLineJoin":"miter",
     "strokeMiterLimit":10,
     "scaleX":1,
     "scaleY":1,
     "angle":0,
     "flipX":false,
     "flipY":false,
     "opacity":1,
     "shadow":null,
     "visible":true,
     "clipTo":null,
     "backgroundColor":"",
     "fillRule":"nonzero",
     "globalCompositeOperation":"source-over",
     "transformMatrix":null,
     "skewX":0,
     "skewY":0,
     "src":"http://192.168.0.123/upload/model_2.png",
     "filters":[
     ],
     "resizeFilters":[
     ],
     "crossOrigin":"",
     "alignX":"none",
     "alignY":"none",
     "meetOrSlice":"meet",
     "id":0,
     "createdWidth":924,
     "hasControls":false,
     "hasBorders":false,
     "lockMovementX":false,
     "lockMovementY":false,
     "selectable":false
     }
    */


}
