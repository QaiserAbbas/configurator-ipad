//
// Copyright (c) 2016 Frazzle. All rights reserved.
//

import Foundation
import ObjectMapper

class SubCategory : Mappable
{
    var name            :String
    var type            :String
    var image           :String
    var descriptions    :[String]
    var brands          :Array<Brand>?
    var products        :Array<Product>?
    var selected        :Bool
    var tag             :Int

    init()
    {
        name            = ""
        type            = ""
        image           = ""
        descriptions    = [""]
        selected        = false
        tag             = 0
    }

    required init?(map: Map)
    {
        name            = ""
        type            = ""
        image           = ""
        descriptions    = [""]
        selected        = false
        tag             = 0
    }

    // Mappable
    func mapping(map: Map)
    {
        name            <- map["name"]
        type            <- map["type"]
        image           <- map["image"]
        descriptions    <- map["descriptions"]
        brands          <- map["brands"]
        products        <- map["products"]
        selected        <- map["selected"]
        tag             <- map["tag"]
    }
    
    static func fromJsonToList(json:String) throws ->Array<SubCategory>
    {
        var items   = Array<SubCategory>()
        if(json     != "")
        {
            //var error: NSError
            if let data     = json.data(using: String.Encoding.utf8)
            {
                if let _    = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [[String: AnyObject]]
                {
                    items   = Mapper<SubCategory>().mapArray(JSONString: json)!
                }
            }
        }
        return items
    }
    
    static func fromJson(json:String)->SubCategory
    {
        return Mapper<SubCategory>().map(JSONString: json)!;
    }

    func toJson()->String
    {
        return Mapper().toJSONString(self, prettyPrint: true)!
    }
    
    static func toJsonFromList(items:Array<SubCategory>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: true)!
    }
    
    static func toJsonNonPrettyFromList(items:Array<SubCategory>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: false)!
    }
}
