//
// Copyright (c) 2016 Frazzle. All rights reserved.
//

import Foundation
import ObjectMapper

class Category : Mappable
{
    var name            :String
    var description     :String
    var subCategories   :Array<SubCategory>

    init()
    {
        name            = ""
        description     = ""
        subCategories   = []
    }

    required init?(map: Map)
    {
        name            = ""
        description     = ""
        subCategories   = []
    }

    // Mappable
    func mapping(map: Map)
    {
        name            <- map["name"]
        description     <- map["description"]
        subCategories   <- map["subCategories"]
    }

    
    static func fromJsonToList(json:String) throws ->Array<SubCategory>
    {
        var items   = Array<SubCategory>()
        if(json     != "")
        {
            //var error: NSError
            if let data     = json.data(using: String.Encoding.utf8)
            {
                if let _    = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: AnyObject]
                {
                    items   = Mapper<SubCategory>().mapArray(JSONString: json)!
                }
            }
        }
        return items
    }
    
    func toJson()->String
    {
        return Mapper().toJSONString(self, prettyPrint: true)!
    }
    
    static func toJsonFromList(items:Array<SubCategory>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: true)!
    }
    
    static func toJsonNonPrettyFromList(items:Array<SubCategory>)->String
    {
        return Mapper().toJSONString(items, prettyPrint: false)!
    }
}
