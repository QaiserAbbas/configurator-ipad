//
// Copyright (c) 2016 Frazzle. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class ClientManager
{

    private static let KEY_CLIENTS_JSON:String          = "KEY_CLIENTS_JSON";
    private static let KEY_CLIENT_JSON:String           = "KEY_CLIENT_JSON";
    
    private static let KEY_NEW_CLIENTS:String           = "KEY_NEW_CLIENTS";

    private static let KEY_BRANDS_JSON:String           = "KEY_BRANDS_JSON";
    private static let KEY_CATEGORIES_JSON:String       = "KEY_CATEGORIES_JSON";
    private static let KEY_CBP_LIST_JSON:String         = "KEY_CBP_LIST_JSON";
    private static let KEY_PRODUCTS_JSON:String         = "KEY_PRODUCTS_JSON";
    
    static func setJson(json:String)
    {
        SimplePersistence.setString(key: KEY_CLIENT_JSON, value: json)
    }
    
    static func getJson()->String
    {
        return SimplePersistence.getString(key: KEY_CLIENT_JSON)
    }
    
    static func setClients(clients:Array<Client>)
    {
        SimplePersistence.setString(key: KEY_CLIENTS_JSON, value:Client.toJsonFromList(items: clients))
    }
    
    static func getClients()->Array<Client>
    {
        return try! Client.fromJsonToList(json: SimplePersistence.getString(key: KEY_CLIENTS_JSON))
    }

    static func setBrands(brands:Array<Brand>)
    {
        SimplePersistence.setString(key: KEY_BRANDS_JSON, value:Brand.toJsonFromList(items: brands))
    }
    
    static func getBrands()->Array<Brand>
    {
        return try! Brand.fromJsonToList(json: SimplePersistence.getString(key: KEY_BRANDS_JSON))
    }

    static func setCategories(categories:Array<SubCategory>)
    {
        SimplePersistence.setString(key: KEY_CATEGORIES_JSON, value:SubCategory.toJsonFromList(items: categories))
    }
    
    static func getCategories()->Array<SubCategory>
    {
        return try! SubCategory.fromJsonToList(json: SimplePersistence.getString(key: KEY_CATEGORIES_JSON))
    }

    static func setCBPList(categories:Array<SubCategory>)
    {
        SimplePersistence.setString(key: KEY_CBP_LIST_JSON, value:SubCategory.toJsonFromList(items: categories))
    }
    
    static func getCBPList()->Array<SubCategory>
    {
        return try! SubCategory.fromJsonToList(json: SimplePersistence.getString(key: KEY_CBP_LIST_JSON))
    }
    
    static func setSelectedProducts(products:Array<Product>)
    {
        SimplePersistence.setString(key: KEY_PRODUCTS_JSON, value:Product.toJsonFromList(items: products))
    }
    
    static func getSelectedProducts()->Array<Product>
    {
        return try! Product.fromJsonToList(json: SimplePersistence.getString(key: KEY_PRODUCTS_JSON))
    }
    
    static func getAsList()->Array<Client>
    {
        return try! Client.fromJsonToList(json: self.getJson())
    }
    
    static func getLastNewClientAdded()->Client?
    {
        let list = getClients()
        if(!list.isEmpty)
        {
            return list.last
        }
        
        return nil
    }
    
    static func addNewClient(client:Client)
    {
        var list = getClients()
        list.append(client)
        setClients(clients: list)
    }
    
    static func removeLastNewClientAdded()
    {
        var list = getClients()
        list.removeLast()
        setClients(clients: list)
    }
    
    static func removeAllClients()
    {
        var list = getClients()
        list.removeAll()
        setClients(clients: list)
    }
    
    static func updateClient(client:Client)
    {
        let list = getClients()
        for item in list
        {
            if (item.id == client.id)
            {
                item.clientname   = client.clientname
                item.phonenumber  = client.phonenumber
                item.email        = client.email
                item.address      = client.address
                item.age          = client.age
                item.gender       = client.gender
                item.image        = client.image
                item.country      = client.country
                item.birthdate    = client.birthdate
                item.city         = client.city
                item.postal       = client.postal
            }
        }
        setClients(clients: list)
    }
    
    static func getClient(clientId: String) -> Client?
    {
        let list = getClients()
        for item in list
        {
            if item.id == clientId
            {
                item.setObjects()
                item.setHairQuestions()
                return item
            }
        }
        
        return nil
    }
}
