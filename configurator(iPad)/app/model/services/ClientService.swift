//
// Copyright (c) 2016 Frazzle. All rights reserved.
//

import Foundation
import Alamofire


class ClientService : Service
{
    static let GET_CLIENTS_SERVICE_URL_SEGMENT:String           = "/clients?userid="
    static let CREATE_CLIENT_SERVICE_URL_SEGMENT:String         = "/createClient"
    static let UPDATE_CLIENT_SERVICE_URL_SEGMENT:String         = "/updateClient"
    static let UPDATE_POINTS_INFO_SERVICE_URL_SEGMENT:String    = "/updatePointsInfo"
    static let REMOVE_CLIENT_SERVICE_URL_SEGMENT:String         = "/removeClient"
    static let UPDATE_POINTS_SERVICE_URL_SEGMENT:String         = "/updateClientPoints"
    static let UPLOAD_SIGN_SERVICE_URL_SEGMENT:String           = "/uploadClientSignature"
    static let UPLOAD_IMAGE_SERVICE_URL_SEGMENT:String          = "/memberphoto"

    static let GET_BRANDS_SERVICE_URL_SEGMENT:String            = "/listProductByBrand?user_id="
    static let GET_CATEGORIES_SERVICE_URL_SEGMENT:String        = "/listProductByCategory?user_id="
    static let CREATE_PRODUCT_SERVICE_URL_SEGMENT:String        = "/createProduct"
    static let SELECT_PRODUCTS_SERVICE_URL_SEGMENT:String       = "/selectProducts"
    static let REMOVE_PRODUCTS_SERVICE_URL_SEGMENT:String       = "/removeProducts"
    static let GET_SELECT_PRODUCTS_SERVICE_URL_SEGMENT:String   = "/getSelectProducts?user_id="
    static let GET_LIST_CBP_LIST_SERVICE_URL_SEGMENT:String     = "/listSelProductByCategory?user_id="
    static let UPDATE_CLIENT_BDATE_SERVICE_URL_SEGMENT:String   = "/updateClientBeautyDate"
    static let REMOVE_CLIENT_BDATE_SERVICE_URL_SEGMENT:String   = "/removeClientBeautyDate"
    
    static let SETUP_PAYMENT_SERVICE_URL_SEGMENT:String         = "/setupPayment"
    
    static func getClients(user:User)
    {
        showNetworkIndicator()
        let clientsUrl  = C.URL_SERVER + GET_CLIENTS_SERVICE_URL_SEGMENT + user.id

        Alamofire.request(clientsUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: buildHeaders(user: user))
            .responseString {
                response in
                hideNetworkIndicator()
                
                if (response.result.isSuccess)
                {
                    if let JSON: String = response.result.value
                    {
                        print("JSON: \(JSON)")
                        var clients: Array<Client>?
                        do
                        {
                            clients = try Client.fromJsonToList(json: JSON)
                            ClientManager.setClients(clients:clients!)
                            
                            if (clients!.count > 0)                            {
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENTS_DOWNLOADED), object: self)
                            }
                            else
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENTS_EMPTY_DOWNLOADED), object: self)
                            }
                        }
                        catch _
                        {
                            clients = nil
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENTS_FAILED_DOWNLOADED), object: self)
                        }
                    }
                }
        }
    }
    
    static func createClient(client:Client, image:UIImage, upload:Bool)
    {
        showNetworkIndicator()
        let uploadUrl   = C.URL_SERVER + CREATE_CLIENT_SERVICE_URL_SEGMENT
        print(uploadUrl)
        Alamofire.upload(multipartFormData:
            {
                multipartFormData in
                if let imageData = UIImageJPEGRepresentation(image, 0.5)
                {
                    if (upload)
                    {
                        multipartFormData.append(imageData, withName: "photo", fileName: "file.jpeg", mimeType: "image/jpeg")
                    }                    
                }
                
                let contentDict = client.toDictionary()! as! [String : String]

                for (key, value) in contentDict
                {
                    multipartFormData.append(value.data(using: .utf8)!, withName: key)
                }
            }, to: uploadUrl, method: .post, headers: nil,
            encodingCompletion:
            {
                encodingResult in
                    switch encodingResult
                    {
                    case .success(let upload, _, _):
                        upload.uploadProgress(closure: { (progress) in
                            print("Upload Progress: \(progress.fractionCompleted)")
                        })
                        
                        upload.responseJSON { response in
                            do
                            {
                                let jsonData = try JSONSerialization.data(withJSONObject: response.result.value!, options: .prettyPrinted)
                                
                                if let JSON = String(data: jsonData, encoding: .utf8)
                                {
                                    print("JSON: \(JSON)")
                                    
                                    let newClient:Client = Client.fromJson(json: JSON)
                                    
                                    if newClient.clientname != ""
                                    {
                                        ClientManager.addNewClient(client: newClient)
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_CREATED), object: self)
                                    }
                                    else
                                    {
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_FAILED_CREATED), object: self)
                                    }
                                }
                                else
                                {
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_FAILED_CREATED), object: self)
                                }
                            }
                            catch
                            {
                                print(error.localizedDescription)
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_FAILED_CREATED), object: self)
                            }

                        }
                    case .failure(let encodingError):
                        print("error:\(encodingError)")
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_FAILED_CREATED), object: self)
                    }
        })
    }

    static func updateClient(client:Client, image:UIImage, uploadImage:Bool)
    {
        showNetworkIndicator()
        let uploadUrl   = C.URL_SERVER + UPDATE_CLIENT_SERVICE_URL_SEGMENT
        print(uploadUrl)
        Alamofire.upload(multipartFormData:
            {
                multipartFormData in
                if let imageData = UIImageJPEGRepresentation(image, 0.5)
                {
                    if (uploadImage)
                    {
                        multipartFormData.append(imageData, withName: "photo", fileName: "file.jpeg", mimeType: "image/jpeg")
                    }
                }
                
                let contentDict = client.toDictionary()! as! [String : String]
                
                for (key, value) in contentDict
                {
                    multipartFormData.append(value.data(using: .utf8)!, withName: key)
                }
        }, to: uploadUrl, method: .post, headers: nil,
           encodingCompletion:
            {
                encodingResult in
                switch encodingResult
                {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        do
                        {
                            if response.result.isFailure
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_FAILED_UPDATED), object: self)
                                return
                            }
                            let jsonData = try JSONSerialization.data(withJSONObject: response.result.value!, options: .prettyPrinted)
                            
                            if let JSON = String(data: jsonData, encoding: .utf8)
                            {
                                print("JSON: \(JSON)")
                                
                                let newClient:Client = Client.fromJson(json: JSON)
                                if (!uploadImage)
                                {
//                                    newClient.image=image
                                }
                                if newClient.clientname != ""
                                {
//                                    ClientManager.addNewClient(client: newClient)
                                    ClientManager.updateClient(client: newClient)
//                                    if (uploadImage)
//                                    {
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_UPDATED_WITH_IMAGE), object: self)
//                                    }
//                                    else
//                                    {
//                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_UPDATED), object: self)
//                                    }
                                    
                                }
                                else
                                {
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_FAILED_UPDATED), object: self)
                                }
                            }
                            else
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_FAILED_UPDATED), object: self)
                            }
                        }
                        catch
                        {
                            print(error.localizedDescription)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_FAILED_UPDATED), object: self)
                        }
                        
                    }
                case .failure(let encodingError):
                    print("error:\(encodingError)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_FAILED_UPDATED), object: self)
                }
        })
    }

    static func removeClient(client:Client)
    {
        showNetworkIndicator()
        let clientUrl   = C.URL_SERVER + REMOVE_CLIENT_SERVICE_URL_SEGMENT
        let user: User  = UserManager.getLoggedInUser()!

        var parameters  = Dictionary<String, String>()
        parameters      = ["userid":user.id, "clientid":client.id]

        Alamofire.request(clientUrl, method: .post, parameters:parameters, encoding: URLEncoding.default, headers: nil)
            .responseString {
                response in
                hideNetworkIndicator()
                
                if (response.result.isSuccess)
                {
                    if let JSON: String = response.result.value
                    {
                        print("JSON: \(JSON)")
                        var clients: Array<Client>?
                        do
                        {
                            if response.result.isFailure
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_FAILED_REMOVED), object: self)
                                return
                            }
                            clients = try Client.fromJsonToList(json: JSON)
                            
                            if (clients!.count > 0)
                            {
                                ClientManager.setClients(clients:clients!)
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_REMOVED), object: self)
                            }
                            else
                            {
                                ClientManager.removeAllClients()
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_REMOVED), object: self)
                            }
                        }
                        catch _
                        {
                            clients = nil
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CLIENT_FAILED_REMOVED), object: self)
                        }
                    }
                }
        }
    }
//    
//    static func updatePoints(parameters:Dictionary<String, String>)
//    {
//        showNetworkIndicator()
//        let clientUrl   = C.URL_SERVER + UPDATE_POINTS_SERVICE_URL_SEGMENT
//        
//        Alamofire.request(clientUrl, method: .post, parameters:parameters, encoding: URLEncoding.default, headers: nil)
//            .responseString {
//                response in
//                hideNetworkIndicator()
//                if (response.result.isSuccess)
//                {
//                }
//        }
//    }
    
    static func updatePoints(parameters:Dictionary<String, String>, image:UIImage)
    {
        showNetworkIndicator()
        let uploadUrl   = C.URL_SERVER + UPDATE_POINTS_SERVICE_URL_SEGMENT
        print(uploadUrl)
        Alamofire.upload(multipartFormData:
            {
                multipartFormData in
                if let imageData = UIImageJPEGRepresentation(image, 0.5)
                {
                    multipartFormData.append(imageData, withName: "photo", fileName: "file.jpeg", mimeType: "image/jpeg")
                }
                
                for (key, value) in parameters
                {
                    multipartFormData.append(value.data(using: .utf8)!, withName: key)
                }
        }, to: uploadUrl, method: .post, headers: nil,
           encodingCompletion:
            {
                encodingResult in
                switch encodingResult
                {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        print("update response = \(String(describing: response.result.value))")
                    }
                case .failure(let encodingError):
                    print("error:\(encodingError)")
                }
        })
    }
    
    static func uploadClientSignature(parameters:Dictionary<String, String>, image:UIImage)
    {
        showNetworkIndicator()
        let uploadUrl   = C.URL_SERVER + UPLOAD_SIGN_SERVICE_URL_SEGMENT
        print(uploadUrl)
        Alamofire.upload(multipartFormData:
            {
                multipartFormData in
                if let imageData = UIImageJPEGRepresentation(image, 0.5)
                {
                    multipartFormData.append(imageData, withName: "photo", fileName: "file.jpeg", mimeType: "image/jpeg")
                }
                
                for (key, value) in parameters
                {
                    multipartFormData.append(value.data(using: .utf8)!, withName: key)
                }
        }, to: uploadUrl, method: .post, headers: nil,
           encodingCompletion:
            {
                encodingResult in
                switch encodingResult
                {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        print("upload client signature response = \(String(describing: response.result.value))")
                    }
                case .failure(let encodingError):
                    print("error:\(encodingError)")
                }
        })
    }
    
    static func getProductsByBrands(user:User)
    {
        showNetworkIndicator()
        let clientsUrl  = C.URL_SERVER + GET_BRANDS_SERVICE_URL_SEGMENT + user.id

        Alamofire.request(clientsUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: buildHeaders(user: user))
            .responseString {
                response in
                hideNetworkIndicator()
                
                if (response.result.isSuccess)
                {
                    if let JSON: String = response.result.value
                    {
                        print("JSON: \(JSON)")
                        var brands: Array<Brand>?
                        do
                        {
                            brands = try Brand.fromJsonToList(json: JSON)
                            ClientManager.setBrands(brands:brands!)
                            
                            if (brands!.count > 0)
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.BRANDS_DOWNLOADED), object: self)
                            }
                            else
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.BRANDS_EMPTY_DOWNLOADED), object: self)
                            }
                        }
                        catch _
                        {
                            brands = nil
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.BRANDS_FAILED_DOWNLOADED), object: self)
                        }
                    }
                }
        }
    }
    
    static func getProductsByCategories(user:User)
    {
        showNetworkIndicator()
        let clientsUrl  = C.URL_SERVER + GET_CATEGORIES_SERVICE_URL_SEGMENT + user.id
        
        Alamofire.request(clientsUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: buildHeaders(user: user))
            .responseString {
                response in
                hideNetworkIndicator()
                
                if (response.result.isSuccess)
                {
                    if let JSON: String = response.result.value
                    {
                        print("JSON: \(JSON)")
                        var categories: Array<SubCategory>?
                        do
                        {
                            categories = try SubCategory.fromJsonToList(json: JSON)
                            ClientManager.setCategories(categories: categories!)
                            
                            if (categories!.count > 0)
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CATEGORIES_DOWNLOADED), object: self)
                            }
                            else
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CATEGORIES_EMPTY_DOWNLOADED), object: self)
                            }
                        }
                        catch _
                        {
                            categories = nil
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CATEGORIES_FAILED_DOWNLOADED), object: self)
                        }
                    }
                }
        }
    }
    
    static func selectProducts(parameters:Dictionary<String, Any>)
    {
        showNetworkIndicator()

        Alamofire.request(C.URL_SERVER+SELECT_PRODUCTS_SERVICE_URL_SEGMENT, method: .post,  parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .responseString
            {
                response in
                hideNetworkIndicator()
                if (response.result.isSuccess)
                {
                    if let JSON: String = response.result.value
                    {
                        print("JSON: \(JSON)")
                        var products: Array<Product>?
                        do
                        {
                            products = try Product.fromJsonToList(json: JSON)
                            ClientManager.setSelectedProducts(products: products!)
                            
                            if (products!.count > 0)
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCTS_SELECTED), object: self)
                            }
                            else
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCTS_FAILED_SELECTED), object: self)
                            }
                        }
                        catch _
                        {
                            products = nil
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCTS_FAILED_SELECTED), object: self)
                        }
                    }
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCTS_FAILED_SELECTED), object: self)
                }
        }
    }
    
    static func removeProducts(parameters:Dictionary<String, Any>)
    {
        showNetworkIndicator()
        
        Alamofire.request(C.URL_SERVER+REMOVE_PRODUCTS_SERVICE_URL_SEGMENT, method: .post,  parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .responseString
            {
                response in
                hideNetworkIndicator()
                if (response.result.isSuccess)
                {
                    if let JSON: String = response.result.value
                    {
                        print("JSON: \(JSON)")
                        var products: Array<Product>?
                        do
                        {
                            products = try Product.fromJsonToList(json: JSON)
                            ClientManager.setSelectedProducts(products: products!)
                            
                            if (products!.count > 0)
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCTS_REMOVED), object: self)
                            }
                            else
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCTS_FAILED_REMOVED), object: self)
                            }
                        }
                        catch _
                        {
                            products = nil
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCTS_FAILED_REMOVED), object: self)
                        }
                    }
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCTS_FAILED_REMOVED), object: self)
                }
        }
    }
    
    static func createProduct(parameters:Dictionary<String, String>)
    {
        showNetworkIndicator()
        Alamofire.request(C.URL_SERVER+CREATE_PRODUCT_SERVICE_URL_SEGMENT, method: .post,  parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .responseString
            {
                response in
                hideNetworkIndicator()
                if (response.result.isSuccess)
                {
                    if let JSON: String = response.result.value
                    {
                        print("JSON: \(JSON)")
                        let product: Product = Product.fromJson(json: JSON)
                        
                        if product.name != ""
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCT_CREATED), object: self)
                        }
                        else
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCT_FAILED_CREATED), object: self)
                        }
                    }
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCT_FAILED_CREATED), object: self)
                }
            }
    }
    
    static func getSelectProducts(user:User)
    {
        showNetworkIndicator()
        let clientsUrl  = C.URL_SERVER + GET_SELECT_PRODUCTS_SERVICE_URL_SEGMENT + user.id
        
        Alamofire.request(clientsUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: buildHeaders(user: user))
            .responseString {
                response in
                hideNetworkIndicator()
                
                if (response.result.isSuccess)
                {
                    if let JSON: String = response.result.value
                    {
                        print("JSON: \(JSON)")
                        var products: Array<Product>?
                        do
                        {
                            products = try Product.fromJsonToList(json: JSON)
                            ClientManager.setSelectedProducts(products: products!)
                            
                            if (products!.count > 0)
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCTS_DOWNLOADED), object: self)
                            }
                            else
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCTS_EMPTY_DOWNLOADED), object: self)
                            }
                        }
                        catch _
                        {
                            products = nil
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PRODUCTS_FAILED_DOWNLOADED), object: self)
                        }
                    }
                }
        }
    }
    
    static func getCategoryBrandProductList(user:User)
    {
        showNetworkIndicator()
        let clientsUrl  = C.URL_SERVER + GET_LIST_CBP_LIST_SERVICE_URL_SEGMENT + user.id
        
        Alamofire.request(clientsUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: buildHeaders(user: user))
            .responseString {
                response in
                hideNetworkIndicator()
                
                if (response.result.isSuccess)
                {
                    if let JSON: String = response.result.value
                    {
                        print("JSON: \(JSON)")
                        var categories: Array<SubCategory>?
                        do
                        {
                            categories = try SubCategory.fromJsonToList(json: JSON)
                            ClientManager.setCBPList(categories: categories!)
                            
                            if (categories!.count > 0)
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CBP_LIST_DOWNLOADED), object: self)
                            }
                            else
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CBP_LIST_EMPTY_DOWNLOADED), object: self)
                            }
                        }
                        catch _
                        {
                            categories = nil
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.CBP_LIST_FAILED_DOWNLOADED), object: self)
                        }
                    }
                }
        }
    }
    
    static func setupPayment(parameters:Dictionary<String, String>)
    {
        showNetworkIndicator()
        Alamofire.request(C.URL_SERVER+SETUP_PAYMENT_SERVICE_URL_SEGMENT, method: .post,  parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .responseString
            {
                response in
                hideNetworkIndicator()
                if (response.result.isSuccess)
                {
                    if let JSON: String = response.result.value
                    {
                        print("JSON: \(JSON)")
                        let paylog: Paylog = Paylog.fromJson(json: JSON)
                        
                        if paylog.id != ""
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PAYLOG_CREATED), object: self)
                            if (UserDefaults.standard.dictionary(forKey: C.PAYLOG_FAILED_CREATED) != nil)
                            {
                                UserDefaults.standard.removeObject(forKey: C.PAYLOG_FAILED_CREATED)
                            }
                        }
                        else
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PAYLOG_FAILED_CREATED), object: self)
                            UserDefaults.standard.set(parameters, forKey: C.PAYLOG_FAILED_CREATED)
                        }
                    }
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.PAYLOG_FAILED_CREATED), object: self)
                    UserDefaults.standard.set(parameters, forKey: C.PAYLOG_FAILED_CREATED)
                }
        }
    }
    
    static func updateClientBeautyDate(parameters:Dictionary<String, String>)
    {
        showNetworkIndicator()
        Alamofire.request(C.URL_SERVER+UPDATE_CLIENT_BDATE_SERVICE_URL_SEGMENT, method: .post,  parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .responseString
            {
                response in
                hideNetworkIndicator()
                if (response.result.isSuccess)
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.UPDATE_CLIENT_BDATE_SUCCESS), object: self)
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.UPDATE_CLIENT_BDATE_FAILED), object: self)
                }
            }
    }
    
    static func removeClientBeautyDate(parameters:Dictionary<String, String>)
    {
        showNetworkIndicator()
        Alamofire.request(C.URL_SERVER+REMOVE_CLIENT_BDATE_SERVICE_URL_SEGMENT, method: .post,  parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .responseString
            {
                response in
                hideNetworkIndicator()
                if (response.result.isSuccess)
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.REMOVE_CLIENT_BDATE_SUCCESS), object: self)
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: C.REMOVE_CLIENT_BDATE_FAILED), object: self)
                }
        }
    }

}
