# beauty_configurator-iPad-
개발자:리선룡

플래트폼:iOS

개발환경:Xcode8.2 - Swift3.0

프로젝트명:beauty_configurator-iPad-

프로젝트요약:

- 범주:iOS Drawing Application

- 특성:

o ACEDrawingView Framework에 의한 그리기처리 진행
o SVGgh Framework에 의한 SVG-Image/Image-SVG변환 진행

개발기간:

시작:2017년 2월 11일
Upgrade 01: 프로젝트 초판완성
완료:진행중
